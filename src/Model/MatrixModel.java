package Model;

import Algorithms.*;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created with IntelliJ IDEA.
 * User: Shrey
 * Date: 10/9/13
 * Time: 1:45 PM
 * To change this template use File | Settings | File Templates.
 */
public class MatrixModel extends AbstractModel {

    ArrayList<Person> persons;
    Group group;


    long timeInside;
    long clickRightCount;
    long clickLeftCount;


    public MatrixModel() {
        this.timeInside = 0;
        this.clickLeftCount =0;
        this.clickRightCount =0;
    }

    double calculateAveragePositionOfNode(int node) {
        ArrayList temp = this.group.getNeighboursOfNode(node);
        double sum = positionOfNode(node) * 1.1;
        for (int i = 0; i < temp.size(); i++) {
            if (Constants.includeWeightsInBaryCentric)
                sum += positionOfNode((int) temp.get(i)) * this.group.getNoOfMeetingsBetweenPeople(node, (int) temp.get(i));
            else
                sum += positionOfNode((int) temp.get(i));
        }
        return sum / (temp.size() + 1.1);
    }

    public void reorderNodesBasedOnBarycentric(int numIterations) {
        BaryData reorderArray[];
        for (int i = 0; i < numIterations; i++) {
            reorderArray = new BaryData[this.group.noOfPeopleInGroup()];
            for (int j = 0; j < this.group.noOfPeopleInGroup(); j++) {
                reorderArray[j] = new BaryData(-1, -1);
                reorderArray[j].index = Constants.orderOfNode.get(j);
                reorderArray[j].average = calculateAveragePositionOfNode(Constants.orderOfNode.get(j));
            }
            BaryData.sort(reorderArray);
            Constants.orderOfNode = new ArrayList<>();
            for (int k = 0; k < reorderArray.length; k++) {
                Constants.orderOfNode.add(reorderArray[k].index);
            }
        }
        firePropertyChange("OrderChanged", null, null);
    }

    public int positionOfNode(int node) {
        return Constants.orderOfNode.indexOf(node);
    }

    public int numberOfNode() {
        return Constants.orderOfNode.size();
    }

    public MinMax getMinMax() {
        return this.group.getMinMax();
    }

    public int nodeAtPosition(int node) {
        return Constants.orderOfNode.get(node);
    }

    public int getNoOfMeetingsBetween(int i, int j) {
        return this.group.getNoOfMeetingsBetweenPeople(i, j);
    }

    public Node[][] getMeet() {
        return this.group.getMeet();
    }

    public void setOrderOfNode(ArrayList<Integer> orderOfNode) {
        Constants.orderOfNode = orderOfNode;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Group getGroup() {
        return group;
    }


    public ArrayList<Person> getPersons() {
        return persons;
    }

    public void setPersons(ArrayList<Person> persons) {
        this.persons = persons;
        this.group = new Group(this.persons);
        Constants.orderOfNode = new ArrayList<>();
        for (int i = 0; i < this.group.noOfPeopleInGroup(); i++) {
            Constants.orderOfNode.add(i);
        }
        Collections.shuffle(Constants.orderOfNode);

    }

    public void  setAndRemoveHighlightedNode(int i, int j) {
        if (i != j) {
            Group groupOld = this.group;
            if (this.group.getMeet()[i][j].isHighlighted() == true
                    ) {
                   if (this.persons.get(i).getHighlighted() == false || this.persons.get(j).getHighlighted() == false) {
                this.group.getMeet()[i][j].setHighlighted(false);
                this.group.getMeet()[j][i].setHighlighted(false);
                   }
            } else {
                this.group.getMeet()[i][j].setHighlighted(true);
                this.group.getMeet()[j][i].setHighlighted(true);
            }
            Group groupNew = this.group;
            firePropertyChange("highlighted change", null, groupNew);
        }
    }

    public void setAndRemoveHighlightedPerson(int i) {
        Group groupOld = this.group;
        if(this.persons.get(i).getHighlighted() == false)
        this.persons.get(i).setHighlighted(true);
        else this.persons.get(i).setHighlighted(false);
        for (int j = 0; j < this.persons.size(); j++) {
            for (int k = 0; k < this.persons.size(); k++) {
                if (this.persons.get(j).getHighlighted() == true
                        && this.persons.get(k).getHighlighted() == true && j !=k) {
                    this.group.getMeet()[j][k].setHighlighted(true);
                    this.group.getMeet()[k][j].setHighlighted(true);

                }
            }
        }
        Group groupNew = this.group;
        firePropertyChange("highlighted change", null, groupNew);

    }

    public boolean isPersonHighlighted(int i) {
        return this.persons.get(i).getHighlighted();
    }

    public void deselectAll() {
        Group groupOld = this.group;
        for (int i = 0; i < this.persons.size(); i++) {
            this.persons.get(i).setHighlighted(false);
        }

        for (int i = 0; i < this.group.getMeet().length; i++) {
            for(int j = 0; j < this.group.getMeet()[0].length; j ++)
                this.group.getMeet()[i][j].setHighlighted(false);
        }
        Group groupNew = this.group;
        firePropertyChange("highlighted change", null,groupNew );

    }

    public long getTimeInside() {
        return timeInside;
    }

    public long getClickRightCount() {
        return clickRightCount;
    }

    public long getClickLeftCount() {
        return clickLeftCount;
    }

    public void setTimeInside(long timeInside) {
        this.timeInside = timeInside;
    }

    public void setClickRightCount(long clickRightCount) {
        this.clickRightCount = clickRightCount;
    }

    public void setClickLeftCount(long clickLeftCount) {
        this.clickLeftCount = clickLeftCount;
    }
}