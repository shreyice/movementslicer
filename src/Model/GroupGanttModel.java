package Model;

import Algorithms.*;
import com.google.common.collect.Sets;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Shrey
 * Date: 10/18/13
 * Time: 8:46 PM
 * To change this template use File | Settings | File Templates.
 */

public class GroupGanttModel extends AbstractModel {

    Group group;
    List<Person> persons;
    ArrayList<Integer> selectedPeople;
    ArrayList<Integer> listOfUniqueStayRegions;
    Map<Integer, Integer> noOfTimesEachPlaceIsVisited;
    TreeMap<Integer, Map<Integer, Integer>> personWiseUniqueStayRegions;
    ArrayList<String> names;
    ArrayList<String> namesSeperat;
    List<MeetingPoint> selectPeopleMeetings;
    List<MeetingPoint> seperatePeople;
    List<MeetingPoint> justMeeting;
    HashMap<Integer, String> rememberedNames = new HashMap<>();

    HashMap<Integer, TreeMap<DateTime, Integer>> connectingLine;

    boolean diffRows = false;

    boolean sorting = true;

    int highlightPerson = -1;

    public GroupGanttModel() {
    }

    public Group getGroup() {
        return group;
    }

    public void updateGroupModel(Group group) {
        this.group = group;
        this.persons = this.group.getPersonList();
        selectedPeopleNo();
//        this.selectPeopleMeetings = calculateSelectedPeopleMeetingsAllPeople();
//        this.selectPeopleMeetings = calculateJustMeeting();
        this.selectPeopleMeetings = calculateSelectedPeopleMeetingsAllCombPeople();
        this.justMeeting = calculateJustMeeting();
        listOfUniqueMeetingPlaces(this.selectPeopleMeetings);
        this.names = setNames(this.listOfUniqueStayRegions);
        this.seperatePeople = calculateSeperatePeople();
//        filter();

        this.namesSeperat = setNames(this.personWiseUniqueStayRegions);


    }

    class Pair {
        public int personId;
        public int regionId;

        public Pair(int personId, int regionId) {
            this.personId = personId;
            this.regionId = regionId;
        }

        @Override
        public boolean equals(Object object) {
            boolean sameSame = false;
            if (object != null && object instanceof Pair) {
                sameSame = (this.personId == ((Pair) object).personId
                        && this.regionId == ((Pair) object).regionId);
            }
            return sameSame;
        }
    }

    public void filter() {
        ArrayList<Pair> unique = new ArrayList<>();
        for (MeetingPoint meet : this.justMeeting) {
            int stayRegionId = meet.getStayRegionId();
            for (StayPoint stay : meet.getStayPoints()) {
                int personId = stay.getPersonId();
                Pair pair = new Pair(personId, stayRegionId);
                if (!unique.contains(pair))
                    unique.add(pair);
            }
        }
//        System.out.println("HI");

        for (int i : this.personWiseUniqueStayRegions.keySet()) {
            TreeMap<Integer, Integer> personWise = new TreeMap<>();
            TreeMap<Integer, Integer> personWise1 = new TreeMap<>();

            personWise.putAll(this.personWiseUniqueStayRegions.get(i));
            personWise1.putAll(this.personWiseUniqueStayRegions.get(i));
            for (int k : personWise.keySet()) {
                if (!unique.contains(new Pair(i, k))) {
                    personWise1.remove(k);
                }
            }
            this.personWiseUniqueStayRegions.get(i).clear();
            this.personWiseUniqueStayRegions.get(i).putAll(personWise1);
        }
    }


    List<MeetingPoint> calculateSeperatePeople() {
        this.personWiseUniqueStayRegions = new TreeMap<>();
        List<MeetingPoint> meeting = new ArrayList<>();
        for (int i = 0; i < this.selectedPeople.size(); i++) {
            this.personWiseUniqueStayRegions.put(this.selectedPeople.get(i), new HashMap<Integer, Integer>());
            ArrayList<StayPoint> list = this.persons.get(this.selectedPeople.get(i)).getStayPointinArrayList();
            Map<Integer, Integer> unique = this.personWiseUniqueStayRegions.get(this.selectedPeople.get(i));
            for (int j = 0; j < list.size(); j++) {
                StayPoint s = list.get(j);
                if (!unique.containsKey(s.getCommonStayRegionId())) {
                    unique.put(s.getCommonStayRegionId(), 1);
                } else {
                    int val = unique.get(s.getCommonStayRegionId());
                    val++;
                    unique.put(s.getCommonStayRegionId(), val);
                }
                MeetingPoint meet = new MeetingPoint();
                ArrayList<Integer> temp1 = new ArrayList<>();
                temp1.add(this.selectedPeople.get(i));
                meet.setPersonId(temp1);
                meet.setCenter(this.getCommonStayRegionCentreFromId(s.getCommonStayRegionId()));
                meet.setInterval(new Interval(s.getArrivalDateTime(), s.getDepartureDateTime()));
                meet.setStayRegionId(s.getCommonStayRegionId());
                ArrayList<StayPoint> temp = new ArrayList<>();
                temp.add(s);
                meet.setStayPoints(temp);
                meeting.add(meet);
            }

            MyComparator comparator = new MyComparator(unique);

            Map<Integer, Integer> newMap = new TreeMap<Integer, Integer>(comparator);
            newMap.putAll(unique);
            this.personWiseUniqueStayRegions.put(this.selectedPeople.get(i), newMap);

//            unique = newMap;
        }
        Collections.sort(meeting);
        return meeting;
    }


    ArrayList<String> setNames(ArrayList<Integer> list) {
        ArrayList<String> names = new ArrayList<>();

        for (int i : list) {
            names.add("Location " + Integer.toString(i));
        }
        return names;
    }

    ArrayList<String> setNames(TreeMap<Integer, Map<Integer, Integer>> map) {
        ArrayList<String> names = new ArrayList<>();

            for (int id : this.selectedPeople) {
                int personId = id;
                for (int k : map.get(personId).keySet())
                    names.add("Location " + Integer.toString(k));
            }
        return names;
    }


    //all combination meetings of people
    List<MeetingPoint> calculateJustMeeting() {
        List<MeetingPoint> meet = new ArrayList<>();
        HashMap<ArrayList<Integer>, List<MeetingPoint>> hashMap = new HashMap<>();
        Set<Set<Integer>> list = Sets.powerSet(Sets.newHashSet(this.selectedPeople));

        for (Set<Integer> current : list) {
            if (current.size() > 1) {
                ArrayList<Integer> tempList = new ArrayList<>();
                for (int i : current) {
                    tempList.add(i);
                }
                List<MeetingPoint> t = this.group.getMeetingsPeople(tempList);
                Collections.sort(tempList);
                if (hashMap.get(tempList) == null) {
                    hashMap.put(tempList, new ArrayList<MeetingPoint>());
                }
                hashMap.get(tempList).addAll(t);
//                meet.addAll(this.group.getMeetingsPeople(tempList));
            }
        }

        // removing duplicate meetings
        hashMap = cleanMapOfDuplicates(hashMap);


        for (List<MeetingPoint> value : hashMap.values()) {
//            System.out.println("Value = " + value);
            meet.addAll(value);
        }
        Collections.sort(meet);
        return meet;
    }

    //all combination meetings of people
    List<MeetingPoint> calculateSelectedPeopleMeetingsAllCombPeople() {
        List<MeetingPoint> meet = new ArrayList<>();
        HashMap<ArrayList<Integer>, List<MeetingPoint>> hashMap = new HashMap<>();
        Set<Set<Integer>> list = Sets.powerSet(Sets.newHashSet(this.selectedPeople));

        for (Set<Integer> current : list) {
            if (current.size() > 1) {
                ArrayList<Integer> tempList = new ArrayList<>();
                for (int i : current) {
                    tempList.add(i);
                }
                List<MeetingPoint> t = this.group.getMeetingsPeople(tempList);
                Collections.sort(tempList);
                if (hashMap.get(tempList) == null) {
                    hashMap.put(tempList, new ArrayList<MeetingPoint>());
                }
                hashMap.get(tempList).addAll(t);
//                meet.addAll(this.group.getMeetingsPeople(tempList));
            }
        }

        //adding selected persons
        for (int i = 0; i < persons.size(); i++) {
            Person p = persons.get(i);
            if (p.getHighlighted()) {
                ArrayList<StayPoint> stayPoints = p.getStayPointinArrayList();
                List<MeetingPoint> tempMeet = new ArrayList<>();
                ArrayList<Integer> person = new ArrayList();
                person.add(p.getId());
                for (int j = 0; j < stayPoints.size(); j++) {
                    MeetingPoint meetingPoint = new MeetingPoint();
                    ArrayList<StayPoint> sp = new ArrayList<>();
                    sp.add(stayPoints.get(j));
                    Interval interval = new Interval(stayPoints.get(j).getArrivalDateTime(), stayPoints.get(j).getDepartureDateTime());
                    meetingPoint.setPersonId(person);
                    meetingPoint.setStayPoints(sp);
                    meetingPoint.setStayRegionId(stayPoints.get(j).getCommonStayRegionId());
                    meetingPoint.setInterval(interval);
//                    meetingPoint.calculateCenter();
                    meetingPoint.setCenter(this.getCommonStayRegionCentreFromId(stayPoints.get(j).getCommonStayRegionId()));
                    meetingPoint.setName("L " + Integer.toString(stayPoints.get(j).getCommonStayRegionId()));
                    tempMeet.add(meetingPoint);
                }
                if (hashMap.get(person) == null) {
                    hashMap.put(person, new ArrayList<MeetingPoint>());
                }
                hashMap.get(person).addAll(tempMeet);
//                meet.addAll(tempMeet);
            }
        }
        // removing duplicate meetings
        hashMap = cleanMapOfDuplicates(hashMap);
        for (List<MeetingPoint> value : hashMap.values()) {
//            System.out.println("Value = " + value);
            meet.addAll(value);
        }


        // added correction (check later if necessary)
        for (MeetingPoint temp : meet) {
            ArrayList<Integer> stayList = new ArrayList<>();
            ListIterator<StayPoint> listIterator = temp.getStayPoints().listIterator();
            while (listIterator.hasNext()) {
                StayPoint stayPoint = listIterator.next();
                if (!stayList.contains(stayPoint.getPersonId())){
                    stayList.add(stayPoint.getPersonId());
                }
                else {
                    listIterator.remove();
                }

            }
        }

        Collections.sort(meet);
        return meet;
    }

    HashMap<ArrayList<Integer>, List<MeetingPoint>> cleanMapOfDuplicates(HashMap<ArrayList<Integer>, List<MeetingPoint>> map) {
        ArrayList<ArrayList<Integer>> list = new ArrayList<>(map.keySet());
        for (int i = 0; i < list.size(); i++) {
            for (int j = 0; j < list.size(); j++) {
                if (list.get(i).size() > list.get(j).size()) {
                    ArrayList<Integer> temp = list.get(i);
                    list.set(i, list.get(j));
                    list.set(j, temp);
                }
            }
        }
        for (int i = 0; i < list.size(); i++) {
            List<MeetingPoint> temp = map.get(list.get(i));
            for (int j = i + 1; j < list.size(); j++) {
                if (list.get(i).containsAll(list.get(j))) {
                    List<MeetingPoint> temp1 = map.get(list.get(j));
                    for (int k = 0; k < temp.size(); k++) {
                        ListIterator<MeetingPoint> listIterator = temp1.listIterator();
                        while (listIterator.hasNext()) {
                            MeetingPoint meetingPoint = listIterator.next();
                            if (isSubsetMeetingPoint(temp.get(k), meetingPoint)) {
                                listIterator.remove();
                            }
                        }
                    }
                }
            }
        }
        return map;
    }


    boolean isSubsetMeetingPoint(MeetingPoint temp, MeetingPoint temp1) {
        for (int i = 0; i < temp1.getStayPoints().size(); i++) {
            StayPoint sp1 = temp1.getStayPoints().get(i);
            for (int j = 0; j < temp.getStayPoints().size(); j++) {
                StayPoint sp = temp.getStayPoints().get(j);
                if (sp1.getPersonId() == sp.getPersonId()) {
                    if (!sp1.equals(sp)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }


    void listOfUniqueMeetingPlaces(List<MeetingPoint> meetingPoints) {
//        this.names = new ArrayList<>();          0
        Map<Integer, Integer> temp = new HashMap<>();
        for (MeetingPoint meetingPoint : meetingPoints) {
            if (!temp.containsKey(meetingPoint.getStayRegionId())) {
                temp.put(meetingPoint.getStayRegionId(), 1);
//                this.names.add(meetingPoint.getName());
            } else {
                int val = temp.get(meetingPoint.getStayRegionId());
                val++;
                temp.put(meetingPoint.getStayRegionId(),
                        val
                );
            }
        }
        MyComparator comparator = new MyComparator(temp);

        Map<Integer, Integer> newMap = new TreeMap<Integer, Integer>(comparator);
        newMap.putAll(temp);

        ArrayList<Integer> list = new ArrayList(newMap.keySet());
        this.listOfUniqueStayRegions = list;
        this.noOfTimesEachPlaceIsVisited = newMap;
    }

    /*
    * Code to sort maps based on values. Courtsey of
    * Sujan Reddy A  on stack overflow
    * http://stackoverflow.com/questions/109383/how-to-sort-a-mapkey-value-on-the-values-in-java
     */
    class MyComparator implements Comparator<Object> {

        Map<Integer, Integer> map;


        public MyComparator(Map<Integer, Integer> map) {
            this.map = map;

        }

        public int compare(Object o1, Object o2) {

            if (map.get(o2) == map.get(o1))
                return 1;
            else
                return map.get(o2).compareTo(map.get(o1));


        }
    }

    void selectedPeopleNo() {
        this.selectedPeople = new ArrayList<>();
        for (int i = 0; i < this.group.noOfPeopleInGroup(); i++) {
            for (int j = 0; j < this.group.noOfPeopleInGroup(); j++) {
                //noinspection PointlessBooleanExpression
                if (this.group.getMeet()[i][j].isHighlighted() == true) {
                    if (!this.selectedPeople.contains(i))
                        this.selectedPeople.add(i);
                    if (!this.selectedPeople.contains(j))
                        this.selectedPeople.add(j);
                }
            }
        }
        for (int i = 0; i < this.persons.size(); i++) {
            if (this.persons.get(i).getHighlighted() == true) {
                if (!this.selectedPeople.contains(i))
                    this.selectedPeople.add(i);
            }
        }
//        Collections.sort(this.selectedPeople);

        //for barycentric sorting (hack)
        if(Constants.orderOfNode.size() != 0 ) {

            ArrayList<Integer> temp = new ArrayList<>();
            for (int i = 0; i < this.selectedPeople.size(); i++) {
                temp.add(Constants.orderOfNode.indexOf(this.selectedPeople.get(i)));

            }
            Collections.sort(temp);

            this.selectedPeople = new ArrayList<>();

            for (int i = 0; i < temp.size(); i++) {
                this.selectedPeople.add(Constants.orderOfNode.get(temp.get(i)));
            }
        }
    }


    public ArrayList<Integer> getListOfUniqueStayRegions() {
        return listOfUniqueStayRegions;
    }

    public ArrayList<Integer> getSelectedPeople() {
        return selectedPeople;
    }


    /*
     *  start date using meetings points in the selected people
     */
    public DateTime startDate() {
        if (this.selectPeopleMeetings.size() == 0) {
            System.out.println("no meetings");
        }
        DateTime startDate = new DateTime();
        startDate = DateTime.now();
        for (MeetingPoint meetingPoint : this.selectPeopleMeetings) {
            if (startDate.isAfter(meetingPoint.startDateTime()))
                startDate = meetingPoint.startDateTime();
        }
        return startDate;
    }


    /*
     *  end date using meetings points in the selected people
     */
    public DateTime endDate() {
        DateTime endDate = this.selectPeopleMeetings.get(0).getInterval().getEnd();
        for (MeetingPoint meetingPoint : this.selectPeopleMeetings) {
            if (endDate.isBefore(meetingPoint.endDateTime()))
                endDate = meetingPoint.endDateTime();
        }
        return endDate;
    }

    public int numberOfPeopleSelected() {
        return this.selectedPeople.size();
    }

    public int numberOfUniquePlacesSelectedPeopleMeet() {
        return this.listOfUniqueStayRegions.size();
    }

    public List<MeetingPoint> getSelectPeopleMeetings() {

        if (this.diffRows)
            return seperatePeople;
        else
            return selectPeopleMeetings;
    }

    public void setPersons(ArrayList<Person> persons) {
        this.persons = persons;
    }

    public String getName(int i) {
        if (this.diffRows)
            return this.namesSeperat.get(i);
        else
            return this.names.get(i);

    }

    public void setName(int i, String s) {
        if (this.diffRows)
            this.namesSeperat.set(i, s);
        else {
//            for (MeetingPoint meetPoint: this.group.getMultiplepeople()) {
//                if (meetPoint.getName().equals(this.names.get(i))) {
//                    meetPoint.setName(s);
//                }
//            }
            this.names.set(i, s);

        }
//        this.rememberedNames.put(i, s);
    }

    public void sort() {
        Collections.reverse(this.listOfUniqueStayRegions);
        Collections.reverse(this.names);

        for (int i : personWiseUniqueStayRegions.keySet()) {
            Map<Integer, Integer> map = new HashMap<>();
            map.putAll(personWiseUniqueStayRegions.get(i));

            MyComparator comparator = new MyComparator(map);
            Map<Integer, Integer> newMap;
            if (this.sorting) {
                newMap = new TreeMap<Integer, Integer>(Collections.reverseOrder(comparator));
            } else {
                newMap = new TreeMap<Integer, Integer>(comparator);
            }
            newMap.putAll(map);
            personWiseUniqueStayRegions.put(i, newMap);
        }
        this.sorting = !this.sorting;

        this.namesSeperat = setNames(this.personWiseUniqueStayRegions);
    }


    public void removeStayRegion(int id) {
        if (this.diffRows) {
            this.namesSeperat.remove(id);
            int personId = 0;
            int stayRegionId = 0;
            int sum = 0;
            int sum1 = 0;

            for (int i = 0; i < this.selectedPeople.size(); i++) {
                personId = this.selectedPeople.get(i);
                sum += this.personWiseUniqueStayRegions.get(personId).size();
                if (sum > id) {
                    ArrayList<Integer> key = new ArrayList<>(this.personWiseUniqueStayRegions.get(personId).keySet());
                    Map<Integer, Integer> tempList = new TreeMap<>();
                    stayRegionId = key.get(id - sum1);
                    tempList.putAll(this.personWiseUniqueStayRegions.get(personId));
                    tempList.remove(key.get(id - sum1));
                    this.personWiseUniqueStayRegions.get(personId).clear();
                    this.personWiseUniqueStayRegions.get(personId).putAll(tempList);
                    break;
                }
                sum1 += this.personWiseUniqueStayRegions.get(personId).size();

            }
//            ArrayList<MeetingPoint> removeList = new ArrayList<>();
            ListIterator<MeetingPoint> listIterator = seperatePeople.listIterator();
            while (listIterator.hasNext()) {
                MeetingPoint meetingPoint = listIterator.next();
                if (meetingPoint.getStayRegionId() == stayRegionId && meetingPoint.getPersonId().get(0) == personId) {
                    if (meetingPoint.getPersonId().size() == 1 &&
                            meetingPoint.getPersonId().get(0) == personId
                            && meetingPoint.getStayRegionId() == stayRegionId) {
                        listIterator.remove();
                    }
                }
            }
//            for (int i = 0; i < this.seperatePeople.size(); i++) {
//                MeetingPoint meetingPoint = this.seperatePeople.get(i);
//                if (meetingPoint.getStayRegionId() == stayRegionId && meetingPoint.getPersonId().get(0) == personId) {
////                    removeList.add(meetingPoint);
//                    if (this.seperatePeople.get(i).getPersonId().size() == 1 &&
//                            this.seperatePeople.get(i).getPersonId().get(0) == personId
//                            && this.seperatePeople.get(i).getStayRegionId() == stayRegionId) {
//                        this.seperatePeople.remove(i);
//                    }
//
//                }
//            }
//            for (int i = 0; i < this.justMeeting.size(); i++) {
//                if (this.justMeeting.get(i).getPersonId().contains(personId)
//                        && this.justMeeting.get(i).getStayRegionId() == stayRegionId) {
//                    this.justMeeting.remove(i);
//                }
//            }

            listIterator = justMeeting.listIterator();
            while (listIterator.hasNext()) {
                MeetingPoint meetingPoint = listIterator.next();
                if (meetingPoint.getPersonId().contains(personId)
                        && meetingPoint.getStayRegionId() == stayRegionId) {
                    listIterator.remove();
                }
            }
//            filter();
//            this.seperatePeople.removeAll(removeList);
            this.namesSeperat = setNames(this.personWiseUniqueStayRegions);

        } else {
            this.listOfUniqueStayRegions.remove(id);
            this.names.remove(id);

            ListIterator<MeetingPoint> listIterator = selectPeopleMeetings.listIterator();

            while (listIterator.hasNext()) {
                MeetingPoint meetingPoint = listIterator.next();
                if (meetingPoint.getStayRegionId() == id) {
                    listIterator.remove();
                }
            }
//        ArrayList<MeetingPoint> removeList = new ArrayList<>();
//            for (int i = 0; i < this.selectPeopleMeetings.size(); i++) {
//                MeetingPoint meetingPoint = this.selectPeopleMeetings.get(i);
//                if (meetingPoint.getStayRegionId() == id) {
//                    this.selectPeopleMeetings.remove(meetingPoint);
//
//                }
//            }
//            this.selectPeopleMeetings.removeAll(removeList);
        }
    }
//    public HashMap<Integer, Map<Integer,Integer>> getPersonWiseUniqueStayRegions() {
//        return personWiseUniqueStayRegions;
//    }

    public int numberOfUniquePlacesForAPerson(int i) {
        return this.personWiseUniqueStayRegions.get(i).size();
    }

    public int totalNumberOfRowsWhenSeperatePerson() {
        int i = 0;
        for (Map<Integer, Integer> list : this.personWiseUniqueStayRegions.values()) {
            i += list.size();
        }
        return i;
    }

    public ArrayList<Integer> listOfUniquePlacesVisitedByAPerson(int i) {
        return new ArrayList<>(this.personWiseUniqueStayRegions.get(i).keySet());
    }

    public int positionOfSelectedPersonInArrayList(int i) {
        return this.selectedPeople.indexOf(i);
    }

    public int getHighlightPerson() {
        return highlightPerson;
    }

    public void setHighlightPerson(int highlightPerson) {
        firePropertyChange("SetHighlight", this.highlightPerson, highlightPerson);
        this.highlightPerson = highlightPerson;
    }

    public int noOfTimes(int i) {
        return this.noOfTimesEachPlaceIsVisited.get(i);
    }

    public boolean isDiffRows() {
        return diffRows;
    }

    public void setDiffRows() {
        this.diffRows = !this.diffRows;
    }

    public List<MeetingPoint> getJustMeeting() {
        return justMeeting;
    }

    public LatitudeAndLongitude getCommonStayRegionCentreFromId(int id) {
        return this.group.getCommonStayRegionCentreFromId(id);
    }

    public ArrayList<StayPoint> getListOFStayPointsFromStayRegion(int id) {
        return new ArrayList<>(this.group.getCommonStayRegionList().get(id).getRegionStayPoints().values());
    }

    public void fireUpdate(){
        firePropertyChange("Person Centric",null,group);
    }
}

