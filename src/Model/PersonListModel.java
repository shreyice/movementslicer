package Model;

import Algorithms.DataRead;
import Algorithms.Person;

import java.io.File;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Shrey
 * Date: 10/11/13
 * Time: 4:23 AM
 * To change this template use File | Settings | File Templates.
 */
public class PersonListModel extends AbstractModel{

    ArrayList<Person> persons;


    public ArrayList<Person>  getPersons() {
        return persons;
    }

    public void setPersons(File []dirs) {
        ArrayList<Person> oldList = this.persons;
        this.persons = DataRead.loadData(dirs);
        System.out.print("Data Loaded");
        firePropertyChange("People List Loaded"  , oldList, this.persons);
    }

    public void setPersonsEval(File []dirs) {
        ArrayList<Person> oldList = this.persons;
        this.persons = DataRead.loadData(dirs);
        System.out.print("Data Loaded");
        firePropertyChange("Evaluation Loaded"  , oldList, this.persons);
    }
}
