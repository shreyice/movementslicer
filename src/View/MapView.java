package View;

import Algorithms.Constants;
import Algorithms.LatitudeAndLongitude;
import Algorithms.MeetingPoint;
import Controller.MapController;
import org.jdesktop.swingx.JXMapViewer;
import org.jdesktop.swingx.OSMTileFactoryInfo;
import org.jdesktop.swingx.input.PanKeyListener;
import org.jdesktop.swingx.input.PanMouseInputListener;
import org.jdesktop.swingx.input.ZoomMouseWheelListenerCursor;
import org.jdesktop.swingx.mapviewer.*;
import org.jdesktop.swingx.painter.CompoundPainter;

import javax.swing.event.MouseInputListener;
import java.io.File;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Shrey
 * Date: 11/18/13
 * Time: 7:36 PM
 * To change this template use File | Settings | File Templates.
 *
 * code example provided by swingLabs. Modified here
 */
public class MapView extends JXMapViewer{

    Set<MapMeetPoint> waypoints;
    MapController mapController;

    HashMap<Integer,ArrayList<GeoPosition>> list;
    HashMap<Integer,ArrayList<LatitudeAndLongitude>> listLnL;

    public MapView() {

        // Create a TileFactoryInfo for OpenStreetMap
        TileFactoryInfo info = new OSMTileFactoryInfo();
        DefaultTileFactory tileFactory = new DefaultTileFactory(info);
        this.setTileFactory(tileFactory);

        // Setup local file cache
        File cacheDir = new File(System.getProperty("user.home") + File.separator + ".jxmapviewer2");
        LocalResponseCache.installResponseCache(info.getBaseURL(), cacheDir, false);

//        // Use 8 threads in parallel to load the tiles
        tileFactory.setThreadPoolSize(16);

        // Set the focus
        GeoPosition  geoPosition= new GeoPosition(45.495503,-73.562869);
        this.setZoom(2);
        this.setAddressLocation(geoPosition);
        this.setMapSaturation(Constants.MAP_SATURATION_VALUE);

        // Add interactions
        MouseInputListener mia = new PanMouseInputListener(this);
        this.addMouseListener(mia);
        this.addMouseMotionListener(mia);
        this.addMouseWheelListener(new ZoomMouseWheelListenerCursor(this));
        this.addKeyListener(new PanKeyListener(this));

//        waypoints = new ArrayList<>(Arrays.asList(
//                new MapMeetPoint("Work", start)
//        ));
//        // Create a waypoint painter that takes all the waypoints
//        WaypointPainter< MapMeetPoint > waypointPainter = new WaypointPainter<MapMeetPoint>();
//        waypointPainter.setWaypoints(new HashSet<MapMeetPoint>(waypoints));
//        waypointPainter.setRenderer(new MapMeetPointRenderer());
//        this.setOverlayPainter(waypointPainter);
    }

    public void setMapController(MapController mapController) {
        this.mapController = mapController;
        updateWayPointList();
    }

    public void updateWayPointList() {

        this.list = new HashMap<>();
        this.listLnL = new HashMap<>();
        waypoints = new HashSet<>();
        Set<GeoPosition> geoPositions = new HashSet<>();
        Set<LatitudeAndLongitude> LnLPositions = new HashSet<>();
        try {
            for (int i = 0; i < this.mapController.listOfMeetingPointsToDisplay().size(); i++) {
                MeetingPoint meetPoint = this.mapController.listOfMeetingPointsToDisplay().get(i);
                DecimalFormat df = new DecimalFormat("0.00000");
                String format = df.format(meetPoint.getCenter().getLatitude());
                String format1 = df.format(meetPoint.getCenter().getLongitude());
                try {
                    double finalValue =  df.parse(format).doubleValue();
                    double finalValue1 = df.parse(format1).doubleValue();
                    geoPositions.add(new GeoPosition(finalValue, finalValue1));
                    LnLPositions.add(new LatitudeAndLongitude(finalValue, finalValue1));

                    this.waypoints.add(new MapMeetPoint("L "+Integer.toString(meetPoint.getStayRegionId())
                            , new GeoPosition(finalValue, finalValue1)));

                    // Create a track from the geo-positions
                    if(this.list.containsKey(meetPoint.getStayRegionId())) {
                        this.list.get(meetPoint.getStayRegionId()).add(new GeoPosition(finalValue, finalValue1));
                    }
                    else {
                        this.list.put(meetPoint.getStayRegionId(), new ArrayList<GeoPosition>());
                        this.list.get(meetPoint.getStayRegionId()).add(new GeoPosition(finalValue, finalValue1));
                    }

                    // Create a track from the latitude
                    if(this.listLnL.containsKey(meetPoint.getStayRegionId())) {
                        this.listLnL.get(meetPoint.getStayRegionId()).add(new LatitudeAndLongitude(finalValue, finalValue1));
                    }
                    else {
                        this.listLnL.put(meetPoint.getStayRegionId(), new ArrayList<LatitudeAndLongitude>());
                        this.listLnL.get(meetPoint.getStayRegionId()).add(new LatitudeAndLongitude(finalValue, finalValue1));
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }



            RoutePainter routePainter = new RoutePainter(list,listLnL);


            WaypointPainter<MapMeetPoint> waypointPainter = new WaypointPainter<>();
            waypointPainter.setWaypoints(waypoints);
            waypointPainter.setRenderer(new MapMeetPointRenderer());


            // Create a compound painter that uses both the route-painter and the waypoint-painter
            List<org.jdesktop.swingx.painter.Painter<JXMapViewer>> painters = new ArrayList<org.jdesktop.swingx.painter.Painter<JXMapViewer>>();
            painters.add(routePainter);
//            painters.add(waypointPainter);

            CompoundPainter<JXMapViewer> painter = new CompoundPainter<JXMapViewer>(painters);
            this.setOverlayPainter(painter);

//            this.setOverlayPainter(waypointPainter);
            this.calculateZoomFrom(geoPositions);
            this.invalidate();
            this.repaint();
        } catch (NullPointerException e) {
            System.out.println("No way Points");
        }
    }
}

