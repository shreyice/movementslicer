package View;

import Algorithms.MeetingPoint;
import Controller.MapController;
import org.jdesktop.swingx.JXMapViewer;
import org.jdesktop.swingx.OSMTileFactoryInfo;
import org.jdesktop.swingx.input.PanKeyListener;
import org.jdesktop.swingx.input.PanMouseInputListener;
import org.jdesktop.swingx.input.ZoomMouseWheelListenerCursor;
import org.jdesktop.swingx.mapviewer.*;

import javax.swing.event.MouseInputListener;
import java.io.File;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.HashSet;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: Shrey
 * Date: 11/27/13
 * Time: 8:49 AM
 * To change this template use File | Settings | File Templates.
 */
public class MiniMapView extends JXMapViewer{

        Set<MapMeetPoint> waypoints;
        MapController mapController;


        public MiniMapView(double latitude, double longitude) {

            // Create a TileFactoryInfo for OpenStreetMap
            TileFactoryInfo info = new OSMTileFactoryInfo();
            DefaultTileFactory tileFactory = new DefaultTileFactory(info);
            this.setTileFactory(tileFactory);

            // Setup local file cache
            File cacheDir = new File(System.getProperty("user.home") + File.separator + ".jxmapviewer2");
            LocalResponseCache.installResponseCache(info.getBaseURL(), cacheDir, false);

//        // Use 8 threads in parallel to load the tiles
//        tileFactory.setThreadPoolSize(8);

            // Set the focus
            GeoPosition start = new GeoPosition(latitude,longitude);
            this.setZoom(2);
            this.setAddressLocation(start);


//            Add interactions
            MouseInputListener mia = new PanMouseInputListener(this);
            this.addMouseListener(mia);
            this.addMouseMotionListener(mia);
            this.addMouseWheelListener(new ZoomMouseWheelListenerCursor(this));
            this.addKeyListener(new PanKeyListener(this));

//        waypoints = new ArrayList<>(Arrays.asList(
//                new MapMeetPoint("Work", start)
//        ));
//        // Create a waypoint painter that takes all the waypoints
//        WaypointPainter< MapMeetPoint > waypointPainter = new WaypointPainter<MapMeetPoint>();
//        waypointPainter.setWaypoints(new HashSet<MapMeetPoint>(waypoints));
//        waypointPainter.setRenderer(new MapMeetPointRenderer());
//        this.setOverlayPainter(waypointPainter);
        }


    }


