/*
 * WaypointRenderer.java
 *
 * Created on March 30, 2006, 5:24 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package View;

import Algorithms.Constants;
import com.sun.corba.se.impl.orbutil.closure.Constant;
import org.jdesktop.swingx.JXMapViewer;
import org.jdesktop.swingx.mapviewer.WaypointRenderer;
import java.awt.*;
import java.awt.geom.Point2D;


/**
 * A fancy waypoint painter
 * @author Martin Steiger
 */
public class MapMeetPointRenderer implements WaypointRenderer<MapMeetPoint>
{


	/**
	 * Uses a default waypoint image
	 */
	public MapMeetPointRenderer()
	{
	}

    @Override
    public void paintWaypoint(Graphics2D g, JXMapViewer viewer, MapMeetPoint w) {

        //To change body of implemented methods use File | Settings | File Templates.
        Point2D point = viewer.getTileFactory().geoToPixel(w.getPosition(), viewer.getZoom());
        int x = (int)point.getX();
        int y = (int)point.getY();

//        System.out.println(x+"  "+y);

        String label = w.getLabel();

//		g.setFont(font);

//		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

//        g.drawString(label, x , y );
//        g.setPaint(Color.ORANGE);
        g.setPaint(new Color(128,128,128));
//          g.fillOval(x-20,y-20,40,40);
        g.fillOval(x - 2*Constants.MAP_LOCATION_RADIUS, y - 2*Constants.MAP_LOCATION_RADIUS, 2 * Constants.MAP_LOCATION_RADIUS, 2 * Constants.MAP_LOCATION_RADIUS);
        g.fillRect(x - Constants.MAP_LOCATION_RADIUS , y - Constants.MAP_LOCATION_RADIUS, Constants.MAP_LOCATION_RADIUS, Constants.MAP_LOCATION_RADIUS);
        g.setPaint(Color.BLACK);

        FontMetrics metrics = g.getFontMetrics();

        int ascent = metrics.getAscent();
        int width = metrics.stringWidth(label)/2;
        g.setPaint(Color.BLACK);
        g.drawString(label, x -15 - width , y-15+ascent/2);
//        g.dispose();
    }
}


