package View;

import Algorithms.Constants;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Shrey on 2/17/14.
 */
public class Label extends JTextField {

    int id;
    String name;

    public Label(int id,String name, int x, int y,int width) {
        super(name);
        this.id = id;
           this.name =name;

        this.setBackground(Constants.BACKGROUND_COLOR);
//        this.setMinimumSize(new Dimension(40,40));
        this.setBounds(x,y +4,width, Constants.BAR_WIDTH - 4);
//        this.setLocation(x,y);
        this.setBorder(null);
        this.setVisible(true);
        Font font = new Font("Tamoha", Font.BOLD, (int) Constants.TEXT_SIZE - 6);
        this.setFont(font);
        this.setText(name);
    }
}
