package View;

import Algorithms.Constants;
import Algorithms.MeetingPoint;
import Algorithms.StayPoint;
import Controller.GroupGanttController;
import javafx.scene.shape.Polyline;
import org.joda.time.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;
/**
 * Created with IntelliJ IDEA.
 * User: Shrey
 * Date: 10/20/13
 * Time: 4:01 AM
 * To change this template use File | Settings | File Templates.
 * The initial idea for the timeLine (http://tillnagel.com/2012/06/animated-time-range-slider/)
 * was inspired by code from Till Nagel (http://tillnagel.com/).
 */

public class GroupGanttView extends JPanel implements
        MouseWheelListener
        , MouseMotionListener
        , MouseListener
        , KeyListener {

    GroupGanttController groupGanttController;
    AnimationGantt animationGantt = new AnimationGantt(this);

    HashMap<Integer, ArrayList<StayPoint>> connectingLine;

    double currentAniSlope = 60.0;
    double finalAniSlope = 4.0;
    int initialAlpha = 0;
    int finalAlpha = 0;

    float dash1[] = {3.0f};

    double currentStartDate;
    double currentEndDate;
    double finalAutoFrameStart;
    double finalAutoFrameEnd;
    double finalStartMillis;
    double finalEndMillis;

    double totalMillis;
    double widthPerMilli;
    double millisPerPixel;

    int foldShades = 3;

    int useOfColors = 0;

    int typeOfLabeling = 0;

    int listStart;
    int listEnd;

    int stringHeight;
    int dateWidth = Constants.PADDING_HORIZONTAL_LEFT;
    int dateHeight;
    boolean writeStayPointNames = false;
    ArrayList<Rectangle.Double> rectangle;
    ArrayList<Polygon> polygons;
    ArrayList<MeetingPoint> drawnMeet;
    MeetingPoint currentSelectedMeet = null;
    MeetingPoint oldSelectedMeet = null;

    int indexPolygon = -1;
    ArrayList<Double> finalDiffMillis;
    ArrayList<Double> diffMillis;
    ArrayList<Integer> animatePoint;
    ArrayList<Double> folds;
    private int mouseX;
    private int mouseY;
    private int previousMouseX;
    private int previousMouseY;

    int rulerStart = -1;
    int rulerEnd = -1;

    double mouseStartDate = -1;
    double mouseEndDate = -1;

    boolean drawRuler = false;
    boolean animation = false;
    boolean autoFrame = false;
    boolean autoFrameWithFolds = false;
    boolean autoFrameWithUnFold = false;
    boolean frameVisible = false;
    boolean frameVisibleWithFold = false;
    boolean frameVisibleWithUnFold = false;
    boolean connectingLines = false;
    boolean overRideBarWidth = false;
    boolean zoomAnimate = false;

    boolean meetLines = false;

    BufferedImage off_Image;
    Rectangle2D.Double display;
    Ticks ticks;

    int max = 0;

    public GroupGanttView() {
        this.addMouseWheelListener(this);
        this.addMouseMotionListener(this);
        this.addMouseListener(this);
        this.addKeyListener(this);
        this.setFocusable(true);
    }

    public void initialize() {
        this.folds = new ArrayList<>();
        this.diffMillis = new ArrayList<>();
        this.finalDiffMillis = new ArrayList<>();
        this.animatePoint = new ArrayList<>();
        double temp = 0.0;
        for (int i = 0; i < this.groupGanttController.getMeetingPointsToDisplay().size(); i++) {
            this.diffMillis.add(0.0);
            this.finalDiffMillis.add(temp);
        }
    }

    public void recalculateSize(){
        int numberOfRows;
        numberOfRows = this.groupGanttController.totalNumberOfRows();
        int height = Constants.PADDING_VERTICAL * 2 + numberOfRows * Constants.BAR_WIDTH;
        this.setPreferredSize(new Dimension(this.getWidth(), height));
        this.getParent().revalidate();

    }

    @Override
    public void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);
        Graphics2D graphics2D = (Graphics2D) graphics;
        try {
            this.ticks = new Ticks(this);
            off_Image = new BufferedImage(this.getWidth(), this.getHeight(), BufferedImage.TYPE_INT_ARGB);
            graphics2D = off_Image.createGraphics();
            graphics2D.setRenderingHint(
                    RenderingHints.KEY_TEXT_ANTIALIASING
                    , RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            Font font = new Font("Tamoha", Font.BOLD, (int) Constants.TEXT_SIZE - 4);
            graphics.setFont(font);
            FontMetrics metrics = graphics.getFontMetrics(font);
            // get the height of a line of text in this
            // font and render context
            stringHeight = metrics.getHeight();
            dateHeight = metrics.stringWidth(Constants.DATE_PATTERN);
            dateWidth = metrics.stringWidth("H");
            max = 0;
            for (int i = 0; i < this.groupGanttController.totalNumberOfRows(); i++) {
                int stringWidth = metrics.stringWidth(this.groupGanttController.getName(i));
                if(max < stringWidth) max = stringWidth;
            }

            if(!this.groupGanttController.isDiffRows()){
                Constants.PADDING_HORIZONTAL_LEFT =  2 * this.dateWidth + max;
            }
            else {
                Constants.PADDING_HORIZONTAL_LEFT =  2 * this.dateWidth + max + metrics.stringWidth("Person   ");
            }
            this.totalMillis = this.currentEndDate - this.currentStartDate;
            this.widthPerMilli = (this.getVisibleRect().getWidth() - Constants.PADDING_HORIZONTAL_LEFT - Constants.PADDING_HORIZONTAL_RIGHT - 2 * this.dateWidth) / this.totalMillis;
            this.millisPerPixel = this.totalMillis / (this.getVisibleRect().getWidth() - 2 * this.dateWidth - Constants.PADDING_HORIZONTAL_LEFT - Constants.PADDING_HORIZONTAL_RIGHT);
            this.setBackground(Constants.BACKGROUND_COLOR);
            updateFolds();
//            if(this.foldShades == 0 )
            shadeDayAndNight(graphics2D);
            drawGanttChart(graphics2D);
            graphics.setColor(Color.black);
            drawXFrame(graphics2D);
            if(this.connectingLines)
                drawConnectingLines(graphics2D);   // does not follow folds
            if (this.drawRuler) {
                drawRuler(graphics2D);
            }
            drawStayPoint(graphics2D);
            drawStayRegionNames(graphics2D);
            if(this.groupGanttController.isDiffRows()  && this.meetLines)
                drawLinesMeet(graphics2D);
            graphics2D.setPaint(Constants.BACKGROUND_COLOR);
            graphics2D.fill(new Rectangle(
                    (int) this.getVisibleRect().getX()
                    , (int) (this.getVisibleRect().getY() + this.getVisibleRect().getHeight() - Constants.PADDING_VERTICAL + 1)
                    , (int) this.getVisibleRect().getWidth()
                    , Constants.PADDING_VERTICAL));
            graphics2D.setPaint(Color.black);
            graphics2D.setPaint(Constants.BACKGROUND_COLOR);
            graphics2D.fill(new Rectangle(
                    (int) this.getVisibleRect().getX()
                    , (int) (this.getVisibleRect().getY())
                    ,(int) this.getVisibleRect().getWidth()
                    , Constants.PADDING_VERTICAL - 1));
            if (this.currentSelectedMeet != null) {
                drawMeetingPointAnnotation(graphics2D, this.currentSelectedMeet);
            }
            if(this.foldShades >= 1 )
                shadeDayAndNight(graphics2D);
            drawGraphTicks(graphics2D);
            drawMouseFollowingTime(graphics2D);
            graphics2D.setPaint(Constants.BACKGROUND_COLOR);
            drawDateTimeLabelsFold(graphics2D);
            drawClearingRectangle(graphics2D);
            if(this.groupGanttController.isDiffRows()){
                drawPersonNameLabels(graphics2D);
            }
            drawDateTimeLabels(graphics2D);
            graphics.drawImage(off_Image, 0, 0, this);
        } catch (Exception e) {
                System.out.println("Paint Component Exception");
            this.removeAll();
            graphics2D.setPaint(Constants.BACKGROUND_COLOR);
            graphics2D.fill(this.getVisibleRect());
        }
    }

    void drawPersonNameLabels(Graphics2D graphics2D ) {

        graphics2D.setPaint(Color.black);
        Font font = new Font("Tamoha", Font.BOLD, (int) Constants.TEXT_SIZE - 4);
        graphics2D.setFont(font);
        FontMetrics metrics = graphics2D.getFontMetrics(font);
        int currentY = this.getHeight() - Constants.PADDING_VERTICAL
                    - (this.groupGanttController.totalNumberOfRowsWhenSeperatePerson() * Constants.BAR_WIDTH);

        for (int i = 0; i < this.groupGanttController.selectedPeople().size(); i++) {
            int personId = this.groupGanttController.selectedPeople().get(i);

            int start = currentY;
            int end = currentY + (Constants.BAR_WIDTH * this.groupGanttController.numberOfUniquePlacesForAPerson(personId));

            //graphics2D.drawString("ASSAS",0,(int)this.getVisibleRect().getMinY() + Constants.PADDING_VERTICAL);
            //graphics2D.drawString("ASSAS",0,s + Constants.PADDING_VERTICAL);
            if(start < this.getVisibleRect().getMinY() + Constants.PADDING_VERTICAL) {
                start = (int) this.getVisibleRect().getY() + Constants.PADDING_VERTICAL;
            }

            if(end > this.getVisibleRect().getMaxY() - Constants.PADDING_VERTICAL) {
                end = (int) this.getVisibleRect().getMaxY() - Constants.PADDING_VERTICAL;
            }
            if(this.useOfColors == 0) {
                graphics2D.setColor(Constants.PEOPLE_COLOR[personId % Constants.PEOPLE_COLOR.length]);
            }


            int stringY = (start+ (end - start)/2);

            if(stringY > this.getVisibleRect().getY() + Constants.PADDING_VERTICAL
                    && stringY < this.getVisibleRect().getMaxY() - Constants.PADDING_VERTICAL
            && end - start > graphics2D.getFontMetrics().getHeight()+2) {
                String name;
                if(Constants.PEOPLE_NAMES.length > 6) {
                    name = Constants.PEOPLE_NAMES[personId] + " " + Integer.toString(this.groupGanttController.selectedPeople().get(i));
                }
                else {
                    name = Constants.PEOPLE_NAMES[personId] + " " ;
                }
                graphics2D.drawString(name, 0, stringY);
            }
            currentY = currentY + Constants.BAR_WIDTH * this.groupGanttController.numberOfUniquePlacesForAPerson(personId);
            graphics2D.setStroke(new BasicStroke(1.2f));
            graphics2D.setColor(Color.black);

            if(currentY > this.getVisibleRect().getY() + Constants.PADDING_VERTICAL && currentY < this.getVisibleRect().getMaxY() - Constants.PADDING_VERTICAL )
                graphics2D.drawLine(0,currentY,Constants.PADDING_HORIZONTAL_LEFT + this.dateWidth ,currentY);
        }
        graphics2D.setStroke(new BasicStroke());

    }

    public void drawLinesMeet(Graphics2D graphics2D) {
        double currentStartX = 0;
        double currentEndX = 0;
        double currentStartMilli = 0;
        double currentEndMilli = 0;
        double currentY = 0;
        graphics2D.setColor(Color.black);
        graphics2D.setStroke(new BasicStroke(2.0f));
        currentY = this.getHeight() - Constants.PADDING_VERTICAL
                - (this.groupGanttController.totalNumberOfRowsWhenSeperatePerson() * Constants.BAR_WIDTH);
        display = new Rectangle.Double(Constants.PADDING_HORIZONTAL_LEFT + this.dateWidth
                , currentY
                , this.getWidth() - Constants.PADDING_HORIZONTAL_LEFT - Constants.PADDING_HORIZONTAL_RIGHT - 2* this.dateWidth
                , (this.groupGanttController.totalNumberOfRowsWhenSeperatePerson() * Constants.BAR_WIDTH)
        );
        for (int i = 0; i < this.groupGanttController.getJustMeeting().size(); ++i) {
            MeetingPoint meetingPoint = this.groupGanttController.getJustMeeting().get(i);
            currentStartMilli = meetingPoint.getInterval().getStart().getMillis();
            currentEndMilli = meetingPoint.getInterval().getEnd().getMillis();
            currentStartX = pixelAtMillis(currentStartMilli);
            currentEndX = pixelAtMillis(currentEndMilli);
            int[] x = new int[meetingPoint.getStayPoints().size()];
            int[] y = new int[meetingPoint.getStayPoints().size()];
            if ((currentEndX - currentStartX) < 1)
                currentEndX += 1;
            for (int j = 0; j < meetingPoint.getStayPoints().size(); j++) {
                x[j] = (int)(currentStartX + (currentEndX - currentStartX)/2.0);
                double yPos = currentY;
                for (int k = 0; k < this.groupGanttController.selectedPeople().indexOf(meetingPoint.getStayPoints().get(j).getPersonId())
                        ; k++) {
                    yPos += Constants.BAR_WIDTH * this.groupGanttController.numberOfUniquePlacesForAPerson(
                            this.groupGanttController.selectedPeople().get(k)
                    );
                }
                y[j] = (int)(yPos + Constants.BAR_WIDTH * this.groupGanttController.listOfUniquePlacesVisitedByAPerson(
                        meetingPoint.getStayPoints().get(j).getPersonId()).indexOf(meetingPoint.getStayRegionId())+ Constants.BAR_WIDTH/2);
                graphics2D.fillOval(x[j]-4,y[j]-4,8,8);
            }
            graphics2D.drawPolyline(x,y,y.length);
        }
    }

    public void drawConnectingLines(Graphics2D graphics2D) {

        //with folds
//        drawConnectingLinesWithFold(graphics2D);

        //withour folds
        drawConnectingLinesWithoutFold(graphics2D);
    }

    public void drawConnectingLinesWithoutFold(Graphics2D graphics2D) {
        this.connectingLine = new HashMap<>();
        for (int i = 0; i < this.drawnMeet.size(); i++) {
            MeetingPoint meetingPoint = this.drawnMeet.get(i);
            for (int j : meetingPoint.getPersonId()) {
                if (!this.connectingLine.containsKey(j)) {
                    this.connectingLine.put(j, new ArrayList<StayPoint>());
                }
            }
            for (int k = 0; k < meetingPoint.getStayPoints().size(); k++) {
                StayPoint s = meetingPoint.getStayPoints().get(k);
                this.connectingLine.get(s.getPersonId()).add(s);
            }
        }

        double currentStartX;
        double currentEndX;
        double currentStartMillis = 0;
        double currentEndMillis = 0;
        double currentY = (this.groupGanttController.isDiffRows()) ?
                this.getHeight() - Constants.PADDING_VERTICAL -
                        (this.groupGanttController.totalNumberOfRowsWhenSeperatePerson() * Constants.BAR_WIDTH)
                : this.getHeight() - Constants.PADDING_VERTICAL
                - (this.groupGanttController.numberOfUniquePlacesSelectedPeopleMeet() * Constants.BAR_WIDTH);
        double yPos = currentY;
        int spacing = (this.groupGanttController.isDiffRows())
                ?
                Constants.BAR_WIDTH / 2
                :
                Constants.BAR_WIDTH / (this.groupGanttController.numberOfPeopleSelected() + 1);
        for (int i : this.connectingLine.keySet()) {
            if(this.useOfColors == 0) {
                graphics2D.setColor(Constants.PEOPLE_COLOR[i % Constants.PEOPLE_COLOR.length]);
            }
            ArrayList<StayPoint> list = this.connectingLine.get(i);
            if (this.groupGanttController.isDiffRows()) {
                for (int k = 0; k < this.groupGanttController.selectedPeople().indexOf(i)
                        ; k++) {
                    yPos += Constants.BAR_WIDTH * this.groupGanttController.numberOfUniquePlacesForAPerson(
                            this.groupGanttController.selectedPeople().get(k));
                }
            }
            yPos += spacing;

            for (int j = 0; j < list.size() - 1; j++) {

                currentStartMillis = list.get(j).getDepartureDateTime().getMillis();
                currentEndMillis = list.get(j + 1).getArrivalDateTime().getMillis();
                currentStartX = pixelAtMillis(currentStartMillis);
                currentEndX = pixelAtMillis(currentEndMillis);
                if (this.groupGanttController.isDiffRows())
                    graphics2D.drawLine((int) currentStartX
                            , (int) yPos + Constants.BAR_WIDTH * this.groupGanttController.listOfUniquePlacesVisitedByAPerson(i).indexOf(
                            list.get(j).getCommonStayRegionId())
                            , (int) currentEndX
                            , (int) yPos + Constants.BAR_WIDTH * this.groupGanttController.listOfUniquePlacesVisitedByAPerson(i).indexOf(
                            list.get(j + 1).getCommonStayRegionId()));

                else {
                    graphics2D.drawLine((int) currentStartX
                            , (int) yPos + spacing * this.groupGanttController.selectedPeople().indexOf(i) + Constants.BAR_WIDTH * this.groupGanttController.listOfStayRegionsForMeetings().indexOf(list.get(j).getCommonStayRegionId())
                            , (int) currentEndX
                            , (int) yPos + spacing * this.groupGanttController.selectedPeople().indexOf(i) + Constants.BAR_WIDTH * this.groupGanttController.listOfStayRegionsForMeetings().indexOf(list.get(j + 1).getCommonStayRegionId()));
                }

            }
            yPos = currentY;
        }
    }

    public void shadeDayAndNight(Graphics2D graphics2D) {

        double currentStartX;
        double currentEndX;
        double currentStartMilli = 0;
        double currentEndMilli = 0;
        DateTime currentStartDateTime = new DateTime((long) this.currentStartDate).minusDays(3).withTimeAtStartOfDay().plusHours(6);
        DateTime currentEndDateTime = new DateTime((long) (this.currentEndDate + this.finalDiffMillis.get(this.finalDiffMillis.size() - 1)));
        int currentY;
        double height;
        if (this.groupGanttController.isDiffRows()) {
            currentY = this.getHeight() - Constants.PADDING_VERTICAL
                    - (this.groupGanttController.totalNumberOfRowsWhenSeperatePerson() * Constants.BAR_WIDTH);
            height = (this.groupGanttController.totalNumberOfRowsWhenSeperatePerson() * Constants.BAR_WIDTH);
        } else {
            currentY = this.getHeight() - Constants.PADDING_VERTICAL
                    - (this.groupGanttController.numberOfUniquePlacesSelectedPeopleMeet() * Constants.BAR_WIDTH);
            height = (this.groupGanttController.numberOfUniquePlacesSelectedPeopleMeet() * Constants.BAR_WIDTH);
        }

        while (currentStartDateTime.isBefore(currentEndDateTime)) {
            currentStartMilli = currentStartDateTime.getMillis();
            currentStartDateTime = currentStartDateTime.plusHours(12);
            currentEndMilli = currentStartDateTime.getMillis();
            currentStartX = pixelAtMillis(currentStartMilli);
            currentEndX = pixelAtMillis(currentEndMilli);
            if(foldShades == 0) {
                graphics2D.setColor(new Color(220, 220, 220));
                graphics2D.fillRect((int) currentStartX, currentY, (int) (currentEndX - currentStartX), (int) height);
                currentStartMilli = currentStartDateTime.getMillis();
                currentStartDateTime = currentStartDateTime.plusHours(12);
                currentEndMilli = currentStartDateTime.getMillis();
                currentStartX = pixelAtMillis(currentStartMilli);
                currentEndX = pixelAtMillis(currentEndMilli);
                graphics2D.setColor(new Color(180, 189, 200));
                graphics2D.fillRect((int) currentStartX, currentY, (int) (currentEndX - currentStartX), (int) height);

            }
            else if(foldShades == 1) {

                graphics2D.setColor(new Color(  200, 198, 180));

                graphics2D.fillRect((int) currentStartX, (int) (this.getVisibleRect().getMaxY() - Constants.PADDING_VERTICAL + 3), (int) (currentEndX - currentStartX), Constants.PADDING_VERTICAL);
                currentStartMilli = currentStartDateTime.getMillis();
                currentStartDateTime = currentStartDateTime.plusHours(12);
                currentEndMilli = currentStartDateTime.getMillis();
                currentStartX = pixelAtMillis(currentStartMilli);
                currentEndX = pixelAtMillis(currentEndMilli);
                graphics2D.setColor(new Color( 180, 189, 200));

                graphics2D.fillRect((int) currentStartX, (int) (this.getVisibleRect().getMaxY() - Constants.PADDING_VERTICAL + 3), (int) (currentEndX - currentStartX), Constants.PADDING_VERTICAL);

            }
            else if (    foldShades == 2) {

                graphics2D.setColor(new Color(255, 255, 255));
                graphics2D.fillRect((int) currentStartX, (int) this.getVisibleRect().getY(), (int) (currentEndX - currentStartX), (int) (currentY - this.getVisibleRect().getY()));
                currentStartDateTime = currentStartDateTime.plusHours(12);
            }
            else if(foldShades == 3) {
                graphics2D.setColor(new Color(255, 255, 255));
                graphics2D.fillRect((int) currentStartX, (int) (this.getVisibleRect().getY()+ this.getVisibleRect().getHeight() - Constants.PADDING_VERTICAL +3), (int) (currentEndX - currentStartX), Constants.PADDING_VERTICAL);
                currentStartDateTime = currentStartDateTime.plusHours(12);
            }
        }
//        if(foldShades == 0)
//            if (this.folds.size() != 0) {
//                for (int i = 0; i < this.folds.size() - 1; i = i + 2) {
//                    Rectangle.Double r = new Rectangle.Double(this.folds.get(i), currentY, this.folds.get(i + 1) - this.folds.get(i), height);
//                    if (this.getVisibleRect().intersects(r)) {
//                        graphics2D.setColor(new Color(190, 190, 190));
//                        graphics2D.fill(r);
//                    }
//                }
//            }
    }

    public void updateFolds() {
        this.folds = new ArrayList<>();
        for (int temp = 0; temp < this.animatePoint.size(); temp = temp + 2) {
            MeetingPoint meetingPoint = this.groupGanttController.getMeetingPointsToDisplay().get(this.animatePoint.get(temp));
            MeetingPoint meetingPoint1 = this.groupGanttController.getMeetingPointsToDisplay().get(this.animatePoint.get(temp + 1));
            double currentStartMilli = meetingPoint.getInterval().getEnd().getMillis() - this.currentStartDate;
            double currentEndMilli = meetingPoint1.getInterval().getStart().getMillis() - this.currentStartDate;
            double currentStartX = Constants.PADDING_HORIZONTAL_LEFT + this.dateWidth + this.widthPerMilli * currentStartMilli - this.widthPerMilli * this.diffMillis.get(this.animatePoint.get(temp));
            double currentEndX = Constants.PADDING_HORIZONTAL_LEFT + this.dateWidth + this.widthPerMilli * currentEndMilli - this.widthPerMilli * this.diffMillis.get(this.animatePoint.get(temp + 1));
            folds.add(currentStartX);
            folds.add(currentEndX);
        }
    }

    void drawGraphTicks(Graphics2D graphics2D) {
        graphics2D.setPaint(Color.black);
        double startMillis;
        double endMillis;

        if (this.animatePoint.size() <= 0) {
            startMillis = this.currentStartDate;
            endMillis = this.currentEndDate;
            ticks.drawTicks(startMillis
                    , endMillis
                    , (Constants.PADDING_HORIZONTAL_LEFT + this.dateWidth)
                    , (int)(this.getVisibleRect().getWidth() - (Constants.PADDING_HORIZONTAL_RIGHT + this.dateWidth))
                    , graphics2D);
        } else {
            startMillis = this.currentStartDate;
            endMillis = this.groupGanttController.getMeetingPointsToDisplay().get(this.animatePoint.get(0)).getInterval().getEnd().getMillis();
            int start = Constants.PADDING_HORIZONTAL_LEFT - this.dateWidth;
            int end = (int) (double) this.folds.get(0);
            ticks.drawTicks(startMillis
                    , endMillis
                    , start
                    , end
                    , graphics2D);
            startMillis = this.groupGanttController.getMeetingPointsToDisplay().get(this.animatePoint.get(this.animatePoint.size() - 1)).getInterval().getStart().getMillis();
            endMillis = this.currentEndDate + this.diffMillis.get(this.diffMillis.size() - 1);
            start = (int) (double) this.folds.get(this.folds.size() - 1);
            end =  (int)(this.getVisibleRect().getWidth() - this.dateWidth - Constants.PADDING_HORIZONTAL_RIGHT);

            ticks.drawTicks(startMillis
                    , endMillis
                    , start
                    , end
                    , graphics2D);

            for (int i = 1; i < this.animatePoint.size() - 1; i = i + 2) {
                startMillis = millisAtPixel(this.folds.get(i));
                endMillis = millisAtPixel(this.folds.get(i + 1));
                start = (int) (double) this.folds.get(i);
                end = (int) (double) this.folds.get(i + 1);
                ticks.drawTicks(startMillis, endMillis, start, end, graphics2D);
                startMillis = millisAtPixel(this.folds.get(i - 1));
                endMillis = millisAtPixel(this.folds.get(i));
                start = (int) (double) this.folds.get(i - 1);
                end = (int) (double) this.folds.get(i);
//                ticks.drawTicks(startMillis, endMillis, start, end, graphics2D);
            }
        }
    }

    void drawRuler(Graphics2D graphics2D) {
        double currentY = this.getHeight() - Constants.PADDING_VERTICAL
                - (this.groupGanttController.numberOfUniquePlacesSelectedPeopleMeet() * Constants.BAR_WIDTH);
        long min;
        if (Constants.PIXEL_TYPE_RULER) {
            graphics2D.setStroke(new BasicStroke(2.0F));
            graphics2D.setPaint(new Color(6, 125, 151, 100));
            if (this.rulerStart < this.rulerEnd) {

                graphics2D.fillRect(this.rulerStart
                        , (int) currentY
                        , this.rulerEnd - this.rulerStart
                        , this.groupGanttController.numberOfUniquePlacesSelectedPeopleMeet() * Constants.BAR_WIDTH);
                graphics2D.drawRect(this.rulerStart
                        , (int) currentY
                        , this.rulerEnd - this.rulerStart
                        , this.groupGanttController.numberOfUniquePlacesSelectedPeopleMeet() * Constants.BAR_WIDTH);
            } else {

                graphics2D.fillRect(this.rulerEnd
                        , (int) currentY
                        , this.rulerStart - this.rulerEnd
                        , this.groupGanttController.numberOfUniquePlacesSelectedPeopleMeet() * Constants.BAR_WIDTH);
                graphics2D.drawRect(this.rulerEnd
                        , (int) currentY
                        , this.rulerStart - this.rulerEnd
                        , this.groupGanttController.numberOfUniquePlacesSelectedPeopleMeet() * Constants.BAR_WIDTH);

            }

            graphics2D.setPaint(new Color(0, 0, 0));
            graphics2D.setStroke(new BasicStroke());
//            min = Minutes.minutesBetween(new DateTime((long) startMillis), new DateTime((long) endMillis)).getMinutes();
//            graphics2D.drawString(Long.toString(min)
//                    ,(int)(this.rulerStart + (this.rulerEnd - this.rulerStart)/2)
//                    ,(int)(currentY + (this.groupGanttController.numberOfUniquePlacesSelectedPeopleMeet() * Constants.BAR_WIDTH)/2.0));
            long start = (long) millisAtPixel(this.rulerStart);
            long end = (long) millisAtPixel(this.rulerEnd);

            min = Minutes.minutesBetween(new DateTime(start), new DateTime(end)).getMinutes();
            graphics2D.drawString(new DateTime(start).withZone(
                    DateTimeZone.forID(Constants.TIMEZONE_TO_DISPLAY)).toString(Constants.DATE_PATTERN)
                    , this.rulerStart - this.dateHeight
                    , this.getHeight() - Constants.PADDING_VERTICAL);
            graphics2D.drawString(new DateTime(end).withZone(
                    DateTimeZone.forID(Constants.TIMEZONE_TO_DISPLAY)).toString(Constants.DATE_PATTERN)
                    , this.rulerEnd
                    , this.getHeight() - Constants.PADDING_VERTICAL);
            graphics2D.drawString(Long.toString(Math.abs(min)) + " mins"
                    , this.rulerStart + (this.rulerEnd - this.rulerStart) / 2 - graphics2D.getFontMetrics().stringWidth(Long.toString(Math.abs(min)) + " mins ") / 2
                    , (int) (currentY + (this.groupGanttController.numberOfUniquePlacesSelectedPeopleMeet() * Constants.BAR_WIDTH) / 2.0));
        } else {
            graphics2D.setStroke(new BasicStroke(2.0F));
            graphics2D.setPaint(new Color(6, 125, 151, 100));
            double startPixel = pixelAtMillis(this.mouseStartDate);
            double endPixel = pixelAtMillis(this.mouseEndDate);

            if (this.mouseStartDate < this.mouseEndDate) {
                graphics2D.fillRect((int) startPixel
                        , (int) currentY
                        , (int) (endPixel - startPixel)
                        , this.groupGanttController.numberOfUniquePlacesSelectedPeopleMeet() * Constants.BAR_WIDTH);
                graphics2D.drawRect((int) startPixel
                        , (int) currentY
                        , (int) (endPixel - startPixel)
                        , this.groupGanttController.numberOfUniquePlacesSelectedPeopleMeet() * Constants.BAR_WIDTH);
            } else {
                graphics2D.fillRect((int) endPixel
                        , (int) currentY
                        , (int) (startPixel - endPixel)
                        , this.groupGanttController.numberOfUniquePlacesSelectedPeopleMeet() * Constants.BAR_WIDTH);
                graphics2D.drawRect((int) endPixel
                        , (int) currentY
                        , (int) (startPixel - endPixel)
                        , this.groupGanttController.numberOfUniquePlacesSelectedPeopleMeet() * Constants.BAR_WIDTH);

            }

            graphics2D.setPaint(new Color(0, 0, 0));
            graphics2D.setStroke(new BasicStroke());
//            min = Minutes.minutesBetween(new DateTime((long) startMillis), new DateTime((long) endMillis)).getMinutes();
//            graphics2D.drawString(Long.toString(min)
//                    ,(int)(this.rulerStart + (this.rulerEnd - this.rulerStart)/2)
//                    ,(int)(currentY + (this.groupGanttController.numberOfUniquePlacesSelectedPeopleMeet() * Constants.BAR_WIDTH)/2.0));
            long start = (long) this.mouseStartDate;
            long end = (long) this.mouseEndDate;

            min = Minutes.minutesBetween(new DateTime(start), new DateTime(end)).getMinutes();
            graphics2D.drawString(new DateTime(start).withZone(
                    DateTimeZone.forID(Constants.TIMEZONE_TO_DISPLAY)).toString(Constants.DATE_PATTERN)
                    , (int) startPixel
                    , this.getHeight() - Constants.PADDING_VERTICAL);
            graphics2D.drawString(new DateTime(end).withZone(
                    DateTimeZone.forID(Constants.TIMEZONE_TO_DISPLAY)).toString(Constants.DATE_PATTERN)
                    , (int) endPixel
                    , this.getHeight() - Constants.PADDING_VERTICAL);
            graphics2D.drawString(Long.toString(Math.abs(min)) + " mins"
                    , (int) (startPixel + (endPixel - startPixel) / 2) - graphics2D.getFontMetrics().stringWidth(Long.toString(Math.abs(min)) + " mins ") / 2
                    , (int) (currentY + (this.groupGanttController.numberOfUniquePlacesSelectedPeopleMeet() * Constants.BAR_WIDTH) / 2.0));
        }
    }

    void drawMouseFollowingTime(Graphics2D graphics2D) {
//        graphics2D.setStroke(new BasicStroke(1.0f,
//                BasicStroke.CAP_BUTT,
//                BasicStroke.JOIN_MITER,
//                10.0f, dash1, 0.0f));
        graphics2D.setStroke(new BasicStroke());
        graphics2D.setPaint(Color.BLACK);
        Font font = new Font("Tamoha", Font.BOLD, (int) Constants.TEXT_SIZE - 4);
        graphics2D.setFont(font);
//        int pixel = this.mouseX - (Constants.PADDING + this.dateWidth);
//        double mouseMillis = this.currentStartDate.getMillis() + pixel * millisPerPixel;
//        DateTime mouseDate = new DateTime((long) mouseMillis);

        if (mouseX >= Constants.PADDING_HORIZONTAL_LEFT + this.dateWidth && mouseX <= this.getWidth() - this.dateWidth - Constants.PADDING_HORIZONTAL_RIGHT) {
            double mouseMillis1 = millisAtPixel(mouseX);
            DateTime mouseDate1 = new DateTime((long) mouseMillis1);
            graphics2D.drawString(mouseDate1.withZone(
                    DateTimeZone.forID(Constants.TIMEZONE_TO_DISPLAY)).toString(Constants.DATE_PATTERN)
                    , mouseX - this.dateHeight / 2
                    , (int) (this.getVisibleRect().getY() + Constants.PADDING_VERTICAL - 2 * this.dateWidth));
            graphics2D.drawLine(mouseX
                    , (int) (this.getVisibleRect().getY() + Constants.PADDING_VERTICAL - this.dateWidth)
                    , mouseX
                    , (int) (this.getVisibleRect().getY() + this.getVisibleRect().getHeight() - Constants.PADDING_VERTICAL));
        }
    }

    void drawStayPoint(Graphics2D graphics2D) {
        graphics2D.setStroke(new BasicStroke(Constants.BAR_WIDTH*.08F,BasicStroke.CAP_ROUND,BasicStroke.JOIN_ROUND));
        for (int j = 0; j < this.drawnMeet.size(); j++) {
            MeetingPoint meetingPoint = this.drawnMeet.get(j);
            drawStayLine(graphics2D, meetingPoint);
            this.groupGanttController.higihLightPerson(-1);
        }
        if (this.currentSelectedMeet != null)
        {
            drawCurrentSelectedPerson(graphics2D, this.currentSelectedMeet);
        }
        graphics2D.setStroke(new BasicStroke());

//        for (int i = 0; i < Constants.BARRYORDER.size(); i++) {
//
//            System.out.println(Constants.PEOPLE_NAMES[Constants.BARRYORDER.get(i)]);
//        }
    }

    void drawStayLine(Graphics2D graphics2D, MeetingPoint meetingPoint) {

        //barycentric sorting (hack)
        if(Constants.orderOfNode.size() != 0 ) {

            TreeMap<Integer, StayPoint> temp = new TreeMap<>();
            for (int i = 0; i < meetingPoint.getStayPoints().size(); i++) {
                temp.put(Constants.orderOfNode.indexOf(meetingPoint.getStayPoints().get(i).getPersonId()), meetingPoint.getStayPoints().get(i));
            }

            meetingPoint.getStayPoints().clear();
            for (Integer r : temp.keySet()) {
                meetingPoint.getStayPoints().add(temp.get(r));
            }
        }
        double currentStartX;
        double currentEndX;
        double currentStartMillis = 0;
        double currentEndMillis = 0;
        double currentY = (this.groupGanttController.isDiffRows()) ?
                this.getHeight() - Constants.PADDING_VERTICAL -
                        (this.groupGanttController.totalNumberOfRowsWhenSeperatePerson() * Constants.BAR_WIDTH)
                : this.getHeight() - Constants.PADDING_VERTICAL
                - (this.groupGanttController.numberOfUniquePlacesSelectedPeopleMeet() * Constants.BAR_WIDTH);
        double yPos = 0;
        int indexOfStayRegion = this.groupGanttController.listOfStayRegionsForMeetings().indexOf(meetingPoint.getStayRegionId());
        if (this.groupGanttController.isDiffRows()) {
            int personId = meetingPoint.getPersonId().get(0);
            yPos = currentY;
            for (int k = 0; k < this.groupGanttController.selectedPeople().indexOf(personId)
                    ; k++) {
                yPos += Constants.BAR_WIDTH * this.groupGanttController.numberOfUniquePlacesForAPerson(
                        this.groupGanttController.selectedPeople().get(k));
            }
            yPos = yPos + Constants.BAR_WIDTH * this.groupGanttController.listOfUniquePlacesVisitedByAPerson(
                    personId).indexOf(meetingPoint.getStayRegionId());
        } else
            currentY = currentY + Constants.BAR_WIDTH * indexOfStayRegion;
        int spacing = (this.groupGanttController.isDiffRows())
                ?
                Constants.BAR_WIDTH / 2
                :
                Constants.BAR_WIDTH / (this.groupGanttController.numberOfPeopleSelected() + 1);
        if (Constants.VERBOSE) {
            System.out.println("Number of people" + this.groupGanttController.numberOfPeopleSelected());
            System.out.println("Bar Width" + Constants.BAR_WIDTH);
            System.out.println("spacing" + spacing);
        }
        yPos = yPos + spacing;
        currentY += spacing;

        for (int i = 0; i < meetingPoint.getStayPoints().size(); i++) {
            int j = meetingPoint.getStayPoints().get(i).getPersonId();
            if(this.useOfColors == 0) {
                graphics2D.setColor(Constants.PEOPLE_COLOR[j % Constants.PEOPLE_COLOR.length]);
            }
            currentStartMillis = meetingPoint.getStayPoints().get(i).getArrivalDateTime().getMillis();
            currentEndMillis = meetingPoint.getStayPoints().get(i).getDepartureDateTime().getMillis();
            currentStartX = pixelAtMillis(currentStartMillis);
            currentEndX = pixelAtMillis(currentEndMillis);
//            yPos = yPos + spacing;
//            currentY += spacing;
            if (this.getVisibleRect().intersectsLine(
                    (int) currentStartX
                    , (this.groupGanttController.isDiffRows()) ? (int) yPos + spacing
                            : (int) currentY + spacing * this.groupGanttController.positionOfSelectedPersonInArrayList(meetingPoint.getStayPoints().get(i).getPersonId())
                    , (int) currentEndX
                    , (this.groupGanttController.isDiffRows()) ? (int) yPos + spacing
                            : (int) currentY + spacing * this.groupGanttController.positionOfSelectedPersonInArrayList(meetingPoint.getStayPoints().get(i).getPersonId())
            )) {

                drawLineAlongFolds(graphics2D
                        , (int) currentStartX
                        , (this.groupGanttController.isDiffRows()) ? (int) yPos
                        : (int) currentY + spacing * this.groupGanttController.positionOfSelectedPersonInArrayList(meetingPoint.getStayPoints().get(i).getPersonId())
                        , (int) currentEndX
                        , (this.groupGanttController.isDiffRows()) ? (int) yPos
                        : (int) currentY + spacing * this.groupGanttController.positionOfSelectedPersonInArrayList(meetingPoint.getStayPoints().get(i).getPersonId()));

//                graphics2D.setColor(Color.black);
//                graphics2D.drawLine((int) currentStartX
//                        , (this.groupGanttController.isDiffRows()) ? (int) yPos
//                        : (int) currentY + spacing * this.groupGanttController.positionOfSelectedPersonInArrayList(meetingPoint.getStayPoints().get(i).getPersonId())
//                        , (int) currentEndX
//                        , (this.groupGanttController.isDiffRows()) ? (int) yPos
//                        : (int) currentY + spacing * this.groupGanttController.positionOfSelectedPersonInArrayList(meetingPoint.getStayPoints().get(i).getPersonId()));

            }
        }
    }

    void drawLineAlongFolds(Graphics2D graphics2D, int startX, int startY, int endX, int endY) {
        if (this.folds.size() == 0) {
            graphics2D.drawLine(startX, startY, endX, endY);
        } else {
            ArrayList<Integer> timeLineX = new ArrayList<>();
            timeLineX.add(Constants.PADDING_HORIZONTAL_LEFT);
            for (int i = 0; i < this.folds.size() - 1; i = i + 2) {
                int startFold = (int) this.folds.get(i).doubleValue();
                int endFold = (int) this.folds.get(i + 1).doubleValue();
                int middleFold = startFold + (endFold - startFold) / 2;
                timeLineX.add(startFold);
                timeLineX.add(middleFold);
                timeLineX.add(endFold);
            }
            timeLineX.add(this.getWidth() - Constants.PADDING_HORIZONTAL_RIGHT);
            int k = 0;
            if(startX <= timeLineX.get(0)){
                graphics2D.drawLine(startX, startY, timeLineX.get(0), endY);
                startX = timeLineX.get(0);
            }



            for (int i = 0; i < timeLineX.size() - 1; i++, k = k % 3) {

                int start = timeLineX.get(i);
                int end = timeLineX.get(i + 1);
                    if(k==0) {
                        if (startX >= start && endX <= end) {
                            graphics2D.drawLine(startX, startY, endX, endY);
                            break;
                        }
                        if (startX >= start && startX < end && endX >= end) {
                            graphics2D.drawLine(startX, startY, end, endY);
                            startX = end;
                        }
                    }
                    if(k == 1) {
                            int startFold = start;
                            int endFold = start + (end - start)*2;
                            int midFold = end;

                        if (startX >= start && endX <= end) {

                            double mid = startFold + (endFold - startFold) * this.currentAniSlope;
                            double mid1 = endFold - (endFold - startFold) * this.currentAniSlope;
                            double point = midFold;
                            double disPoint = (this.groupGanttController.isDiffRows()) ?
                                    (this.getHeight() - Constants.PADDING_VERTICAL
                                            - (this.groupGanttController.totalNumberOfRowsWhenSeperatePerson() * Constants.BAR_WIDTH))
                                            + (this.groupGanttController.totalNumberOfRowsWhenSeperatePerson() * Constants.BAR_WIDTH) / 2.0
                                    :
                                    (this.getHeight() - Constants.PADDING_VERTICAL
                                            - (this.groupGanttController.numberOfUniquePlacesSelectedPeopleMeet() * Constants.BAR_WIDTH))
                                            + (this.groupGanttController.numberOfUniquePlacesSelectedPeopleMeet() * Constants.BAR_WIDTH) / 2.0;
                            double slope = (disPoint - endY) / (mid - startFold);
                            double slope1 = (disPoint - endY) / (mid1 - endFold);

                                graphics2D.drawLine(startX
                                        , (int) (slope * (startX - startFold )) + endY
                                        , endX
                                        , (int) (slope * (endX - startFold )) + endY);
                            break;
                        }
                        if (startX >= start && startX < end && endX >= end) {

                            double mid = startFold + (endFold - startFold) * this.currentAniSlope;
                            double mid1 = endFold - (endFold - startFold) * this.currentAniSlope;
                            double point = midFold;
                            double disPoint = (this.groupGanttController.isDiffRows()) ?
                                    (this.getHeight() - Constants.PADDING_VERTICAL
                                            - (this.groupGanttController.totalNumberOfRowsWhenSeperatePerson() * Constants.BAR_WIDTH))
                                            + (this.groupGanttController.totalNumberOfRowsWhenSeperatePerson() * Constants.BAR_WIDTH) / 2.0
                                    :
                                    (this.getHeight() - Constants.PADDING_VERTICAL
                                            - (this.groupGanttController.numberOfUniquePlacesSelectedPeopleMeet() * Constants.BAR_WIDTH))
                                            + (this.groupGanttController.numberOfUniquePlacesSelectedPeopleMeet() * Constants.BAR_WIDTH) / 2.0;
                            double slope = (disPoint - endY) / (mid - startFold);
                            double slope1 = (disPoint - endY) / (mid1 - endFold);


                                graphics2D.drawLine(startX
                                        , (int) (slope * (startX - startFold )) + endY
                                        , midFold
                                        , (int) (slope * (midFold - startFold )) + endY);
                            startX = end;
                        }
                    }
                    if(k == 2){
                        int startFold = start - (end - start);
                        int endFold = end;
                        int midFold = start;

                        if (startX >= start && endX <= end) {
                            double mid = startFold + (endFold - startFold) * this.currentAniSlope;
                            double mid1 = endFold - (endFold - startFold) * this.currentAniSlope;
                            double point = midFold;
                            double disPoint = (this.groupGanttController.isDiffRows()) ?
                                    (this.getHeight() - Constants.PADDING_VERTICAL
                                            - (this.groupGanttController.totalNumberOfRowsWhenSeperatePerson() * Constants.BAR_WIDTH))
                                            + (this.groupGanttController.totalNumberOfRowsWhenSeperatePerson() * Constants.BAR_WIDTH) / 2.0
                                    :
                                    (this.getHeight() - Constants.PADDING_VERTICAL
                                            - (this.groupGanttController.numberOfUniquePlacesSelectedPeopleMeet() * Constants.BAR_WIDTH))
                                            + (this.groupGanttController.numberOfUniquePlacesSelectedPeopleMeet() * Constants.BAR_WIDTH) / 2.0;
                            double slope = (disPoint - endY) / (mid - startFold);
                            double slope1 = (disPoint - endY) / (mid1 - endFold);

                            graphics2D.drawLine(startX
                                    , (int) (slope * (endFold - startX )) + endY
                                    , endX
                                    , (int)(slope * (endFold - endX )) + endY);

                            break;
                        }
                        if (startX >= start && startX < end && endX >= end) {
                            double mid = startFold + (endFold - startFold) * this.currentAniSlope;
                            double mid1 = endFold - (endFold - startFold) * this.currentAniSlope;
                            double point = midFold;
                            double disPoint = (this.groupGanttController.isDiffRows()) ?
                                    (this.getHeight() - Constants.PADDING_VERTICAL
                                            - (this.groupGanttController.totalNumberOfRowsWhenSeperatePerson() * Constants.BAR_WIDTH))
                                            + (this.groupGanttController.totalNumberOfRowsWhenSeperatePerson() * Constants.BAR_WIDTH) / 2.0
                                    :
                                    (this.getHeight() - Constants.PADDING_VERTICAL
                                            - (this.groupGanttController.numberOfUniquePlacesSelectedPeopleMeet() * Constants.BAR_WIDTH))
                                            + (this.groupGanttController.numberOfUniquePlacesSelectedPeopleMeet() * Constants.BAR_WIDTH) / 2.0;
                            double slope = (disPoint - endY) / (mid - startFold);
                            double slope1 = (disPoint - endY) / (mid1 - endFold);


                                graphics2D.drawLine(startX
                                        , (int) (slope * (endFold - startX)) + endY
                                        , endFold
                                        , endY);
                            startX = end ;
                        }
                    }
                k++;
            }
        }
    }

    void drawMeetingPointAnnotation(Graphics2D graphics2D, MeetingPoint meetingPoint) {

        Font font = new Font("Tamoha", Font.BOLD, (int) Constants.TEXT_SIZE - 6);
        graphics2D.setFont(font);
        double currentStartX = pixelAtMillis(meetingPoint.getInterval().getStartMillis());
        double currentEndX = pixelAtMillis(meetingPoint.getInterval().getEndMillis());
        graphics2D.setColor(Color.black);
        graphics2D.setStroke(new BasicStroke(1.0f,
                BasicStroke.CAP_BUTT,
                BasicStroke.JOIN_MITER,
                10.0f, dash1, 0.0f));
        int currentY;
        double height;
        if (this.groupGanttController.isDiffRows()) {
            currentY = this.getHeight() - Constants.PADDING_VERTICAL
                    - (this.groupGanttController.totalNumberOfRowsWhenSeperatePerson() * Constants.BAR_WIDTH);
            height = (this.groupGanttController.totalNumberOfRowsWhenSeperatePerson() * Constants.BAR_WIDTH);
        } else {
            currentY = this.getHeight() - Constants.PADDING_VERTICAL
                    - (this.groupGanttController.numberOfUniquePlacesSelectedPeopleMeet() * Constants.BAR_WIDTH);
            height = (this.groupGanttController.numberOfUniquePlacesSelectedPeopleMeet() * Constants.BAR_WIDTH);
        }
        graphics2D.drawLine((int) currentStartX
                , currentY
                , (int) currentStartX
                , (int) (this.getVisibleRect().getY() + this.getVisibleRect().getHeight() - Constants.PADDING_VERTICAL));
        graphics2D.drawLine((int) currentEndX
                , currentY
                , (int) currentEndX
                , (int) (this.getVisibleRect().getY() + this.getVisibleRect().getHeight() - Constants.PADDING_VERTICAL));
        graphics2D.setStroke(new BasicStroke());
        String date = meetingPoint.getInterval().getStart().withZone(
                DateTimeZone.forID(Constants.TIMEZONE_TO_DISPLAY)).toString(Constants.DATE_PATTERN);
        graphics2D.drawString(date
                , (int) currentStartX - graphics2D.getFontMetrics().stringWidth(date)
                , (int) (this.getVisibleRect().getY() + Constants.PADDING_VERTICAL - graphics2D.getFontMetrics().stringWidth("H") + 4));
        date = meetingPoint.getInterval().getEnd().withZone(
                DateTimeZone.forID(Constants.TIMEZONE_TO_DISPLAY)).toString(Constants.DATE_PATTERN);
        graphics2D.drawString(date
                , (int) currentEndX
                , (int) (this.getVisibleRect().getY() + Constants.PADDING_VERTICAL - graphics2D.getFontMetrics().stringWidth("H") + 4));
    }

    void drawCurrentSelectedPerson(Graphics2D graphics2D, MeetingPoint meetingPoint) {

        ArrayList<Integer> stayLineY = new ArrayList<>();
        double currentStartX;
        double currentEndX;
        double currentStartMillis = 0;
        double currentEndMillis = 0;
        double currentY = (this.groupGanttController.isDiffRows()) ?
                this.getHeight() - Constants.PADDING_VERTICAL -
                        (this.groupGanttController.totalNumberOfRowsWhenSeperatePerson() * Constants.BAR_WIDTH)
                : this.getHeight() - Constants.PADDING_VERTICAL
                - (this.groupGanttController.numberOfUniquePlacesSelectedPeopleMeet() * Constants.BAR_WIDTH);
        double yPos = 0;
        int indexOfStayRegion = this.groupGanttController.listOfStayRegionsForMeetings().indexOf(meetingPoint.getStayRegionId());
        if (this.groupGanttController.isDiffRows()) {
            int personId = meetingPoint.getPersonId().get(0);
            yPos = currentY;
            for (int k = 0; k < this.groupGanttController.selectedPeople().indexOf(personId)
                    ; k++) {
                yPos += Constants.BAR_WIDTH * this.groupGanttController.numberOfUniquePlacesForAPerson(
                        this.groupGanttController.selectedPeople().get(k));
            }
            yPos = yPos + Constants.BAR_WIDTH * this.groupGanttController.listOfUniquePlacesVisitedByAPerson(
                    personId).indexOf(meetingPoint.getStayRegionId());
        } else {
            currentY = currentY + Constants.BAR_WIDTH * indexOfStayRegion;
        }
        int spacing = (this.groupGanttController.isDiffRows())
                ?
                Constants.BAR_WIDTH / 2
                :
                Constants.BAR_WIDTH / (this.groupGanttController.numberOfPeopleSelected() + 1);
        yPos = yPos + spacing;
        currentY += spacing;
        int personId = -1;
        double distance = 0;
        for (int i = 0; i < meetingPoint.getStayPoints().size(); i++) {
            int j = meetingPoint.getStayPoints().get(i).getPersonId();
            currentStartMillis = meetingPoint.getStayPoints().get(i).getArrivalDateTime().getMillis();
            currentEndMillis = meetingPoint.getStayPoints().get(i).getDepartureDateTime().getMillis();
            currentStartX = pixelAtMillis(currentStartMillis);
            currentEndX = pixelAtMillis(currentEndMillis);


            double temp = Line2D.ptLineDist((int) currentStartX
                    , (this.groupGanttController.isDiffRows()) ? (int) yPos
                    : (int) currentY + spacing * this.groupGanttController.positionOfSelectedPersonInArrayList(meetingPoint.getStayPoints().get(i).getPersonId())
                    , (int) currentEndX
                    , (this.groupGanttController.isDiffRows()) ? (int) yPos
                    : (int) currentY + spacing * this.groupGanttController.positionOfSelectedPersonInArrayList(meetingPoint.getStayPoints().get(i).getPersonId())
                    , mouseX
                    , mouseY);

            stayLineY.add((int) currentY + spacing * this.groupGanttController.positionOfSelectedPersonInArrayList(meetingPoint.getStayPoints().get(i).getPersonId()));


            if (i == 0) {
                distance = temp;
                personId = j;
            } else {
                if (distance > temp) {
                    distance = temp;
                    personId = j;
                }
            }
        }

        Font font = graphics2D.getFont();
        Font font1 = new Font("Tamoha", Font.BOLD, (int) (2 * Constants.TEXT_SIZE - 4));
        graphics2D.setFont(font1);

        if(typeOfLabeling == 1) {
            // draw Closet person using mouse
            currentY = (this.groupGanttController.isDiffRows()) ?
                    this.getHeight() - Constants.PADDING_VERTICAL -
                            (this.groupGanttController.totalNumberOfRowsWhenSeperatePerson() * Constants.BAR_WIDTH)
                    : this.getHeight() - Constants.PADDING_VERTICAL
                    - (this.groupGanttController.numberOfUniquePlacesSelectedPeopleMeet() * Constants.BAR_WIDTH);
            currentStartMillis = meetingPoint.getInterval().getStartMillis();
            currentEndMillis = meetingPoint.getInterval().getEndMillis();
            currentStartX = pixelAtMillis(currentStartMillis);
            currentEndX = pixelAtMillis(currentEndMillis);
            currentY = currentY + Constants.BAR_WIDTH * indexOfStayRegion + Constants.BAR_WIDTH;

            int offSet = 5;
            Rectangle stringBounds = graphics2D.getFontMetrics().getStringBounds(Constants.PEOPLE_NAMES[personId], graphics2D).getBounds();
            int drawStringX = mouseX - stringBounds.width / 2 - offSet;
            int drawStringY = mouseY;

            if (drawStringX - stringBounds.width < Constants.PADDING_HORIZONTAL_LEFT) {
                drawStringX = mouseX + stringBounds.width / 2 + offSet;
            }

            if (drawStringY - stringBounds.height < Constants.PADDING_VERTICAL) {
                drawStringY = 2 * drawStringY;
            }
        DrawUtils.drawAtVisualCenter(Constants.PEOPLE_NAMES[personId]
                , drawStringX
                , mouseY
                , graphics2D
                , (this.useOfColors == 0) ? Constants.PEOPLE_COLOR[personId % Constants.PEOPLE_COLOR.length] : Color.black
                , true
                , Color.white
                , 2.0f
                , true
                , new Color(190, 190, 190));
        }

        this.groupGanttController.higihLightPerson(personId);

        if(this.typeOfLabeling == 2) {
            // draw All Persons and highlightSelected Person
            int drawStringBoxX = 0;
            int drawStringBoxY = 0;
            currentY = (this.groupGanttController.isDiffRows()) ?
                    this.getHeight() - Constants.PADDING_VERTICAL -
                            (this.groupGanttController.totalNumberOfRowsWhenSeperatePerson() * Constants.BAR_WIDTH)
                    : this.getHeight() - Constants.PADDING_VERTICAL
                    - (this.groupGanttController.numberOfUniquePlacesSelectedPeopleMeet() * Constants.BAR_WIDTH);
            currentStartMillis = meetingPoint.getInterval().getStartMillis();
            currentEndMillis = meetingPoint.getInterval().getEndMillis();
            currentStartX = pixelAtMillis(currentStartMillis);
            currentEndX = pixelAtMillis(currentEndMillis);
            currentY = currentY + Constants.BAR_WIDTH * indexOfStayRegion;

            graphics2D.setColor(Color.black);

            int centerLineX = 0;
            int centerLineY = 0;

            //left of arrow
            drawStringBoxX = (int) (currentStartX + (currentEndX - currentStartX) / 2);
            drawStringBoxY = (int) (currentY + Constants.BAR_WIDTH / 2);
            centerLineX = drawStringBoxX;
            centerLineY = drawStringBoxY;
//            graphics2D.drawLine(drawStringBoxX - Constants.offsetForStringBoxFromCenter, drawStringBoxY, drawStringBoxX, drawStringBoxY);
            drawStringBoxX = drawStringBoxX - Constants.offsetForStringBoxFromCenter - Constants.offsetForStringBoxforLine;


            int numberOfPersons = meetingPoint.getStayPoints().size();
            int lengthOfLargestName = 0;

            for (int i = 0; i < numberOfPersons; i++) {
                int j = meetingPoint.getStayPoints().get(i).getPersonId();
                Rectangle tempPixelBound = DrawUtils.getPixelBounds(
                        Constants.PEOPLE_NAMES[j]
                        , graphics2D
                        , 0
                        , 0);
                int tempwidth = tempPixelBound.width;


                if (lengthOfLargestName < tempwidth) {
                    lengthOfLargestName = tempwidth;
//                    System.out.println(Constants.PEOPLE_NAMES[j]);
                }
            }

            //calculate the position vertically to start printing
            if (numberOfPersons % 2 == 0) {
                drawStringBoxY = drawStringBoxY - Constants.spacingHover / 2;
                for (int i = 0; i < numberOfPersons / 2; i++) {
                    int j = meetingPoint.getStayPoints().get(i).getPersonId();
                    Rectangle tempPixelBound = DrawUtils.getPixelBounds(
                            Constants.PEOPLE_NAMES[j]
                            , graphics2D
                            , 0
                            , 0);
                    drawStringBoxY = drawStringBoxY - tempPixelBound.height;
//                int tempwidth = tempPixelBound.width;


                    if (i != 0) {
                        drawStringBoxY = drawStringBoxY - Constants.spacingHover;
                    }
                }
            } else {
                int j = meetingPoint.getStayPoints().get(numberOfPersons / 2).getPersonId();

                drawStringBoxY -= (DrawUtils.getPixelBounds(
                        Constants.PEOPLE_NAMES[j]
                        , graphics2D
                        , 0
                        , 0).height) / 2;

                for (int i = 0; i < numberOfPersons / 2; i++) {
                    j = meetingPoint.getStayPoints().get(numberOfPersons / 2).getPersonId();
                    Rectangle tempPixelBound = DrawUtils.getPixelBounds(
                            Constants.PEOPLE_NAMES[j]
                            , graphics2D
                            , 0
                            , 0);
                    drawStringBoxY -= tempPixelBound.height;
                    int tempwidth = tempPixelBound.width;

                    drawStringBoxY -= Constants.spacingHover;
                }
            }


            int downBound = (int) this.getVisibleRect().getHeight() - Constants.PADDING_VERTICAL;
            // fix for printing out of visibile window
            if (drawStringBoxY < Constants.PADDING_VERTICAL) { //up
                drawStringBoxY += (Constants.PADDING_VERTICAL - drawStringBoxY) + 2;
            } else if (drawStringBoxY + 2 * (centerLineY - drawStringBoxY) > downBound) { // down
                drawStringBoxY = drawStringBoxY - (drawStringBoxY + 2 * (centerLineY - drawStringBoxY) - downBound) - 2;
            }


            if (drawStringBoxX - lengthOfLargestName < Constants.PADDING_HORIZONTAL_LEFT) {
                //right of arrow
                drawStringBoxX = (int) (currentStartX + (currentEndX - currentStartX) / 2);
                drawStringBoxY = (int) (currentY + Constants.BAR_WIDTH / 2);
                centerLineX = drawStringBoxX;
                centerLineY = drawStringBoxY;
                drawStringBoxX += Constants.offsetForStringBoxFromCenter + Constants.offsetForStringBoxforLine;
            } else {
                drawStringBoxX -= lengthOfLargestName;
            }

            for (int i = 0; i < meetingPoint.getStayPoints().size(); i++) {
                int j = meetingPoint.getStayPoints().get(i).getPersonId();
                if (this.useOfColors == 0) {
                    graphics2D.setColor(Constants.PEOPLE_COLOR[j % Constants.PEOPLE_COLOR.length]);
                }

                int widthOfNames = DrawUtils.getPixelBounds(
                        Constants.PEOPLE_NAMES[j]
                        , graphics2D
                        , 0
                        , 0).width;
                int heightOfNames = DrawUtils.getPixelBounds(
                        Constants.PEOPLE_NAMES[j]
                        , graphics2D
                        , 0
                        , 0).height;
                graphics2D.setColor(Constants.BACKGROUND_COLOR);
                graphics2D.fillRect(drawStringBoxX - 2, drawStringBoxY - 2, widthOfNames + 4, heightOfNames + 4);
                Stroke stroke = graphics2D.getStroke();
                if (j == personId) {
                    graphics2D.setStroke(new BasicStroke(4.0f));
                    graphics2D.setColor(Constants.ROLLOVER_HIGHLIGHT_PEOPLE);
                }
                graphics2D.drawRect(drawStringBoxX - 2, drawStringBoxY - 2, widthOfNames + 4, heightOfNames + 4);
                graphics2D.setStroke(stroke);
                graphics2D.setColor(Color.black);
                DrawUtils.drawAtVisualCenter(Constants.PEOPLE_NAMES[j]
                        , drawStringBoxX + widthOfNames / 2
                        , drawStringBoxY + heightOfNames / 2
                        , graphics2D
                        , (this.useOfColors == 0) ? Constants.PEOPLE_COLOR[j % Constants.PEOPLE_COLOR.length] : Color.black
                        , false
                        , null
                        , 2.0f
                        , false
                        , null);

                if (this.useOfColors == 0) {
                    graphics2D.setColor(Constants.PEOPLE_COLOR[j % Constants.PEOPLE_COLOR.length]);
                }
//                graphics2D.setStroke((j == personId) ? new BasicStroke(6.0f) : new BasicStroke(1.0f));
                graphics2D.setStroke(new BasicStroke(6.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_BEVEL));
                if (drawStringBoxX > (int) (currentStartX + (currentEndX - currentStartX) / 2)) {

                    graphics2D.drawLine((int) (currentStartX + (currentEndX - currentStartX) / 2)
                            , stayLineY.get(i)
                            , drawStringBoxX - 5
                            , drawStringBoxY + heightOfNames / 2
                    );
                } else {
                    graphics2D.drawLine((int) (currentStartX + (currentEndX - currentStartX) / 2)
                            , stayLineY.get(i)
                            , drawStringBoxX + lengthOfLargestName + 5
                            , drawStringBoxY + heightOfNames / 2
                    );
                }
                drawStringBoxY += heightOfNames + Constants.spacingHover;
                graphics2D.setColor(Color.black);
            }
        }
        graphics2D.setFont(font);

    }

    void drawGanttChart(Graphics2D graphics2D) {
        if (Constants.VERBOSE) {
//            System.out.println(this.currentStartDate.toString());
//            System.out.println(this.currentEndDate.toString());  .
            System.out.println(new DateTime((long) this.currentStartDate));
            System.out.println(new DateTime((long) this.currentEndDate));
        }
        drawMeetingPoints(graphics2D);
        drawGanttLines(graphics2D);
    }

    void drawMeetingPoints(Graphics2D graphics2D) {
        if (Constants.VERBOSE) {
            System.out.println("Width per milli" + this.widthPerMilli);
            System.out.println("Start Pixel " + (Constants.PADDING_HORIZONTAL_LEFT + this.dateWidth));
            System.out.println("End Pixel " + (this.getWidth() - Constants.PADDING_HORIZONTAL_RIGHT - this.dateWidth));
        }
        this.rectangle = new ArrayList<>();
        this.drawnMeet = new ArrayList<>();
//        this.connectingLine = new HashMap<>();
        double currentStartX = 0;
        double currentEndX = 0;
        double currentStartMilli = 0;
        double currentEndMilli = 0;
        double currentY = 0;
        if (this.groupGanttController.isDiffRows()) {
            currentY = this.getHeight() - Constants.PADDING_VERTICAL
                    - (this.groupGanttController.totalNumberOfRowsWhenSeperatePerson() * Constants.BAR_WIDTH);
            display = new Rectangle.Double(this.getVisibleRect().getX() + Constants.PADDING_HORIZONTAL_LEFT + this.dateWidth
                    , currentY
                    , this.getVisibleRect().getWidth() -  Constants.PADDING_HORIZONTAL_LEFT - Constants.PADDING_HORIZONTAL_RIGHT -2 * this.dateWidth
                    , (this.groupGanttController.totalNumberOfRowsWhenSeperatePerson() * Constants.BAR_WIDTH)
            );
            for (int i = 0; i < this.groupGanttController.getMeetingPointsToDisplay().size(); ++i) {
                MeetingPoint meetingPoint = this.groupGanttController.getMeetingPointsToDisplay().get(i);
                currentStartMilli = meetingPoint.getInterval().getStart().getMillis();
                currentEndMilli = meetingPoint.getInterval().getEnd().getMillis();
                int personId = meetingPoint.getPersonId().get(0);
                currentStartX = pixelAtMillis(currentStartMilli);
                currentEndX = pixelAtMillis(currentEndMilli);
                if ((currentEndX - currentStartX) < 1)
                    currentEndX += 1;
                double yPos = currentY;
                for (int k = 0; k < this.groupGanttController.selectedPeople().indexOf(meetingPoint.getPersonId().get(0))
                        ; k++) {
                    yPos += Constants.BAR_WIDTH * this.groupGanttController.numberOfUniquePlacesForAPerson(
                            this.groupGanttController.selectedPeople().get(k)
                    );
                }

//                for (int j : meetingPoint.getPersonId()
//                        ) {
//                    if (!this.connectingLine.containsKey(j)) {
//                        this.connectingLine.put(j, new ArrayList<StayPoint>());
//                    }
//                }
//                for (int k = 0; k < meetingPoint.getStayPoints().size(); k++) {
//                    StayPoint s = meetingPoint.getStayPoints().get(k);
//                    this.connectingLine.get(s.getPersonId()).add(s);
//                }
                if (display.intersects(new Rectangle2D.Double(currentStartX
                        , yPos + Constants.BAR_WIDTH * this.groupGanttController.listOfUniquePlacesVisitedByAPerson(
                        personId).indexOf(meetingPoint.getStayRegionId())
                        , currentEndX - currentStartX
                        , Constants.BAR_WIDTH))
                        ) {

                    this.drawnMeet.add(this.groupGanttController.getMeetingPointsToDisplay().get(i));
                    drawRectangles(graphics2D
                            , currentStartX
                            , currentEndX
                            , yPos + Constants.BAR_WIDTH * this.groupGanttController.listOfUniquePlacesVisitedByAPerson(
                            personId).indexOf(meetingPoint.getStayRegionId()), meetingPoint.getStayRegionId());
                }
            }
        } else {
            currentY = this.getHeight() - Constants.PADDING_VERTICAL
                    - (this.groupGanttController.numberOfUniquePlacesSelectedPeopleMeet() * Constants.BAR_WIDTH);

            display = new Rectangle.Double(Constants.PADDING_HORIZONTAL_LEFT + this.dateWidth,
                    currentY,
                    this.getWidth() - Constants.PADDING_HORIZONTAL_LEFT - Constants.PADDING_HORIZONTAL_RIGHT - 2* this.dateWidth
                    , (this.groupGanttController.numberOfUniquePlacesSelectedPeopleMeet() * Constants.BAR_WIDTH)
            );
            for (int i = 0; i < this.groupGanttController.getMeetingPointsToDisplay().size(); ++i) {
                MeetingPoint meetingPoint = this.groupGanttController.getMeetingPointsToDisplay().get(i);
                currentStartMilli = meetingPoint.getInterval().getStart().getMillis();
                currentEndMilli = meetingPoint.getInterval().getEnd().getMillis();
//                for (int j : meetingPoint.getPersonId()
//                        ) {
//                    if (!this.connectingLine.containsKey(j)) {
//                        this.connectingLine.put(j, new ArrayList<StayPoint>());
//                    }
//                }
//                for (int k = 0; k < meetingPoint.getStayPoints().size(); k++) {
//                    StayPoint s = meetingPoint.getStayPoints().get(k);
//                    this.connectingLine.get(s.getPersonId()).add(s);
//                }
                int indexOfStayRegion =
                        this.groupGanttController.listOfStayRegionsForMeetings().indexOf(meetingPoint.getStayRegionId());
                currentStartX = pixelAtMillis(currentStartMilli);
                currentEndX = pixelAtMillis(currentEndMilli);
                if ((currentEndX - currentStartX) < 1)
                    currentEndX += 1;
                if (display.intersects(new Rectangle2D.Double(currentStartX
                        , currentY + Constants.BAR_WIDTH * indexOfStayRegion
                        , currentEndX - currentStartX
                        , Constants.BAR_WIDTH))
                        ) {

                    this.drawnMeet.add(this.groupGanttController.getMeetingPointsToDisplay().get(i));
                    drawRectangles(graphics2D, currentStartX, currentEndX, currentY + Constants.BAR_WIDTH * indexOfStayRegion, meetingPoint.getStayRegionId());
                }

            }
        }
//        graphics2D.draw(display);
    }

    void updateAnimationState(double animation) {
        this.setAnimationState();
        if (animation == 1) {
            this.animation = false;
        } else {
            for (int i = 0; i < this.diffMillis.size(); i++) {
                this.diffMillis.set(i
                        , this.diffMillis.get(i) + (this.finalDiffMillis.get(i) - this.diffMillis.get(i)) * animation);
            }
            this.currentAniSlope = this.currentAniSlope + (this.finalAniSlope - this.currentAniSlope) * animation;
            this.initialAlpha = (int) (this.initialAlpha + (this.finalAlpha - this.initialAlpha) * animation);
        }
    }

    void updateResetAnimationState(double animation) {
        if (animation == 1) {
            this.animatePoint = new ArrayList<>();
        } else {

            for (int i = 0; i < this.diffMillis.size(); i++) {
                this.diffMillis.set(i
                        , this.diffMillis.get(i) + (this.finalDiffMillis.get(i) - this.diffMillis.get(i)) * animation);
            }
            this.initialAlpha = (int) (this.initialAlpha + (this.finalAlpha - this.initialAlpha) * animation);
            this.currentAniSlope = this.currentAniSlope + (this.finalAniSlope - this.currentAniSlope) * animation;
        }

    }

    void updateAnimationWithAutoFoldState(double animation) {
        if (animation == 1.0) {
            this.autoFrameWithFolds = false;
        } else {
            this.setAnimationState();
            for (int i = 0; i < this.diffMillis.size(); i++) {
                this.diffMillis.set(i, this.diffMillis.get(i) + (this.finalDiffMillis.get(i) - this.diffMillis.get(i)) * animation);

            }
            this.initialAlpha = (int) (this.initialAlpha + (this.finalAlpha - this.initialAlpha) * animation);
            this.currentAniSlope = this.currentAniSlope + (this.finalAniSlope - this.currentAniSlope) * animation;
//        this.finalStartMillis = this.finalAutoFrameStart - this.finalDiffMillis.get(this.listStart);
//        this.finalEndMillis = this.finalAutoFrameEnd
//                - this.finalDiffMillis.get(this.listEnd);

            this.finalStartMillis =
                    this.groupGanttController.getMeetingPointsToDisplay().get(0).getInterval().getStart().getMillis();

            this.finalEndMillis = this.groupGanttController.getMeetingPointsToDisplay().get(
                    this.groupGanttController.getMeetingPointsToDisplay().size() - 1).getInterval().getEnd().getMillis()
                    - this.finalDiffMillis.get(this.groupGanttController.getMeetingPointsToDisplay().size() - 1);

            this.currentStartDate = (this.currentStartDate + (this.finalStartMillis - this.currentStartDate) * animation);
            this.currentEndDate = (this.currentEndDate + (this.finalEndMillis - this.currentEndDate) * animation);
        }
    }

    void updateAnimationWithAutoFoldStateVisibleInterval(double animation) {
        if (animation == 1.0) {
            this.frameVisibleWithFold = false;
        } else {
            this.setAnimationState();
            for (int i = 0; i < this.diffMillis.size(); i++) {
                this.diffMillis.set(i
                        , this.diffMillis.get(i) + (this.finalDiffMillis.get(i) - this.diffMillis.get(i)) * animation);

            }
            this.initialAlpha = (int) (this.initialAlpha + (this.finalAlpha - this.initialAlpha) * animation);
            this.currentAniSlope = this.currentAniSlope + (this.finalAniSlope - this.currentAniSlope) * animation;
            this.finalStartMillis = this.finalAutoFrameStart - this.finalDiffMillis.get(this.listStart);
            this.finalEndMillis = this.finalAutoFrameEnd
                    - this.finalDiffMillis.get(this.listEnd);
            this.currentStartDate = (this.currentStartDate + (this.finalStartMillis - this.currentStartDate) * animation);
            this.currentEndDate = (this.currentEndDate + (this.finalEndMillis - this.currentEndDate) * animation);
        }
    }

    void drawXFrame(Graphics2D graphics2D) {
        drawFoldLines(graphics2D);
        int count  = -1;
        int sum = 0;
//        sum = sum + this.groupGanttController.numberOfUniquePlacesForAPerson(this.groupGanttController.selectedPeople().get(count));
        int currentY;
        if (this.groupGanttController.isDiffRows()) {
            currentY = this.getHeight() - Constants.PADDING_VERTICAL
                    - (this.groupGanttController.totalNumberOfRowsWhenSeperatePerson() * Constants.BAR_WIDTH);
        } else {
            currentY = this.getHeight() - Constants.PADDING_VERTICAL
                    - (this.groupGanttController.numberOfUniquePlacesSelectedPeopleMeet() * Constants.BAR_WIDTH);
        }

        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        int personID = 0;
        for (int i = 0; i < this.groupGanttController.totalNumberOfRows() + 1; i++) {
            if (i == this.groupGanttController.totalNumberOfRows()) {
                graphics2D.setStroke(new BasicStroke());
                graphics2D.setColor(Color.black);
            } else {
                graphics2D.setStroke(new BasicStroke(1.0f,
                        BasicStroke.CAP_BUTT,
                        BasicStroke.JOIN_MITER,
                        10.0f, dash1, 0.0f));
                graphics2D.setColor(Constants.X_FRAME_COLOR);
            }
            if (this.groupGanttController.isDiffRows()) {
                if (i == sum && i != this.groupGanttController.totalNumberOfRows()) {
                    graphics2D.setStroke(new BasicStroke(1.5f));
                    graphics2D.setColor(Color.black);
                    sum = sum + this.groupGanttController.numberOfUniquePlacesForAPerson(this.groupGanttController.selectedPeople().get(++count));

                }

            }
            if (this.animatePoint.size() == 0) {
                graphics2D.drawLine(Constants.PADDING_HORIZONTAL_LEFT + this.dateWidth
                        , currentY, this.getWidth() - Constants.PADDING_HORIZONTAL_RIGHT - this.dateWidth, currentY);
            } else {
                MeetingPoint start = this.groupGanttController.getMeetingPointsToDisplay().get(this.animatePoint.get(0));
                double currentStart = this.currentStartDate - this.currentStartDate;
                double currentEnd = start.getInterval().getEnd().getMillis() - this.currentStartDate;
                double currentX = Constants.PADDING_HORIZONTAL_LEFT + this.dateWidth + this.widthPerMilli * currentStart;
                double currentEX = Constants.PADDING_HORIZONTAL_LEFT + this.dateWidth + this.widthPerMilli * currentEnd;
                if (this.getVisibleRect().intersectsLine((int) currentX, currentY, (int) currentEX, currentY))
                    graphics2D.drawLine((int) currentX, currentY, (int) currentEX, currentY);
                for (int temp = 0; temp < this.animatePoint.size(); temp = temp + 2) {
                    MeetingPoint meetingPoint = this.groupGanttController.getMeetingPointsToDisplay().get(this.animatePoint.get(temp));
                    MeetingPoint meetingPoint1 = this.groupGanttController.getMeetingPointsToDisplay().get(this.animatePoint.get(temp + 1));
                    double currentStartMilli = meetingPoint.getInterval().getEnd().getMillis();
                    double currentEndMilli = meetingPoint1.getInterval().getStart().getMillis();
                    double currentStartX = pixelAtMillis(currentStartMilli);
                    double currentEndX = pixelAtMillis(currentEndMilli);
                    double mid = currentStartX + (currentEndX - currentStartX) * this.currentAniSlope;
                    double mid1 = currentEndX - (currentEndX - currentStartX) * this.currentAniSlope;
                    double point = (currentEndX - currentStartX) / 2.0 + currentStartX;
                    double disPoint = (this.groupGanttController.isDiffRows()) ?
                            (this.getHeight() - Constants.PADDING_VERTICAL
                                    - (this.groupGanttController.totalNumberOfRowsWhenSeperatePerson() * Constants.BAR_WIDTH))
                                    + (this.groupGanttController.totalNumberOfRowsWhenSeperatePerson() * Constants.BAR_WIDTH) / 2.0
                            :
                            (this.getHeight() - Constants.PADDING_VERTICAL
                                    - (this.groupGanttController.numberOfUniquePlacesSelectedPeopleMeet() * Constants.BAR_WIDTH))
                                    + (this.groupGanttController.numberOfUniquePlacesSelectedPeopleMeet() * Constants.BAR_WIDTH) / 2.0;
                    double slope = (disPoint - currentY) / (mid - currentStartX);
                    double slope1 = (disPoint - currentY) / (mid1 - currentEndX);

                    if (i <= this.groupGanttController.totalNumberOfRows() / 2) {
                        if (this.getVisibleRect().intersectsLine((int) currentStartX
                                , currentY
                                , (int) point
                                , (int) (slope * (point - currentStartX)) + currentY))
                            graphics2D.drawLine((int) currentStartX
                                    , currentY
                                    , (int) point
                                    , (int) (slope * (point - currentStartX)) + currentY);
                        if (this.getVisibleRect().intersectsLine((int) point
                                , (int) (slope1 * (point - currentEndX)) + currentY
                                , (int) currentEndX
                                , currentY))
                            graphics2D.drawLine((int) point
                                    , (int) (slope1 * (point - currentEndX)) + currentY
                                    , (int) currentEndX
                                    , currentY);
                    } else {
                        if (this.getVisibleRect().intersectsLine((int) currentEndX
                                , currentY
                                , (int) point
                                , (int) (slope1 * (point - currentEndX)) + currentY
                        ))
                            graphics2D.drawLine((int) currentEndX
                                    , currentY
                                    , (int) point
                                    , (int) (slope1 * (point - currentEndX)) +  currentY);
                        if (this.getVisibleRect().intersectsLine(
                                (int) point
                                , (int) (slope * (point - currentStartX)) +  currentY
                                , (int) currentStartX
                                , currentY
                        ))
                            graphics2D.drawLine((int) point
                                    , (int) (slope * (point - currentStartX)) +  currentY
                                    , (int) currentStartX
                                    , currentY);
                    }
                    try {
                        meetingPoint = this.groupGanttController.getMeetingPointsToDisplay().get(this.animatePoint.get(temp + 1));
                        meetingPoint1 = this.groupGanttController.getMeetingPointsToDisplay().get(this.animatePoint.get(temp + 2));
                        double currentStartMillis = meetingPoint.getInterval().getStart().getMillis() - this.currentStartDate;
                        double currentEndMillis = meetingPoint1.getInterval().getEnd().getMillis() - this.currentStartDate;
                        currentStartX = Constants.PADDING_HORIZONTAL_LEFT + this.dateWidth + this.widthPerMilli * currentStartMillis - this.widthPerMilli * this.diffMillis.get(this.animatePoint.get(temp + 1));
                        currentEndX = Constants.PADDING_HORIZONTAL_LEFT + this.dateWidth + this.widthPerMilli * currentEndMillis - this.widthPerMilli * this.diffMillis.get(this.animatePoint.get(temp + 2));
                        if (this.getVisibleRect().intersectsLine((int) currentStartX, currentY, (int) currentEndX, currentY))
                            graphics2D.drawLine((int) currentStartX, currentY, (int) currentEndX, currentY);

                    } catch (Exception e) {
                        if(Constants.VERBOSE)
                            System.out.println("Exception in x-frame");
                        meetingPoint = this.groupGanttController.getMeetingPointsToDisplay().get(this.animatePoint.get(this.animatePoint.size() - 1));
                        double currentStartMillis = meetingPoint.getInterval().getStart().getMillis() - this.currentStartDate;
                        currentStartX = Constants.PADDING_HORIZONTAL_LEFT + this.dateWidth + this.widthPerMilli * currentStartMillis - this.widthPerMilli * this.diffMillis.get(this.animatePoint.get(this.animatePoint.size() - 1));

                        if (this.getVisibleRect().intersectsLine((int) currentStartX, currentY, this.getWidth() - Constants.PADDING_HORIZONTAL_RIGHT - this.dateWidth, currentY))
                            graphics2D.drawLine((int) currentStartX, currentY, this.getWidth() - Constants.PADDING_HORIZONTAL_RIGHT - this.dateWidth, currentY);
                    }
                }
            }
            currentY = currentY + Constants.BAR_WIDTH;
            if (i == this.groupGanttController.totalNumberOfRows() - 1) {

                currentY = (int) (this.getVisibleRect().getY() + this.getVisibleRect().getHeight() - Constants.PADDING_VERTICAL);
            }
        }
    }

    void drawFoldLines(Graphics2D graphics2D) {
        polygons = new ArrayList<>();
        graphics2D.setStroke(new BasicStroke(1.0f,
                BasicStroke.CAP_BUTT,
                BasicStroke.JOIN_MITER,
                10.0f, dash1, 0.0f));
        for (int i = 0; i < folds.size(); i = i + 2) {
            int currentY = 0;
            if (this.groupGanttController.isDiffRows()) {

                currentY = this.getHeight() - Constants.PADDING_VERTICAL
                        - (this.groupGanttController.totalNumberOfRowsWhenSeperatePerson() * Constants.BAR_WIDTH);

            } else {
                currentY = this.getHeight() - Constants.PADDING_VERTICAL
                        - (this.groupGanttController.numberOfUniquePlacesSelectedPeopleMeet() * Constants.BAR_WIDTH);
            }

            int endY = this.getHeight() - Constants.PADDING_VERTICAL;

            if (this.getVisibleRect().intersects(
                    new Rectangle2D.Double(
                            folds.get(i), currentY, folds.get(i + 1) - folds.get(i),
                            (this.groupGanttController.isDiffRows()) ?
                                    (this.groupGanttController.totalNumberOfRowsWhenSeperatePerson() * Constants.BAR_WIDTH)
                                    : (this.groupGanttController.numberOfUniquePlacesSelectedPeopleMeet() * Constants.BAR_WIDTH)
                    )
            )) {

                double mid = folds.get(i) + (folds.get(i + 1) - folds.get(i)) * this.currentAniSlope;
                double mid1 = folds.get(i + 1) - (folds.get(i + 1) - folds.get(i)) * this.currentAniSlope;
                double point = (folds.get(i + 1) - folds.get(i)) / 2.0 + folds.get(i);
                double disPoint = (this.groupGanttController.isDiffRows()) ?
                        (this.getHeight() - Constants.PADDING_VERTICAL
                                - (this.groupGanttController.totalNumberOfRowsWhenSeperatePerson() * Constants.BAR_WIDTH))
                                + (this.groupGanttController.totalNumberOfRowsWhenSeperatePerson() * Constants.BAR_WIDTH) / 2.0
                        :
                        (this.getHeight() - Constants.PADDING_VERTICAL
                                - (this.groupGanttController.numberOfUniquePlacesSelectedPeopleMeet() * Constants.BAR_WIDTH))
                                + (this.groupGanttController.numberOfUniquePlacesSelectedPeopleMeet() * Constants.BAR_WIDTH) / 2.0;
//                graphics2D.setStroke(new BasicStroke());
                double slope = (disPoint - currentY) / (mid - folds.get(i));
                double slope1 = -1 * (disPoint - currentY) / (mid1 - folds.get(i + 1));
                Polygon polygon = new Polygon();
                polygon.addPoint((int) folds.get(i).doubleValue(), currentY);
                polygon.addPoint((int) point, (int) (slope * (point - folds.get(i))) + currentY);
                polygon.addPoint((int) point, (int) (slope1 * (point - folds.get(i + 1))) + endY);
                polygon.addPoint((int) folds.get(i).doubleValue(), endY);
                graphics2D.setPaint(new GradientPaint((int) folds.get(i).doubleValue(), currentY + (endY - currentY) / 2, new Color(Constants.BACKGROUND_COLOR.getRed(), Constants.BACKGROUND_COLOR.getBlue(), Constants.BACKGROUND_COLOR.getGreen(), 100),
                        (int) point, currentY + (endY - currentY) / 2, new Color(128, 128, 128), true));
                this.polygons.add(polygon);
                graphics2D.fill(polygon);
                graphics2D.setPaint(new Color(Constants.BACKGROUND_COLOR.getRed(), Constants.BACKGROUND_COLOR.getBlue(), Constants.BACKGROUND_COLOR.getGreen(),this.initialAlpha));
                graphics2D.fill(polygon);
                graphics2D.setPaint(new Color(100, 100, 100, 255 - this.initialAlpha));
                graphics2D.drawLine((int) folds.get(i).doubleValue(), currentY, (int) folds.get(i).doubleValue(), endY);
                polygon = new Polygon();
                polygon.addPoint((int) folds.get(i + 1).doubleValue(), currentY);
                polygon.addPoint((int) folds.get(i + 1).doubleValue(), endY);
                polygon.addPoint((int) point, (int) (slope1 * (point - folds.get(i + 1))) + endY);
                polygon.addPoint((int) point, (int) (slope * (point - folds.get(i))) + currentY);
                graphics2D.setPaint(new GradientPaint((int) folds.get(i + 1).doubleValue(), currentY + (endY - currentY) / 2, new Color(Constants.BACKGROUND_COLOR.getRed(), Constants.BACKGROUND_COLOR.getBlue(), Constants.BACKGROUND_COLOR.getGreen(), 100),
                        (int) point, currentY + (endY - currentY) / 2, new Color(128, 128, 128), true));
                graphics2D.fill(polygon);
                this.polygons.add(polygon);
                graphics2D.setPaint(new Color(Constants.BACKGROUND_COLOR.getRed(), Constants.BACKGROUND_COLOR.getBlue(), Constants.BACKGROUND_COLOR.getGreen(), this.initialAlpha));
                graphics2D.fill(polygon);
                graphics2D.setPaint(new Color(100, 100, 100, 255 - this.initialAlpha));
                graphics2D.drawLine((int) folds.get(i + 1).doubleValue(), currentY, (int) folds.get(i + 1).doubleValue(), endY);

            } else polygons.add(new Polygon());
        }
    }

    void drawStayRegionNames(Graphics2D graphics2D) {
        graphics2D.setPaint(Color.black);
        Font font = new Font("Tamoha", Font.BOLD, (int) Constants.TEXT_SIZE - 4);
        graphics2D.setFont(font);
        FontMetrics metrics = graphics2D.getFontMetrics(font);
        double currentY = 0;
        if (this.groupGanttController.isDiffRows()) {
            currentY = this.getHeight() - Constants.PADDING_VERTICAL
                    - (this.groupGanttController.totalNumberOfRowsWhenSeperatePerson() * Constants.BAR_WIDTH);
        } else

            currentY = (this.getHeight() - Constants.PADDING_VERTICAL
                    - (this.groupGanttController.numberOfUniquePlacesSelectedPeopleMeet() * Constants.BAR_WIDTH));

        this.removeAll();
        for (int i = 0; i < ((this.groupGanttController.isDiffRows()) ? this.groupGanttController.totalNumberOfRowsWhenSeperatePerson() :
                this.groupGanttController.numberOfUniquePlacesSelectedPeopleMeet())
                ; i++) {
            int stringWidth = metrics.stringWidth(this.groupGanttController.getName(i));
            int x = Constants.PADDING_HORIZONTAL_LEFT + this.dateWidth - stringWidth -5;
            if (currentY >= (this.getVisibleRect().getY() + Constants.PADDING_VERTICAL - Constants.BAR_WIDTH / 2)
                    && currentY <= (this.getVisibleRect().getY() + this.getVisibleRect().getHeight() - Constants.PADDING_VERTICAL - Constants.BAR_WIDTH / 2)) {
                Label label = new Label(i, this.groupGanttController.getName(i), x, (int) currentY, stringWidth +5);
//            final GroupGanttController groupGanttController1 = this.groupGanttController;
                label.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {

                        Label label1 = (Label) e.getSource();
                        groupGanttController.setName(label1.id, label1.getText());
                    }
                });
                label.addMouseListener(new MouseListener() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        if (e.getButton() == MouseEvent.BUTTON3) {
                            Label label1 = (Label) e.getSource();
                            groupGanttController.removeStayRegion(label1.id);
//                        System.out.println("Remove  "+label1.name);
                            if (animatePoint.size() != 0) {
                                animation = true;
                                autoFrameWithFolds = false;
                                autoFrame = false;
                                autoFrameWithUnFold = false;
                                animationGantt.startGanttAnimation();

                            }

                        }
                    }

                    @Override
                    public void mousePressed(MouseEvent e) {

                    }

                    @Override
                    public void mouseReleased(MouseEvent e) {

                    }

                    @Override
                    public void mouseEntered(MouseEvent e) {
                    }

                    @Override
                    public void mouseExited(MouseEvent e) {

                    }
                });
                this.add(label);
//                graphics2D.drawString(Integer.toString(this.groupGanttController.noOfTimes(
//                        this.groupGanttController.listOfStayRegionsForMeetings().get(i))),(int)(this.getVisibleRect().getX() + this.getVisibleRect().getWidth() - Constants.PADDING_HORIZONTAL),(int)currentY);

            }
//            graphics2D.drawString(Integer.toString(i), x, (int) currentY);
            currentY = (currentY + Constants.BAR_WIDTH);
        }
    }

    void drawClearingRectangle(Graphics2D graphics2D) {
        graphics2D.setPaint(Constants.BACKGROUND_COLOR);
        graphics2D.fill(new Rectangle((int)(this.getVisibleRect().getMaxX()  - Constants.PADDING_HORIZONTAL_RIGHT )
                , (int)this.getVisibleRect().getY() + Constants.PADDING_VERTICAL
                , Constants.PADDING_HORIZONTAL_RIGHT
                , (int)this.getVisibleRect().getHeight()));

        graphics2D.fill(new Rectangle((int)this.getVisibleRect().getX()
                , (int)this.getVisibleRect().getY() + Constants.PADDING_VERTICAL
                , Constants.PADDING_HORIZONTAL_LEFT + this.dateWidth
                , (int)this.getVisibleRect().getHeight()));

    }

    void drawRectangles(Graphics2D graphics2D, double currentStartX, double currentEndX, double currentY, int i) {

        graphics2D.setPaint(Constants.MEETING_COLOR);
        Rectangle2D.Double temp = new Rectangle.Double(currentStartX
                , currentY
                , currentEndX - currentStartX
                , Constants.BAR_WIDTH);
        rectangle.add(temp);

        graphics2D.fill(temp);

        if (this.writeStayPointNames) {
            graphics2D.setPaint(Color.BLACK);
            graphics2D.drawString(Integer.toString(i), (int) temp.getCenterX(), (int) temp.getCenterY());
        }
    }

    void drawDateTimeLabels(Graphics2D graphics2D) {

        double currentY = this.getVisibleRect().getY();
        double totalWidth = (this.groupGanttController.numberOfUniquePlacesSelectedPeopleMeet() * Constants.BAR_WIDTH) / 2.0;
        graphics2D.setPaint(Color.black);
        label_line(graphics2D, Constants.PADDING_HORIZONTAL_LEFT - max
                - this.dateWidth
                , currentY + this.getVisibleRect().getHeight() / 2 - this.dateHeight / 2, 90 * java.lang.Math.PI / 180, new DateTime((long) millisAtPixel(Constants.PADDING_HORIZONTAL_LEFT + this.dateWidth)).withZone(
                DateTimeZone.forID(Constants.TIMEZONE_TO_DISPLAY)).toString(Constants.DATE_PATTERN));
        label_line(graphics2D, this.getVisibleRect().getWidth() + this.dateWidth - Constants.PADDING_HORIZONTAL_RIGHT
                , currentY + this.getVisibleRect().getHeight() / 2 - this.dateHeight / 2, 90 * java.lang.Math.PI / 180
                , new DateTime((long) millisAtPixel(this.getVisibleRect().getWidth() - Constants.PADDING_HORIZONTAL_RIGHT - this.dateWidth)).withZone(
                DateTimeZone.forID(Constants.TIMEZONE_TO_DISPLAY)).toString(Constants.DATE_PATTERN));
        graphics2D.setPaint(new Color(0, 0, 0, 80));

//        graphics2D.drawString(this.currentEndDate.withZone(
//                DateTimeZone.forID(Constants.TIMEZONE_TO_DISPLAY)).toString(Constants.DATE_PATTERN)
//                , this.getWidth() - Constants.PADDING - this.dateWidth
    }

    void drawDateTimeLabelsFold(Graphics2D graphics2D){
        for (int i = 0; i < this.folds.size() - 1; i = i + 2) {
//            graphics2D.setPaint(new Color(0,0,0,80-( this.initialAlpha)));
            graphics2D.setPaint(new Color(0, 0, 0, 255 - this.initialAlpha));

            Font font = new Font("Tamoha", Font.BOLD, (int) Constants.TEXT_SIZE - 6);
            graphics2D.setFont(font);
            if (i == this.indexPolygon) {

                // drawing vertical labels
//                label_line(graphics2D
//                        , folds.get(i) - this.dateWidth/2
//                        , (int) (this.getVisibleRect().getY() +this.getVisibleRect().getHeight() - Constants.PADDING_VERTICAL)
//                        , 90 * java.lang.Math.PI / 180
//                        , new DateTime((long) millisAtPixel(folds.get(i))).withZone(
//                        DateTimeZone.forID(Constants.TIMEZONE_TO_DISPLAY)).toString("HH:mm dd MMM"));
//                label_line(graphics2D
//                        , folds.get(i +1) - this.dateWidth/2
//                        , (int) (this.getVisibleRect().getY() +this.getVisibleRect().getHeight() - Constants.PADDING_VERTICAL)
//                        , 90 * java.lang.Math.PI / 180
//                        , new DateTime((long) millisAtPixel(folds.get(i+1))).withZone(
//                        DateTimeZone.forID(Constants.TIMEZONE_TO_DISPLAY)).toString("HH:mm dd MMM"));
                String date = new DateTime((long) millisAtPixel(folds.get(i))).withZone(
                        DateTimeZone.forID(Constants.TIMEZONE_TO_DISPLAY)).toString(Constants.DATE_PATTERN);
                graphics2D.drawString(date
                        , (int) (double) folds.get(i) - graphics2D.getFontMetrics().stringWidth(date)
                        , (int) (this.getVisibleRect().getY() + Constants.PADDING_VERTICAL - graphics2D.getFontMetrics().stringWidth("H") + 4));
                graphics2D.drawString(new DateTime((long) millisAtPixel(folds.get(i + 1))).withZone(
                        DateTimeZone.forID(Constants.TIMEZONE_TO_DISPLAY)).toString(Constants.DATE_PATTERN)
                        , (int) (double) folds.get(i + 1)
                        , (int) (this.getVisibleRect().getY() + Constants.PADDING_VERTICAL - graphics2D.getFontMetrics().stringWidth("H") + 4));

            }

            double start = millisAtPixel(this.folds.get(i));
            double end = millisAtPixel(this.folds.get(i + 1));
            double pos = this.folds.get(i) + (this.folds.get(i + 1) - this.folds.get(i)) / 2.0;

            double range = (end - start) / (1000.0 * 60.0 * 60.0);

            if (range < (1.0 / (60.0 * 60.0))) {
                Seconds seconds = Seconds.secondsBetween(new DateTime((long) start), new DateTime((long) end));
                String sec = Integer.toString(seconds.getSeconds());
//                graphics2D.drawString(sec
//                        , (int) (pos - graphics2D.getFontMetrics().stringWidth(sec)/2)
//                        , (int) (this.getVisibleRect().getY() +this.getVisibleRect().getHeight() / 2));


                graphics2D.drawString(sec
                        , (int) (pos - graphics2D.getFontMetrics().stringWidth(sec) / 2)
                        , (int) (this.getVisibleRect().getY() + this.getVisibleRect().getHeight() - Constants.PADDING_VERTICAL));
                graphics2D.drawString("s"
                        , (int) (pos - graphics2D.getFontMetrics().stringWidth("s") / 2)
                        , (int) (this.getVisibleRect().getY() + this.getVisibleRect().getHeight() - Constants.PADDING_VERTICAL) + this.dateWidth);
//                label_line(graphics2D,(int) (pos - graphics2D.getFontMetrics().stringWidth("H")/2)
//                        , (int) (this.getVisibleRect().getY() +this.getVisibleRect().getHeight() - Constants.PADDING_VERTICAL )
//                        ,45 * java.lang.Math.PI / 180
//                        ,sec);

            } else if (range < (1.0 )) {
                Minutes minutes = Minutes.minutesBetween(new DateTime((long) start), new DateTime((long) end));
                String min = Integer.toString(minutes.getMinutes());

                graphics2D.drawString(min
                        , (int) (pos - graphics2D.getFontMetrics().stringWidth(min) / 2)
                        , (int) (this.getVisibleRect().getY() + this.getVisibleRect().getHeight() - Constants.PADDING_VERTICAL));

                graphics2D.drawString("m"
                        , (int) (pos - graphics2D.getFontMetrics().stringWidth("m") / 2)
                        , (int) (this.getVisibleRect().getY() + this.getVisibleRect().getHeight() - Constants.PADDING_VERTICAL) + this.dateWidth);
//                label_line(graphics2D, (int) ((pos - graphics2D.getFontMetrics().stringWidth("H") / 2))
//                        , (int) (this.getVisibleRect().getY() + this.getVisibleRect().getHeight() - Constants.PADDING_VERTICAL)
//                        , 45 * java.lang.Math.PI / 180
//                        , min);
//                graphics2D.drawString(min
//                        , (int) (pos - graphics2D.getFontMetrics().stringWidth(min)/2)
//                        , (int) (this.getVisibleRect().getY() +this.getVisibleRect().getHeight() / 2));
            } else if (range < 24.0) {
                Hours hours = Hours.hoursBetween(new DateTime((long) start), new DateTime((long) end));
                String hour = Integer.toString(hours.getHours());
//                graphics2D.drawString(hour, (int) (pos - graphics2D.getFontMetrics().stringWidth(hour)/2)
//                        , (int) (this.getVisibleRect().getY() +this.getVisibleRect().getHeight() / 2));
                graphics2D.drawString(hour
                        , (int) (pos - graphics2D.getFontMetrics()  .stringWidth(hour) / 2)
                        , (int) (this.getVisibleRect().getY() + this.getVisibleRect().getHeight() - Constants.PADDING_VERTICAL));
                graphics2D.drawString("h"
                        , (int) (pos - graphics2D.getFontMetrics().stringWidth("h") / 2)
                        , (int) (this.getVisibleRect().getY() + this.getVisibleRect().getHeight() - Constants.PADDING_VERTICAL) + this.dateWidth);
//                label_line(graphics2D,(int) (pos - graphics2D.getFontMetrics().stringWidth("H")/2)
//                        , (int) (this.getVisibleRect().getY() +this.getVisibleRect().getHeight() - Constants.PADDING_VERTICAL)
//                        ,45 * java.lang.Math.PI / 180
//                        ,hour);
            } else {
                Days days = Days.daysBetween(new DateTime((long) start), new DateTime((long) end));
                String day = Integer.toString(days.getDays());
//                graphics2D.drawString(day
//                        , (int) (pos - graphics2D.getFontMetrics().stringWidth(day)/2)
//                        , (int) (this.getVisibleRect().getY() + this.getVisibleRect().getHeight() / 2));
                graphics2D.drawString(day
                        , (int) (pos - graphics2D.getFontMetrics().stringWidth(day) / 2)
                        , (int) (this.getVisibleRect().getY() + this.getVisibleRect().getHeight() - Constants.PADDING_VERTICAL));
                graphics2D.drawString(""
                        , (int) (pos - graphics2D.getFontMetrics().stringWidth("d") / 2)
                        , (int) (this.getVisibleRect().getY() + this.getVisibleRect().getHeight() - Constants.PADDING_VERTICAL) + this.dateWidth);

//                label_line(graphics2D,(int) (pos - graphics2D.getFontMetrics().stringWidth("H")/2)
//                        , (int) (this.getVisibleRect().getY() +this.getVisibleRect().getHeight() - Constants.PADDING_VERTICAL)
//                        ,45 * java.lang.Math.PI / 180
//                        ,day);
            }

        }
    }

    void label_line(Graphics g, double x, double y, double theta, String label) {

        Graphics2D g2D = (Graphics2D) g;
        // Create a rotation transformation for the font.
        AffineTransform fontAT = new AffineTransform();
        // get the current font
        Font theFont = g2D.getFont();
        // Derive a new font using a rotation transform
        fontAT.rotate(theta);
        Font theDerivedFont = theFont.deriveFont(fontAT);
        // set the derived font in the Graphics2D context
        g2D.setFont(theDerivedFont);
        // Render a string using the derived font
        g2D.drawString(label, (int) x, (int) y);
        // put the original font back
        g2D.setFont(theFont);
        fontAT.rotate(-theta);

    }

    void drawGanttLines(Graphics2D graphics2D) {
        graphics2D.setPaint(Color.BLACK);
        graphics2D.setStroke(new BasicStroke());
        // horizontal line
//        graphics2D.drawLine(Constants.PADDING + this.dateWidth
//                , this.getHeight() - Constants.PADDING
//                , this.getWidth() - Constantsthis.getVisibleRect().getWidth.PADDING - this.dateWidth
//                , this.getHeight() - Constants.PADDING
//        );

//        // vertical line
        graphics2D.drawLine(Constants.PADDING_HORIZONTAL_LEFT + this.dateWidth
                , this.getHeight() - Constants.PADDING_VERTICAL
                -
                ((this.groupGanttController.isDiffRows()) ? this.groupGanttController.totalNumberOfRowsWhenSeperatePerson() * Constants.BAR_WIDTH :
                        this.groupGanttController.numberOfUniquePlacesSelectedPeopleMeet() * Constants.BAR_WIDTH)
                , Constants.PADDING_HORIZONTAL_LEFT + this.dateWidth
                , this.getHeight() - Constants.PADDING_VERTICAL);
    }

    public ArrayList<Rectangle.Double> getRectangle() {
        return rectangle;
    }

    public void setGroupGanttController(GroupGanttController groupGanttController) {
        this.groupGanttController = groupGanttController;
    }

    public void setCurrentStartDate(double currentStartDate) {
        this.currentStartDate = currentStartDate;
    }

    public void setCurrentEndDate(double currentEndDate) {
        this.currentEndDate = currentEndDate;
    }

    /* Thanks to Michael McGuffin (http://profs.etsmtl.ca/mmcguffin/)
     * for the framework for zooming in code. And for the bug fixes
    */
    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        if (!animation && !autoFrame && !autoFrameWithFolds && !autoFrameWithUnFold && !frameVisible && !frameVisibleWithFold && !frameVisibleWithUnFold ) {
            double zoomFactor = 1.15f;
            int notches = e.getWheelRotation();
            if (notches < 0) {
                zoomIn(zoomFactor, e.getX() - (Constants.PADDING_HORIZONTAL_LEFT + this.dateWidth), this.currentStartDate, this.currentEndDate);

            } else {
                zoomIn(1.0f / zoomFactor, e.getX() - (Constants.PADDING_HORIZONTAL_LEFT + this.dateWidth), this.currentStartDate, this.currentEndDate);

            }
        }
        if (this.animatePoint.size() != 0 && this.zoomAnimate)
         {
            this.animation = true;
            this.autoFrameWithFolds = false;
            this.autoFrame = false;
            this.autoFrameWithUnFold = false;
            if (this.animation)
                animationGantt.startGanttAnimation();
        } else {
            this.repaint();
        }
    }

    public void zoomIn(
            double zoomFactor // greater than 1 to zoom in, between 0 and 1 to zoom out
            , double centerXInPixels, double startDate, double endDate) {
        double widthOfWidgetInPixels = this.getVisibleRect().getWidth() - Constants.PADDING_HORIZONTAL_LEFT - Constants.PADDING_HORIZONTAL_RIGHT - 2 * this.dateWidth;
        double startTime = startDate;
        double endTime = endDate;
        double scaleFactorInWorldSpaceUnitsPerPixel = (endTime - startTime) / widthOfWidgetInPixels;
        double offsetXInPixels = -startTime / scaleFactorInWorldSpaceUnitsPerPixel; // where the world's origin is, in pixel space
        scaleFactorInWorldSpaceUnitsPerPixel /= zoomFactor;
        offsetXInPixels = centerXInPixels - (centerXInPixels - offsetXInPixels) * zoomFactor;
        startTime = -offsetXInPixels * scaleFactorInWorldSpaceUnitsPerPixel;
        endTime = scaleFactorInWorldSpaceUnitsPerPixel * widthOfWidgetInPixels + startTime;
        DateTime tempStartDateOfTimeLine = new DateTime((long) startTime);
        DateTime tempEndDateOfTimeLine = new DateTime((long) endTime);
        try {
            double n = tempEndDateOfTimeLine.getMillis() - tempStartDateOfTimeLine.getMillis();
            double millisPixel = n / (this.getVisibleRect().getWidth() -  2 *this.dateWidth - Constants.PADDING_HORIZONTAL_LEFT - Constants.PADDING_HORIZONTAL_RIGHT);
            if (tempEndDateOfTimeLine.isAfter(tempStartDateOfTimeLine) && millisPixel >= 1) {
                this.currentStartDate = tempStartDateOfTimeLine.getMillis();
                this.currentEndDate = tempEndDateOfTimeLine.getMillis();
            } else {
                this.currentStartDate = startDate;
                this.currentEndDate = endDate;

            }
        } catch (Exception e) {
            if (e instanceof ArithmeticException) if (Constants.VERBOSE)
                System.out.println("Cannot zoom further");
            this.currentStartDate = startDate;
            this.currentEndDate = endDate;
        }
    }

    @Override
    public void mouseDragged(MouseEvent e) {

        previousMouseX = mouseX;
        previousMouseY = mouseY;
        mouseX = e.getX();
        mouseY = e.getY();

        int deltaX = previousMouseX - mouseX;
        int deltaY = previousMouseY - mouseY;
        if (!animation && !autoFrame && !autoFrameWithFolds && !autoFrameWithUnFold && !frameVisible && !frameVisibleWithFold && !frameVisibleWithUnFold ) {
            translateToDateTime(deltaX);
            checkMousePositionInsideRectangle();
            checkMousePositionInsidePolygon();

            try {
                this.groupGanttController.drawCurves(this.currentSelectedMeet, this.rectangle.get(
                        this.drawnMeet.indexOf(this.currentSelectedMeet)
                ).getCenterX(), this.rectangle.get(
                        this.drawnMeet.indexOf(this.currentSelectedMeet)).getCenterY());
                this.oldSelectedMeet = this.currentSelectedMeet;
            } catch (Exception e1) {
                if(Constants.VERBOSE)

                    System.out.println("not inside any meeting");
                this.oldSelectedMeet = this.currentSelectedMeet;

                this.groupGanttController.drawCurves(this.currentSelectedMeet, -1, -1);
            }
            this.repaint();
        }

    }

    public void translateToDateTime(int deltaX) {
        this.currentStartDate = (this.currentStartDate + deltaX * millisPerPixel);
        this.currentEndDate = (this.currentEndDate + deltaX * millisPerPixel);
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
        if (this.drawRuler && e.isControlDown()) {
            if (e.getX() - mouseX > 0) {
                this.rulerEnd = this.rulerEnd + 1;
                this.mouseEndDate = millisAtPixel(this.rulerEnd + 1);
            } else {
                this.rulerEnd = this.rulerEnd - 1;
                this.mouseEndDate = millisAtPixel(this.rulerEnd - 1);
            }
        } else if (this.drawRuler && e.isShiftDown()) {
            this.rulerEnd = e.getX();
            this.mouseEndDate = millisAtPixel(e.getX());
        }
        mouseX = e.getX();
        mouseY = e.getY();
        checkMousePositionInsidePolygon();

        if (!animation && !autoFrame && !autoFrameWithFolds && !autoFrameWithUnFold && !frameVisible && !frameVisibleWithFold && !frameVisibleWithUnFold ) {
            checkMousePositionInsideRectangle();
            if (this.oldSelectedMeet != this.currentSelectedMeet) {
                try {
                    this.groupGanttController.drawCurves(this.currentSelectedMeet, this.rectangle.get(
                            this.drawnMeet.indexOf(this.currentSelectedMeet)
                    ).getCenterX(), this.rectangle.get(
                            this.drawnMeet.indexOf(this.currentSelectedMeet)).getCenterY());
                    this.oldSelectedMeet = this.currentSelectedMeet;
                } catch (Exception e1) {
                    if(Constants.VERBOSE)

                        System.out.println("not inside any meeting");
                    this.oldSelectedMeet = this.currentSelectedMeet;
                    this.groupGanttController.drawCurves(this.currentSelectedMeet, -1, -1);
                }

            }
            this.repaint();

        }
    }


    void checkMousePositionInsidePolygon() {
        for (int i = 0; i < this.polygons.size() - 1; i = i + 2) {
            Polygon polygon = this.polygons.get(i);
            Polygon polygon1 = this.polygons.get(i + 1);
            if (polygon.contains(mouseX, mouseY) || polygon1.contains(mouseX, mouseY)) {
                this.indexPolygon = i;
                break;
            } else {
                this.indexPolygon = -1;
            }
        }
    }

    void checkMousePositionInsideRectangle() {
        for (int i = 0; i < this.rectangle.size(); ++i) {
            Rectangle.Double rect = this.rectangle.get(i);
            if (rect.contains(mouseX, mouseY)) {
                this.currentSelectedMeet = this.drawnMeet.get(i);
                break;
            } else {
                this.currentSelectedMeet = null;
            }
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {


    }

    void setAutoFrameVisibleInterval() {

        try {
            this.finalAutoFrameStart = this.drawnMeet.get(0).getInterval().getStartMillis();
            this.finalAutoFrameEnd = this.drawnMeet.get(this.drawnMeet.size() - 1).getInterval().getEndMillis();

            this.listStart = this.groupGanttController.getMeetingPointsToDisplay().indexOf(this.drawnMeet.get(0));
            this.listEnd = this.groupGanttController.getMeetingPointsToDisplay().indexOf(this.drawnMeet.get(
                            this.drawnMeet.size() - 1)
            );
            this.finalStartMillis = this.finalAutoFrameStart - this.diffMillis.get(this.listStart);
            this.finalEndMillis = this.finalAutoFrameEnd - this.diffMillis.get(this.listEnd);
        } catch (Exception e) {
                System.out.println("AutoFrame Visible problem");
            this.animationGantt.animationParameter = 1.0f;
        }
    }

    void setAutoFrameState() {
        try {
            this.finalStartMillis = this.groupGanttController.getMeetingPointsToDisplay().get(0).getInterval().getStart().getMillis();
            this.finalEndMillis = this.groupGanttController.getMeetingPointsToDisplay().get(
                    this.groupGanttController.getMeetingPointsToDisplay().size() - 1).getInterval().getEnd().getMillis();
        } catch (Exception e) {
            if(Constants.VERBOSE)

                System.out.println("AutoFrame problem");
            this.animationGantt.animationParameter = 1.0f;
        }
    }

    void updateAutoFrameVisibleInterval(double animationParameter) {

        if (animationParameter == 1.0f)
            this.frameVisible = false;

        else {
            //for framing visible interval
            this.finalStartMillis = this.finalAutoFrameStart - this.diffMillis.get(this.listStart);
            this.finalEndMillis = this.finalAutoFrameEnd
                    - this.diffMillis.get(this.listEnd);
            this.currentStartDate = (this.currentStartDate + (this.finalStartMillis - this.currentStartDate) * animationParameter);
            this.currentEndDate = (this.currentEndDate + (this.finalEndMillis - this.currentEndDate) * animationParameter);
//        this.currentStartDate = (this.currentStartDate + th);
//        this.currentEndDate = (this.currentEndDate + (this.finalEndMillis - this.currentEndDate) * animationParameter);
        }

    }

    void updateAutoFrame(double animationParameter) {
        this.finalStartMillis = this.groupGanttController.getMeetingPointsToDisplay()
                .get(0).getInterval().getStart().getMillis();
        this.finalEndMillis = this.groupGanttController.getMeetingPointsToDisplay().get(
                this.groupGanttController.getMeetingPointsToDisplay().size() - 1).getInterval().getEnd().getMillis()
                - this.diffMillis.get(this.groupGanttController.getMeetingPointsToDisplay().size() - 1);

        //for framing visible interval
//        this.finalStartMillis = this.finalAutoFrameStart - this.diffMillis.get(this.listStart);
//        this.finalEndMillis = this.finalAutoFrameEnd
//                - this.diffMillis.get(this.listEnd);
        this.currentStartDate = (this.currentStartDate + (this.finalStartMillis - this.currentStartDate) * animationParameter);
        this.currentEndDate = (this.currentEndDate + (this.finalEndMillis - this.currentEndDate) * animationParameter);
//        this.currentStartDate = (this.currentStartDate + th);
//        this.currentEndDate = (this.currentEndDate + (this.finalEndMillis - this.currentEndDate) * animationParameter);

        if (animationParameter == 1.0f)
            this.autoFrame = false;
    }

    void setResetAnimation() {
        for (int i = 0; i < this.finalDiffMillis.size(); i++) {
            this.finalDiffMillis.set(i, 0.0);
//            this.increaseMillis.set(i,
//                    (this.finalDiffMillis.get(i) - this.diffMillis.get(i)) / (Constants.NUM_FRAMES_PER_SECOND * Constants.DURATION_OF_ANIMATION));
        }
        this.finalAlpha = 255;
        this.finalAniSlope = 60.0;

    }

    void setAnimationState() {

        this.finalAniSlope = 4.0;
        this.finalAlpha = 0;
        double temp = 0.0;
        this.animatePoint = new ArrayList<>();
//        this.increaseMillis = new ArrayList<>();
        int j = 0;
        this.finalDiffMillis.set(0, temp);
        for (int i = 0; i < this.groupGanttController.getMeetingPointsToDisplay().size(); i++) {

            double currentStart;
            double currentEnd;
            double currentStartMillis = 0;
            double currentEndMillis = 0;
            currentStartMillis = this.groupGanttController.getMeetingPointsToDisplay().get(j).getInterval().getEnd().getMillis() - this.currentStartDate;
            currentEndMillis = this.groupGanttController.getMeetingPointsToDisplay().get(i).getInterval().getStart().getMillis() - this.currentStartDate;
            currentStart = Constants.PADDING_HORIZONTAL_LEFT + this.dateWidth + this.widthPerMilli * currentStartMillis;
            currentEnd = Constants.PADDING_HORIZONTAL_LEFT + this.dateWidth + this.widthPerMilli * currentEndMillis;
            if ((currentEnd - currentStart) >= Constants.FOLD_SIZE) {

                MeetingPoint meetingPoint = this.groupGanttController.getMeetingPointsToDisplay().get(i);
                MeetingPoint meetingPoint1 = this.groupGanttController.getMeetingPointsToDisplay().get(j);
                double currentStartMilli = meetingPoint.getInterval().getEnd().getMillis() - this.currentStartDate;
                double currentEndMilli = meetingPoint1.getInterval().getStart().getMillis() - this.currentStartDate;
                this.animatePoint.add(j);
                this.animatePoint.add(i);
                temp = temp +
                        (this.groupGanttController.getMeetingPointsToDisplay().get(i).getInterval().getStart().getMillis() - (

                                this.groupGanttController.getMeetingPointsToDisplay().get(j).getInterval().getEnd().getMillis()
                                        +
                                        (this.millisPerPixel * Constants.FOLD_SIZE)));
            }
            if (this.groupGanttController.getMeetingPointsToDisplay().get(j).getInterval().getEnd().getMillis()
                    <
                    this.groupGanttController.getMeetingPointsToDisplay().get(i).getInterval().getEnd().getMillis()) {
                j = i;
            }
            this.finalDiffMillis.set(i, temp);
//            this.increaseMillis.add((this.finalDiffMillis.get(i) - this.diffMillis.get(i))/(Constants.NUM_FRAMES_PER_SECOND * Constants.DURATION_OF_ANIMATION ));

        }

        this.finalDiffMillis.set(this.groupGanttController.getMeetingPointsToDisplay().size() - 1, temp);
//        this.increaseMillis.add((this.finalDiffMillis.get(this.groupGanttController.getMeetingPointsToDisplay().size() - 1)
//                - this.diffMillis.get(this.groupGanttController.getMeetingPointsToDisplay().size() - 1))/(Constants.NUM_FRAMES_PER_SECOND * Constants.DURATION_OF_ANIMATION ));
    }

    void updateUnFoldWithFrame(double animationParameter) {
        for (int i = 0; i < this.diffMillis.size(); i++) {
            this.diffMillis.set(i, this.diffMillis.get(i) + (this.finalDiffMillis.get(i) - this.diffMillis.get(i)) * animationParameter);
        }
        this.finalStartMillis = this.groupGanttController.getMeetingPointsToDisplay().get(0).getInterval().getStart().getMillis();
        this.finalEndMillis = this.groupGanttController.getMeetingPointsToDisplay().get(
                this.groupGanttController.getMeetingPointsToDisplay().size() - 1).getInterval().getEnd().getMillis()
                - this.finalDiffMillis.get(this.groupGanttController.getMeetingPointsToDisplay().size() - 1);
        this.currentStartDate = (this.currentStartDate + (this.finalStartMillis - this.currentStartDate) * animationParameter);
        this.currentEndDate = (this.currentEndDate + (this.finalEndMillis - this.currentEndDate) * animationParameter);
        this.currentAniSlope = this.currentAniSlope + (this.finalAniSlope - this.currentAniSlope) * animationParameter;

        this.initialAlpha = (int) (this.initialAlpha + (this.finalAlpha - this.initialAlpha) * animationParameter);

        if (animationParameter == 1.0f) {
            this.autoFrameWithUnFold = false;
            this.animatePoint = new ArrayList<>();
        }

    }

    void updateUnFoldWithFrameVisibleInterval(double animationParameter) {
        for (int i = 0; i < this.diffMillis.size(); i++) {
            this.diffMillis.set(i, this.diffMillis.get(i) + (this.finalDiffMillis.get(i) - this.diffMillis.get(i)) * animationParameter);
        }

        this.finalStartMillis = this.finalAutoFrameStart - this.finalDiffMillis.get(this.listStart);
        this.finalEndMillis = this.finalAutoFrameEnd
                - this.finalDiffMillis.get(this.listEnd);
        this.currentStartDate = (this.currentStartDate + (this.finalStartMillis - this.currentStartDate) * animationParameter);
        this.currentEndDate = (this.currentEndDate + (this.finalEndMillis - this.currentEndDate) * animationParameter);
        this.currentAniSlope = this.currentAniSlope + (this.finalAniSlope - this.currentAniSlope) * animationParameter;

        this.initialAlpha = (int) (this.initialAlpha + (this.finalAlpha - this.initialAlpha) * animationParameter);

        if (animationParameter == 1.0f) {
            this.frameVisibleWithUnFold = false;
            this.animatePoint = new ArrayList<>();
        }

    }

    @Override
    public void mousePressed(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.

        if (e.isAltDown() && e.getButton() == MouseEvent.BUTTON3) {
            this.animation = false;
            this.autoFrameWithFolds = false;
            this.autoFrame = false;
            this.autoFrameWithUnFold = false;
            animationGantt.resetGanttAnimation();

        } else if (e.getButton() == MouseEvent.BUTTON3) {
            this.animation = true;
            this.autoFrameWithFolds = false;
            this.autoFrame = false;
            this.autoFrameWithUnFold = false;
            animationGantt.startGanttAnimation();
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
        this.requestFocus();
    }

    @Override
    public void mouseExited(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
        this.currentSelectedMeet = null;
        this.groupGanttController.drawCurves(this.currentSelectedMeet, -1, -1);
        this.repaint();
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyChar() == 'j') {
            this.overRideBarWidth = true;
            Constants.BAR_WIDTH++;
            recalculateSize();
            this.revalidate();
            this.repaint();

        } else if (e.getKeyChar() == 'k') {
            this.overRideBarWidth = true;
            Font font = new Font("Tamoha", Font.BOLD, (int) Constants.TEXT_SIZE - 6);

            if(Constants.BAR_WIDTH > 3 + font.getSize())
            Constants.BAR_WIDTH--;
            recalculateSize();
            this.revalidate();
            this.repaint();

        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

        if (e.getKeyChar() == 'z') {
            this.drawRuler = true;
            this.rulerStart = mouseX;
            this.rulerEnd = mouseX;
            this.mouseEndDate = millisAtPixel(mouseX);
            this.repaint();
        } else if (e.getKeyChar() == 'x') {
            this.drawRuler = false;
            this.rulerStart = -1;
            this.rulerEnd = -1;
            this.mouseStartDate = -1;
            this.mouseEndDate = -1;
            this.repaint();
        } else if (e.getKeyChar() == 'v') {
            this.frameVisible = true;
            this.autoFrame = false;
            this.animation = false;
            this.autoFrameWithFolds = false;
            this.autoFrameWithUnFold = false;
            frameVisibleWithFold = false;
            this.frameVisibleWithUnFold = false;
            if (frameVisible)
                animationGantt.startAutoFrameVisible();

        } else if (e.getKeyChar() == 'a') {
            this.autoFrame = true;
            this.animation = false;
            this.frameVisible = false;
            this.autoFrameWithFolds = false;
            this.autoFrameWithUnFold = false;
            frameVisibleWithFold = false;
            this.frameVisibleWithUnFold = false;
            if (autoFrame)
                animationGantt.startAutoFrameAnimation();
        } else if (e.getKeyChar() == 'f' && this.animatePoint.size() == 0) {
            this.autoFrameWithFolds = true;
            this.animation = false;
            this.autoFrame = false;
            this.frameVisible = false;
            this.autoFrameWithUnFold = false;
            frameVisibleWithFold = false;
            this.frameVisibleWithUnFold = false;
            if (this.autoFrameWithFolds)
                animationGantt.startAutoFrameWithFolds();
        } else if (e.getKeyChar() == 'f') {
            this.autoFrameWithUnFold = true;
            this.autoFrameWithFolds = false;
            this.animation = false;
            this.autoFrame = false;
            this.frameVisible = false;
            frameVisibleWithFold = false;
            this.frameVisibleWithUnFold = false;
            if (this.autoFrameWithUnFold)
                animationGantt.startUnFoldWithFrame();
        } else if (e.getKeyChar() == 't' && this.animatePoint.size() == 0) {
            frameVisibleWithFold = true;
            this.autoFrameWithFolds = false;
            this.animation = false;
            this.autoFrame = false;
            this.frameVisible = false;
            this.autoFrameWithUnFold = false;
            this.frameVisibleWithUnFold = false;
            if (frameVisibleWithFold)
                animationGantt.startAutoFrameWithFoldsVisibleInterval();
        } else if (e.getKeyChar() == 't') {
            this.frameVisibleWithUnFold = true;
            this.autoFrameWithUnFold = false;
            this.autoFrameWithFolds = false;
            this.animation = false;
            this.autoFrame = false;
            this.frameVisible = false;
            frameVisibleWithFold = false;
            if (this.frameVisibleWithUnFold)
                animationGantt.startUnFoldVisible();
        } else if (e.getKeyChar() == 'c') {
            this.groupGanttController.sort();
            this.repaint();
        } else if (e.getKeyChar() == 'r') {
//            recalculateSize();
            this.groupGanttController.setDiffRows();
            this.recalculateSize();
            this.initialize();
            this.groupGanttController.fireUpdate();
            this.invalidate();
            this.repaint();
            this.groupGanttController.updateMainView();

        } else if (e.getKeyChar() == 'l') {
            this.connectingLines = !this.connectingLines;
            this.invalidate();
            this.repaint();

        }
        else if (e.getKeyChar() == 'p') {
            this.foldShades++;
            if (this.foldShades > 3)this.foldShades = 0;
            this.revalidate();
            this.repaint();
        }
        else if (e.getKeyChar() == 'o') {
            this.zoomAnimate = !this.zoomAnimate;
        }
        else if (e.getKeyChar() == 'i') {
            this.meetLines = !this.meetLines;
            this.revalidate();
            this.repaint();
        }
        else if (e.getKeyChar() == 'b') {
            this.useOfColors++;
            if(this.useOfColors > 1) this.useOfColors =0;
            this.revalidate();
            this.repaint();
        }
        else if (e.getKeyChar() == 'n') {
            this.typeOfLabeling++;
            if(this.typeOfLabeling > 2) this.typeOfLabeling =0;
            this.revalidate();
            this.repaint();
        }

    }

    double millisAtPixel(double milliPixel) {
        double mouseMillis;
        if (this.folds.size() == 0) {
            double pixel = milliPixel - (Constants.PADDING_HORIZONTAL_LEFT + this.dateWidth);
            mouseMillis = this.currentStartDate + pixel * millisPerPixel;
            return mouseMillis;
        } else {
            double pixel = milliPixel - (Constants.PADDING_HORIZONTAL_LEFT + this.dateWidth);
            try {
                if (milliPixel <= folds.get(0)) {
                    mouseMillis = this.currentStartDate + pixel * millisPerPixel;
                    return mouseMillis;
                } else if (milliPixel > folds.get(folds.size() - 1)) {
                    double diff = this.diffMillis.get(this.animatePoint.get(folds.size() - 1));
                    mouseMillis = this.currentStartDate + pixel * millisPerPixel + diff;
                    return mouseMillis;
                } else {
                    for (int i = 1; i < folds.size(); i = i + 2) {
                        if (milliPixel > folds.get(i) && milliPixel <= folds.get(i + 1)) {
                            double diff = this.diffMillis.get(this.animatePoint.get(i + 1));
                            mouseMillis = this.currentStartDate + pixel * millisPerPixel + diff;
                            return mouseMillis;
                        }
                        if (milliPixel > folds.get(i - 1) && milliPixel <= folds.get(i)) {
                            double startDate = this.groupGanttController.getMeetingPointsToDisplay().get(this.animatePoint.get(i - 1)).getInterval().getEnd().getMillis();
                            double endDate = this.groupGanttController.getMeetingPointsToDisplay().get(this.animatePoint.get(i)).getInterval().getStart().getMillis();
                            double diff = endDate - startDate;
                            double foldsMilisPerPixel = diff / (folds.get(i) - folds.get(i - 1));
                            pixel = milliPixel - folds.get(i - 1);
                            mouseMillis = startDate + foldsMilisPerPixel * pixel;
                            return mouseMillis;
                        }
                    }
                }
            } catch (Exception e) {
                if(Constants.VERBOSE)

                    System.out.println("Millis at pixel problem");
            }
        }
        return 0;
    }

    double pixelAtMillis(double millis) {
        double diff = 0;
        double x = 0;
        if (this.animatePoint.size() == 0) {
            diff = millis - this.currentStartDate;
            x = Constants.PADDING_HORIZONTAL_LEFT + this.dateWidth + this.widthPerMilli * diff;
            return x;

        } else {
            if (millis < this.groupGanttController.getMeetingPointsToDisplay().get(this.animatePoint.get(0)).getInterval().getEnd().getMillis()) {
                diff = millis - this.currentStartDate;
                x = Constants.PADDING_HORIZONTAL_LEFT
                        +
                        this.dateWidth
                        +
                        this.widthPerMilli * diff
                ;
                return x;
            } else if (millis >= this.groupGanttController.getMeetingPointsToDisplay().get(this.animatePoint.get(this.animatePoint.size() - 1)).getInterval().getStart().getMillis()) {
                diff = millis - this.currentStartDate;
                x = Constants.PADDING_HORIZONTAL_LEFT
                        +
                        this.dateWidth
                        +
                        this.widthPerMilli * diff
                        -
                        this.widthPerMilli * this.diffMillis.get(this.animatePoint.get(this.animatePoint.size() - 1));
                return x;
            } else {
                for (int i = 1; i < this.animatePoint.size(); i = i + 2) {
                    if (millis >= this.groupGanttController.getMeetingPointsToDisplay().get(this.animatePoint.get(i)).getInterval().getStart().getMillis()
                            && millis < this.groupGanttController.getMeetingPointsToDisplay().get(this.animatePoint.get(i + 1)).getInterval().getEnd().getMillis()) {
                        diff = millis - this.currentStartDate;
//                        System.out.println("milli" + millis);
//                        double foldWidthPerMillis =
//                                (this.groupGanttController.getMeetingPointsToDisplay().get(this.animatePoint.get(i + 1)).getInterval().getStart().getMillis() - this.groupGanttController.getMeetingPointsToDisplay().get(this.animatePoint.get(i)).getInterval().getEnd().getMillis())
//                                /
//                                        (double) (this.folds.get(i + 1) - this.folds.get(i)) ;
//                        System.out.println(foldWidthPerMillis+"after mulyi"+(millis * foldWidthPerMillis));
//                        x =  millis * foldWidthPerMillis;
                        x = Constants.PADDING_HORIZONTAL_LEFT
                                + this.dateWidth
                                + this.widthPerMilli
                                * diff
                                - this.widthPerMilli
                                * this.diffMillis.get(this.animatePoint.get(i + 1));
//                        System.out.println("folds"+folds.get(i)+"pixel" +
//                                x);

                        return (int) x;
                    }
                    if (millis >= this.groupGanttController.getMeetingPointsToDisplay().get(this.animatePoint.get(i - 1)).getInterval().getEnd().getMillis()
                            && millis < this.groupGanttController.getMeetingPointsToDisplay().get(this.animatePoint.get(i)).getInterval().getStart().getMillis()) {

                        double startDate = this.groupGanttController.getMeetingPointsToDisplay().get(this.animatePoint.get(i - 1)).getInterval().getEnd().getMillis();
                        double endDate = this.groupGanttController.getMeetingPointsToDisplay().get(this.animatePoint.get(i)).getInterval().getStart().getMillis();
                        diff = endDate - startDate;
                        double pixelPerMillis = (folds.get(i) - folds.get(i - 1)) / diff;
                        diff = millis - startDate;
//                        System.out.println("milli" + millis);
//                        System.out.println(foldWidthPerMillis+"after mulyi"+(millis * foldWidthPerMillis));
                        x = folds.get(i - 1) + diff * pixelPerMillis;
//                        System.out.println("folds"+folds.get(i)+"pixel" +
//                                x);
                        return x;
                    }
                }
            }
        }
        return 1;
    }
}