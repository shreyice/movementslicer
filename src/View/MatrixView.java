package View;

import Algorithms.Constants;
import AppEvent.AppEvent;
import Controller.MatrixController;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import static Algorithms.Constants.ROLLOVER_HIGHLIGHT_MEETING;

/**
 * Created with IntelliJ IDEA.
 * User: Shrey
 * Date: 10/9/13
 * Time: 5:29 PM
 * To change this template use File | Settings | File Templates.
 */
public class MatrixView extends JPanel implements MouseMotionListener, MouseListener , KeyListener {


    int matrixPaddingHeight;
    int matrixPaddingWidth;
    int height;
    int width;
    int sizeOfEachCell;
    int lengthOfLongestString =0;

    int drawX;
    int drawY;

    int mouseX;
    int mouseY;
    MatrixController controller;
    ArrayList<Integer> selectedPeople;

    long startTime;
    long leftClick;
    long rightClick;

    public int personId = -1;

    int typeOfHighlight = 3;
    int typeOfLabelAroundMatrix = 0;

    int extraCell= 1;

    Font font;

    boolean useOfColor = false;

    public MatrixView() {
        this.addMouseListener(this);
        this.addMouseMotionListener(this);
        this.addKeyListener(this);
        this.setBackground(Color.WHITE);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);    //To change body of overridden methods use File | Settings | File Templates.
        Graphics2D graphics2D = (Graphics2D)g;

        this.height =(int) this.getVisibleRect().getHeight();
        this.width = (int)this.getVisibleRect().getWidth();

//        Font fontLabel = new Font("SansSerif", Font.BOLD,(int) (this.sizeOfEachCell * .4));

        font = new Font("SansSerif", Font.BOLD, (int)((width < height)?width*.02:height*.02));
//        graphics2D.fillRect(0,0,width,height);

        int lengthOfEachCharacter = graphics2D.getFontMetrics(font).getMaxAscent()+ graphics2D.getFontMetrics(font).getMaxDescent();
        for (int i = 0; i < Constants.PEOPLE_NAMES.length; i++) {
            int lenghtOfCurrentName = Constants.PEOPLE_NAMES[i].length();
            if (this.lengthOfLongestString < lenghtOfCurrentName) {
                this.lengthOfLongestString= lenghtOfCurrentName*lengthOfEachCharacter;
            }
        }

        this.height -= lengthOfLongestString;
//        this.width -= lengthOfLongestString;
        this.setBackground(Constants.BACKGROUND_COLOR);


        if(Constants.matrixNumbers) {
            extraCell = 1;
        }
        else {
            extraCell =0;
        }
            if (height < width) {
                Constants.PADDING_MATRIX = (int) 0.01 * this.height;
                this.sizeOfEachCell = (this.height - Constants.PADDING_MATRIX * 2) / (this.controller.numberOfNode() + 2+extraCell);
                this.drawX = Constants.PADDING_MATRIX + (2+extraCell) * this.sizeOfEachCell;
                this.drawY = (this.height - this.sizeOfEachCell * (this.controller.numberOfNode() + extraCell) - Constants.PADDING_MATRIX) / 2;
            } else {
                Constants.PADDING_MATRIX = (int) 0.01 * this.width;
                this.sizeOfEachCell = (this.width - Constants.PADDING_MATRIX * 2) / (this.controller.numberOfNode() + 2+extraCell);
                this.drawX = Constants.PADDING_MATRIX + (2+extraCell) * this.sizeOfEachCell;
                this.drawY = (this.height - this.sizeOfEachCell * (this.controller.numberOfNode() + extraCell) - Constants.PADDING_MATRIX) / 2;
            }


        this.matrixPaddingHeight = Constants.PADDING_MATRIX + 2*this.sizeOfEachCell;
        this.matrixPaddingWidth = Constants.PADDING_MATRIX + 2*this.sizeOfEachCell;
        drawHighlightPeople(graphics2D);

        if (personId != -1) {
            personHighlight(graphics2D);
        }
        drawSelectedMeets(graphics2D);
        drawSelectedPeople(graphics2D);
        drawMatrix(graphics2D);
        drawMatrixLabel(graphics2D);
        fillValues(graphics2D);
        drawHighlightMeeting(graphics2D);


    }

    void personHighlight(Graphics2D graphics2D) {
        int j = this.controller.nodeAtPosition(this.controller.positionOfNode(personId));
        if( j >= Constants.PEOPLE_COLOR.length) j = j % Constants.PEOPLE_COLOR.length ;
        if(this.useOfColor)
        graphics2D.setColor(Constants.PEOPLE_COLOR[j]);

        switch (this.typeOfHighlight){
            case 0:{

                graphics2D.setStroke(new BasicStroke(10.0f));
                graphics2D.drawRect(
                        (int) (this.drawX - this.sizeOfEachCell + sizeOfEachCell * .20),
                        (int) (this.drawY + this.controller.positionOfNode(personId) * sizeOfEachCell + sizeOfEachCell * .20),
                        (int) (sizeOfEachCell - sizeOfEachCell * .40),
                        (int) (sizeOfEachCell - sizeOfEachCell * .40)
                );

                graphics2D.drawRect(
                        (int)(this.drawX + this.controller.positionOfNode(personId) * sizeOfEachCell + sizeOfEachCell*.20)
                        ,(int)(this.drawY + this.controller.numberOfNode() * sizeOfEachCell + sizeOfEachCell*.20 )
                        , (int)(sizeOfEachCell - sizeOfEachCell* .40)
                        , (int)(sizeOfEachCell - sizeOfEachCell* .40)
                );

                graphics2D.setStroke(new BasicStroke());
                break;
            }

            case 1: {

                // horizontal
                graphics2D.drawRect(
                        (this.drawX - this.sizeOfEachCell),
                        (this.drawY + this.controller.positionOfNode(personId) * sizeOfEachCell),
                        (sizeOfEachCell),
                        (sizeOfEachCell)
                );

                //vertical
                graphics2D.drawRect(
                        (this.drawX + this.controller.positionOfNode(personId) * sizeOfEachCell),
                        (this.drawY + this.controller.numberOfNode() * sizeOfEachCell),
                        (sizeOfEachCell),
                        (sizeOfEachCell)
                );
                break;
            }
            case 2:{
                graphics2D.setColor(Constants.ROLLOVER_HIGHLIGHT_MEETING);

                //vertical rectangle highlight
                graphics2D.fillRect((this.drawX + this.controller.positionOfNode(personId) * sizeOfEachCell)
                        , this.drawY
                        , sizeOfEachCell
                        , sizeOfEachCell * (this.controller.numberOfNode() + 2)+ this.lengthOfLongestString);

                //horizontal rectangle highlight
                graphics2D.fillRect(this.drawX - 3*this.sizeOfEachCell
                        , (this.drawY + this.controller.positionOfNode(personId) * sizeOfEachCell)
                        , sizeOfEachCell * (this.controller.numberOfNode() + 3)
                        , sizeOfEachCell);
            }
            case 3:{
                graphics2D.setColor(Constants.ROLLOVER_HIGHLIGHT_PEOPLE);
                graphics2D.drawRect(this.drawX - 2 * this.sizeOfEachCell + 6
                        , this.drawY + this.controller.positionOfNode(personId) * sizeOfEachCell + 6
                        , sizeOfEachCell * 2  - 12
                        , sizeOfEachCell - 12);

                graphics2D.drawRect(this.drawX + this.controller.positionOfNode(personId) * sizeOfEachCell + 6
                        , this.drawY + this.controller.numberOfNode() * sizeOfEachCell + 6
                        , sizeOfEachCell - 12
                        , sizeOfEachCell * 2 - 12);
            }
            }
        graphics2D.setColor(Color.black);
        }


    void drawSelectedPeople(Graphics2D graphics2D) {
        graphics2D.setPaint(Constants.SELECTED_HIGHLIGHT_PEOPLE);

        for (int i = 0; i < this.controller.numberOfNode(); i++) {
                if (this.controller.isHighlightedPerson(i)) {

                    if(Constants.matrixNumbers) {

                        graphics2D.fillRect(this.drawX - this.sizeOfEachCell
                                , this.drawY + this.controller.positionOfNode(i) * sizeOfEachCell
                                , sizeOfEachCell
                                , sizeOfEachCell);
                        graphics2D.fillRect(this.drawX + this.controller.positionOfNode(i) * sizeOfEachCell
                                , this.drawY + (this.controller.numberOfNode()) * sizeOfEachCell
                                , sizeOfEachCell
                                , sizeOfEachCell);
                    }
                    else {
                        graphics2D.setStroke(new BasicStroke(6.0f));
                        graphics2D.drawRect(this.drawX - 2 * this.sizeOfEachCell +(int) 0.04f * sizeOfEachCell
                                , this.drawY + this.controller.positionOfNode(i) * sizeOfEachCell +(int) 0.04f * sizeOfEachCell
                                , sizeOfEachCell * 2 - (int)0.08f*sizeOfEachCell
                                , sizeOfEachCell - (int)0.08f*sizeOfEachCell);
                        graphics2D.drawRect(this.drawX + this.controller.positionOfNode(i) * sizeOfEachCell + (int) 0.04f * sizeOfEachCell
                                , this.drawY + (this.controller.numberOfNode()) * sizeOfEachCell  +(int) 0.04f * sizeOfEachCell
                                , sizeOfEachCell -(int) 0.08f * sizeOfEachCell
                                , sizeOfEachCell *2 -(int) 0.08f * sizeOfEachCell);

                    }
                }
        }
    }

    void drawSelectedMeets(Graphics2D graphics2D) {
        graphics2D.setPaint(Constants.SELECTED_HIGHLIGHT_MEETING);
        for (int i = 0; i < this.controller.numberOfNode(); i++) {
            for (int j = 0; j < this.controller.numberOfNode(); j++) {

                if (this.controller.getMeet()[i][j].isHighlighted()) {
                    graphics2D.drawRect(this.drawX + this.controller.positionOfNode(i) * sizeOfEachCell
                            , this.drawY + this.controller.positionOfNode(j) * sizeOfEachCell
                            , sizeOfEachCell
                            , sizeOfEachCell);
                }
            }
        }
    }

    public void drawHighlightPeople(Graphics2D graphics2D) {
        graphics2D.setPaint(Constants.ROLLOVER_HIGHLIGHT_PEOPLE);
        if(Constants.matrixNumbers) {
            if (mouseX > this.drawX - this.sizeOfEachCell
                    && mouseX < this.drawX
                    && mouseY < this.drawY + (this.controller.numberOfNode()) * sizeOfEachCell) {
                for (int i = 0; i < this.controller.numberOfNode(); i++) {
                    if (mouseY > this.drawY + i * sizeOfEachCell
                            && mouseY < this.drawY + (i + 1) * sizeOfEachCell
                            )
                        //vertical rectangle highlight
                        graphics2D.fillRect(this.drawX - this.sizeOfEachCell
                                , this.drawY + i * sizeOfEachCell
                                , sizeOfEachCell
                                , sizeOfEachCell);
                }
            }
            if (mouseY > this.drawY + this.controller.numberOfNode() * sizeOfEachCell
                    && mouseY < this.drawY + (this.controller.numberOfNode() + 1) * sizeOfEachCell
                    && mouseX < this.drawX + this.controller.numberOfNode() * sizeOfEachCell)
                for (int i = 0; i < this.controller.numberOfNode(); i++) {

                    if (mouseX > this.drawX + i * sizeOfEachCell
                            && mouseX < this.drawX + (i + 1) * sizeOfEachCell
                            )
                        //vertical rectangle highlight
                        graphics2D.drawRect(this.drawX + i * sizeOfEachCell
                                , this.drawY + this.controller.numberOfNode() * sizeOfEachCell
                                , sizeOfEachCell
                                , sizeOfEachCell);
                }
        }
        else {

            graphics2D.setStroke(new BasicStroke(6.0f));

            if (mouseX > this.drawX - 2*this.sizeOfEachCell
                    && mouseX < this.drawX
                    && mouseY < this.drawY + (this.controller.numberOfNode()) * sizeOfEachCell) {
                for (int i = 0; i < this.controller.numberOfNode(); i++) {
                    if (mouseY > this.drawY + i * sizeOfEachCell
                            && mouseY < this.drawY + (i + 1) * sizeOfEachCell
                            )
                        //vertical rectangle highlight

                        graphics2D.drawRect(this.drawX - 2 * this.sizeOfEachCell + 6
                                , this.drawY + i * sizeOfEachCell + 6
                                , sizeOfEachCell * 2  - 12
                                , sizeOfEachCell - 12);
                }
            }
            if (mouseY > this.drawY + this.controller.numberOfNode() * sizeOfEachCell
                    && mouseY < this.drawY + (this.controller.numberOfNode() + 2) * sizeOfEachCell
                    && mouseX < this.drawX + this.controller.numberOfNode() * sizeOfEachCell)
                for (int i = 0; i < this.controller.numberOfNode(); i++) {

                    if (mouseX > this.drawX + i * sizeOfEachCell
                            && mouseX < this.drawX + (i + 1) * sizeOfEachCell
                            )
                        //vertical rectangle highlight
                        graphics2D.drawRect(this.drawX + i * sizeOfEachCell + 6
                                , this.drawY + this.controller.numberOfNode() * sizeOfEachCell + 6
                                , sizeOfEachCell - 12
                                , sizeOfEachCell * 2 - 12);
                }

        }
    }


    public void drawHighlightMeeting(Graphics2D graphics2D) {
        graphics2D.setPaint(ROLLOVER_HIGHLIGHT_MEETING);
        graphics2D.setStroke(new BasicStroke(10.0f));

        if(Constants.matrixNumbers) {
            //without rectangles
            if (mouseX > this.drawX && mouseX < this.drawX + this.controller.numberOfNode() * sizeOfEachCell &&
                    mouseY > this.drawY && mouseY < this.drawY + this.controller.numberOfNode() * sizeOfEachCell) {
                //vertical rectangle highlight
                graphics2D.fillRect(this.drawX + (mouseX - this.drawX) / sizeOfEachCell * sizeOfEachCell
                        , this.drawY
                        , sizeOfEachCell
                        , sizeOfEachCell * (this.controller.numberOfNode() + 1 + extraCell) + this.lengthOfLongestString);

                //horizontal rectangle highlight
                graphics2D.fillRect(this.drawX - (2 + extraCell) * this.sizeOfEachCell
                        , this.drawY + (mouseY - this.drawY) / sizeOfEachCell * sizeOfEachCell
                        , sizeOfEachCell * (this.controller.numberOfNode() + 2 + extraCell)
                        , sizeOfEachCell);
//            ((mouseY - Constants.PADDING_MATRIX) / sizeOfEachCell),);
            }
        }
        else {
                //vertical rectangle highlight

                for (int i = 0; i < this.controller.numberOfNode(); i++) {
                    for (int j = 0; j < this.controller.numberOfNode(); j++) {

                        if (mouseX > this.drawX + this.controller.positionOfNode(i)*sizeOfEachCell && mouseX < this.drawX + this.controller.positionOfNode(i)*sizeOfEachCell+sizeOfEachCell &&
                                mouseY > this.drawY + this.controller.positionOfNode(j)*sizeOfEachCell && mouseY < this.drawY + this.controller.positionOfNode(j)*sizeOfEachCell +sizeOfEachCell) {
                            graphics2D.drawRect(this.drawX + this.controller.positionOfNode(i) * sizeOfEachCell + (int) (sizeOfEachCell * 0.1)
                                    , this.drawY + this.controller.positionOfNode(j) * sizeOfEachCell + (int) (sizeOfEachCell * 0.1)
                                    , sizeOfEachCell - (int)(sizeOfEachCell*0.2)
                                    ,sizeOfEachCell -  (int)(sizeOfEachCell*0.2));
                        }
                    }
                }
//            ((mouseY - Constants.PADDING_MATRIX) / sizeOfEachCell),);


        }

        graphics2D.setStroke(new BasicStroke());
    }

    public void drawMatrix(Graphics2D graphics2D) {

        double oldRange =(this.controller.getMinMax().max-this.controller.getMinMax().min);
//        System.out.println(oldRange);
        double newRange = 255.0 - 0.0;

        //draw Rectangles to make the matrix
        for (int i = 0; i < this.controller.numberOfNode(); ++i) {
            for (int j = 0; j < this.controller.numberOfNode(); j++) {
                if (i == j) continue;
                graphics2D.setColor(Color.black);
                double scale= (this.controller.getNoOfMeetingsBetween(this.controller.nodeAtPosition(i),
                        this.controller.nodeAtPosition(j)) - this.controller.getMinMax().min)/oldRange;
                //place this scale in the new range
                double newValue=(newRange*scale)+0.0;
//                System.out.print(newValue+"   ");
//                   to write values inside each cell
//                graphics2D.drawString(Double.toString(newValue),
//                        Constants.PADDING_MATRIX + (i)*this.sizeOfEachCell+sizeOfEachCell/2 - (graphics2D.getFontMetrics().stringWidth(Integer.toString(this.group.getMeet()[i][j]))/2),
//                        Constants.PADDING_MATRIX + this.sizeOfEachCell/2 + (j)*this.sizeOfEachCell  + (graphics2D.getFontMetrics().getAscent())/2 );
//                System.out.print("  "+this.group.getNoOfMeetingsBetweenPeople(j, i) + "  " + newValue);
                graphics2D.setColor(Constants.BACKGROUND_COLOR);
                graphics2D.fillRect((int)(this.drawX + j * sizeOfEachCell + sizeOfEachCell*.10),
                        (int)(this.drawY + i * sizeOfEachCell + sizeOfEachCell*.10),
                        (int)(sizeOfEachCell - sizeOfEachCell* .20) + 2,
                        (int)(sizeOfEachCell - sizeOfEachCell* .20) +2);
                graphics2D.setColor(new Color(0,0,0,(int)newValue));
                graphics2D.fillRect((int)(this.drawX + j * sizeOfEachCell + sizeOfEachCell*.10),
                        (int)(this.drawY + i * sizeOfEachCell + sizeOfEachCell*.10),
                        (int)(sizeOfEachCell - sizeOfEachCell* .20)+2,
                        (int)(sizeOfEachCell - sizeOfEachCell* .20) +2);
            }

//            System.out.println();
        }

        graphics2D.setColor(Color.black);

        //draw matrix using lines
        //draw Rectangle around the matrix
//        for (int i = 0; i <= this.group.noOfPeopleInGroup(); ++i) {
//            graphics2D.drawLine(Constants.PADDING_MATRIX + i*this.sizeOfEachCell,
//                    Constants.PADDING_MATRIX,
//                    Constants.PADDING_MATRIX + i*this.sizeOfEachCell,
//                    Constants.PADDING_MATRIX + this.sizeOfEachCell*this.group.noOfPeopleInGroup());
//            graphics2D.drawLine(Constants.PADDING_MATRIX ,
//                    Constants.PADDING_MATRIX + i*this.sizeOfEachCell,
//                    Constants.PADDING_MATRIX + this.group.noOfPeopleInGroup()*this.sizeOfEachCell,
//                    Constants.PADDING_MATRIX + i*this.sizeOfEachCell);
//
//        }


    }

    void drawMatrixLabel(Graphics2D graphics2D) {
        Font fontLabel = new Font("SansSerif", Font.BOLD,(int) (this.sizeOfEachCell * .4));

        FontMetrics fontMetrics = graphics2D.getFontMetrics(fontLabel);
        graphics2D.setFont(fontLabel);

        //draw Labels around Matrix
        graphics2D.setColor(Color.black);
        graphics2D.setStroke(new BasicStroke(5.0f));
        for (int i = 0; i < this.controller.numberOfNode(); ++i) {
            int j = this.controller.nodeAtPosition(i);
            if( j >= Constants.PEOPLE_COLOR.length) j = j % Constants.PEOPLE_COLOR.length ;
            if(this.useOfColor)
            graphics2D.setColor(Constants.PEOPLE_COLOR[j]);


            String stringNum = Integer.toString(this.controller.nodeAtPosition(i)) ;

            // get the visual center of the component.
            int centerX = this.drawX - this.sizeOfEachCell + sizeOfEachCell / 2;
            int centerY = this.drawY + this.sizeOfEachCell / 2 + (i) * this.sizeOfEachCell;

            if(Constants.matrixNumbers) {
                drawAtVisualCenter(stringNum, centerX, centerY, graphics2D, Color.BLACK);
                graphics2D.drawRect(
                        (int) (this.drawX - this.sizeOfEachCell + sizeOfEachCell * .20),
                        (int) (this.drawY + i * sizeOfEachCell + sizeOfEachCell * .20),
                        (int) (sizeOfEachCell - sizeOfEachCell * .40),
                        (int) (sizeOfEachCell - sizeOfEachCell * .40)
                );
            }

            drawAtVisualCenter(Constants.PEOPLE_NAMES[this.controller.nodeAtPosition(i)]+" ", this.drawX - this.sizeOfEachCell*(1+extraCell) - this.sizeOfEachCell/2
                    + graphics2D.getFontMetrics().stringWidth(Constants.PEOPLE_NAMES[this.controller.nodeAtPosition(i)]+" ")/2, centerY , graphics2D, (this.useOfColor)?Constants.PEOPLE_COLOR[j]:Color.black);
//
            //vertical text


            stringNum = Integer.toString(this.controller.nodeAtPosition(i)) ;

            // get the visual center of the component.
            centerX = this.drawX + (i) * this.sizeOfEachCell + sizeOfEachCell / 2;
            centerY = this.drawY + this.sizeOfEachCell  + this.controller.numberOfNode() * this.sizeOfEachCell ;

            if(Constants.matrixNumbers) {
                graphics2D.drawRect(
                        (int)(this.drawX + i * sizeOfEachCell + sizeOfEachCell*.20)
                        ,(int)(this.drawY + this.controller.numberOfNode() * sizeOfEachCell + sizeOfEachCell*.20 )
                        , (int)(sizeOfEachCell - sizeOfEachCell* .40)
                        , (int)(sizeOfEachCell - sizeOfEachCell* .40)
                );

                drawAtVisualCenter(stringNum, centerX, centerY, graphics2D, Color.BLACK);
            }
            centerY += this.sizeOfEachCell*extraCell;
            String name = Constants.PEOPLE_NAMES[this.controller.nodeAtPosition(i)];

//            for (int k = 0; k < name.length(); k++) {
//                char c = name.charAt(k);
//                drawAtVisualCenter(Character.toString(c), centerX, centerY, graphics2D, Constants.PEOPLE_COLOR[j]);
//                centerY += graphics2D.getFontMetrics().getHeight()/2+2;
//            }

            Rectangle rectangle = DrawUtils.getPixelBounds(name,graphics2D,centerX,centerY);

            label_line(graphics2D, centerX  + rectangle.height/2 , centerY + rectangle.width/2 , -90 * java.lang.Math.PI / 180, name, (this.useOfColor) ? Constants.PEOPLE_COLOR[j] : Color.black);
//              drawAtVisualCenter(name, centerX, centerY, graphics2D, Color.BLACK);


//            graphics2D.drawString(Integer.toString(this.controller.nodeAtPosition(i)),
//                    Constants.PADDING_MATRIX + (i) * this.sizeOfEachCell + sizeOfEachCell / 2 - (graphics2D.getFontMetrics().stringWidth(Integer.toString(this.controller.nodeAtPosition(i))) / 2),
//                    Constants.PADDING_MATRIX + this.sizeOfEachCell / 2 + this.controller.numberOfNode() * this.sizeOfEachCell + (graphics2D.getFontMetrics().getAscent()) / 2);
//     graphics2D.drawString(Integer.toString(i),(int)rectangle.getMinX() ,(int)rectangle.getMinY() + i*sizeOfEachCell);
        }

//        graphics2D.setFont(this.font);

    }

    void label_line(Graphics2D graphics2D, int x, int y, double theta, String label,Color color) {

        // Create a rotation transformation for the font.
        AffineTransform fontAT = new AffineTransform();
        // get the current font
        Font theFont = graphics2D.getFont();
        // Derive a new font using a rotation transform
        fontAT.rotate(theta);
        Font theDerivedFont = theFont.deriveFont(fontAT);
        // set the derived font in the Graphics2D context
        graphics2D.setFont(theDerivedFont);
        // Render a string using the derived font
        graphics2D.drawString(label, x, y);

//        drawAtVisualCenter(label,(int)x,(int)y,graphics2D,color);
        // put the original font back
        graphics2D.setFont(theFont);
        fontAT.rotate(-theta);

    }

    void drawAtVisualCenter(String stringNum, int centerX, int centerY, Graphics2D graphics2D, Color color) {
        Color old = graphics2D.getColor();
        graphics2D.setColor(color);

// get the bounds of the string to draw.
        FontMetrics fontMetrics = graphics2D.getFontMetrics();

        Rectangle stringBounds = fontMetrics.getStringBounds(stringNum, graphics2D).getBounds();

// get the visual bounds of the text using a GlyphVector.
        Font font = graphics2D.getFont();
        FontRenderContext renderContext = graphics2D.getFontRenderContext();
        GlyphVector glyphVector = font.createGlyphVector(renderContext, stringNum);

        Rectangle visualBounds = glyphVector.getVisualBounds().getBounds();

// calculate the lower left point at which to draw the string. note that this we
// give the graphics context the y corridinate at which we want the baseline to
// be placed. use the visual bounds height to center on in conjuction with the
// position returned in the visual bounds. the vertical position given back in the
// visualBounds is a negative offset from the basline of the text.
        int textX = centerX - stringBounds.width / 2;
        int textY = centerY - visualBounds.height / 2 - visualBounds.y;

        graphics2D.drawString(stringNum, textX, textY);
        graphics2D.setColor(old);
    }


    void fillValues(Graphics2D graphics2D) {
      Font fontLabel = new Font("SansSerif", Font.BOLD,(int) (this.sizeOfEachCell * .4));

        FontMetrics fontMetrics = graphics2D.getFontMetrics(fontLabel);
        graphics2D.setFont(fontLabel);
        graphics2D.setColor(Color.WHITE);
        for (int i = 0; i < this.controller.numberOfNode(); i++) {
            for (int j = 0; j < this.controller.numberOfNode(); j++) {
                if(i==j)continue;
                drawAtVisualCenter(Integer.toString(this.controller.getMeet()[this.controller.nodeAtPosition(j)][this.controller.nodeAtPosition(i)].getNumberOfMeetings()),
                        this.drawX + (i) * this.sizeOfEachCell + sizeOfEachCell / 2,
                        this.drawY + this.sizeOfEachCell / 2 + (j) * this.sizeOfEachCell ,graphics2D,Color.WHITE);
//                graphics2D.drawString(Integer.toString(this.controller.getMeet()[this.controller.nodeAtPosition(j)][this.controller.nodeAtPosition(i)].getNumberOfMeetings()),
//                        Constants.PADDING_MATRIX + (i) * this.sizeOfEachCell + sizeOfEachCell / 2 - (graphics2D.getFontMetrics().stringWidth(Integer.toString(this.controller.getMeet()[i][j].getNumberOfMeetings())) / 2),
//                        Constants.PADDING_MATRIX + this.sizeOfEachCell / 2 + (j) * this.sizeOfEachCell + (graphics2D.getFontMetrics().getAscent()) / 2);
            }
        }
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        mouseX = e.getX();
        mouseY = e.getY();
        repaint();
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }
   
    public void mousePressed(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
        if(e.getButton() == MouseEvent.BUTTON3)  {
            this.controller.reorderMatrix(1);
            this.rightClick++;
        }
        else if (e.getButton() == MouseEvent.BUTTON1) {
            this.leftClick++;
            if (mouseX > this.drawX && mouseX < this.drawX + this.controller.numberOfNode() * sizeOfEachCell
                    && mouseY < this.drawY + this.controller.numberOfNode() * sizeOfEachCell)
                this.controller.setAndRemoveHighlight(this.controller.nodeAtPosition((mouseX - this.drawX) / sizeOfEachCell),
                        this.controller.nodeAtPosition(((mouseY - this.drawY) / sizeOfEachCell)));


            if(Constants.matrixNumbers) {
                if (mouseX > this.drawX - this.sizeOfEachCell
                        && mouseX < this.drawX
                        && mouseY < this.drawY + (this.controller.numberOfNode()) * sizeOfEachCell) {
                    for (int i = 0; i < this.controller.numberOfNode(); i++) {

                        if (mouseY > this.drawY + i * sizeOfEachCell
                                && mouseY < this.drawY + (i + 1) * sizeOfEachCell
                                )
                            this.controller.setAndRemoveHighlightedPerson(this.controller.nodeAtPosition(i));
                    }

                }

                if(mouseY > this.drawY + this.controller.numberOfNode() * sizeOfEachCell
                        && mouseY < this.drawY + (this.controller.numberOfNode() +1) * sizeOfEachCell
                        && mouseX < this.drawX + this.controller.numberOfNode() * sizeOfEachCell)
                    for (int i =0 ; i < this.controller.numberOfNode() ; i++){
                        if(mouseX > this.drawX + i * sizeOfEachCell
                                && mouseX < this.drawX + (i +1) * sizeOfEachCell
                                )
                            this.controller.setAndRemoveHighlightedPerson(this.controller.nodeAtPosition(i));
                    }
            }
            else {
                if (mouseX > this.drawX - 2*this.sizeOfEachCell
                        && mouseX < this.drawX
                        && mouseY < this.drawY + (this.controller.numberOfNode()) * sizeOfEachCell) {
                    for (int i = 0; i < this.controller.numberOfNode(); i++) {

                        if (mouseY > this.drawY + i * sizeOfEachCell
                                && mouseY < this.drawY + (i + 1) * sizeOfEachCell
                                )
                            this.controller.setAndRemoveHighlightedPerson(this.controller.nodeAtPosition(i));
                    }

                }

                if(mouseY > this.drawY + this.controller.numberOfNode() * sizeOfEachCell
                        && mouseY < this.drawY + (this.controller.numberOfNode() +2) * sizeOfEachCell
                        && mouseX < this.drawX + this.controller.numberOfNode() * sizeOfEachCell)
                    for (int i =0 ; i < this.controller.numberOfNode() ; i++){
                        if(mouseX > this.drawX + i * sizeOfEachCell
                                && mouseX < this.drawX + (i +1) * sizeOfEachCell
                                )
                            this.controller.setAndRemoveHighlightedPerson(this.controller.nodeAtPosition(i));
                    }

            }


        }

        else if (e.getButton() == MouseEvent.BUTTON2) {

            AppEvent ae = new AppEvent(AppEvent.NAV_EVENT);
            this.controller.handleAppEvent(ae);
        }
        repaint();
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
        this.startTime = System.nanoTime();
        this.rightClick =0;
        this.leftClick = 0;
        this.requestFocus();
    }

    @Override
    public void mouseExited(MouseEvent e) {
        this.controller.setTimeInside(TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - this.startTime));
        this.controller.setClickRightCount(this.rightClick);
        this.controller.setClickLeftCount(this.leftClick);
        mouseX = -1;
        mouseY = -1;
        this.invalidate();
        this.repaint();
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setController(MatrixController controller) {
        this.controller = controller;
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {

        if (e.getKeyChar() == 'c') {
            this.controller.deselectAll();
            this.repaint();
        }
        if(e.getKeyChar() == 'r') {

            this.typeOfHighlight++;

            if(this.typeOfHighlight >3) {
                this.typeOfHighlight = 0;
            }
        }
        if(e.getKeyChar() == 'b') {
            this.useOfColor = !this.useOfColor;
            this.repaint();
        }

    }
}
