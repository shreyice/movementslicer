package View;

import javax.swing.*;
import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: Shrey
 * Date: 10/11/13
 * Time: 4:30 AM
 * To change this template use File | Settings | File Templates.
 */
public class FileChooserView {

    File[] dirs;


    public FileChooserView(JFrame jFrame) {

        //Create a file chooser
        final JFileChooser fc = new JFileChooser();
        fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fc.setMultiSelectionEnabled(true);
        //In response to a button click:
        fc.setCurrentDirectory(new File("input"));
        int returnVal = fc.showOpenDialog(jFrame);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            this.dirs = fc.getSelectedFiles();
        }

    }

    public File[] getDirs() {
        return dirs;
    }
}
