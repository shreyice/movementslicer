package View;

import Controller.MainController;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

/**
 * Created with IntelliJ IDEA.
 * User: Shrey
 * Date: 10/10/13
 * Time: 1:28 PM
 * To change this template use File | Settings | File Templates.
 */
public class MainView {

    JFrame jFrame;
    JMenuBar menuBar;
    JMenu menu;
    JMenuItem menuItem;
    ActionListener actionListener;

    MainController mainController;

    public MainView() {

        jFrame = new JFrame();
        jFrame.setVisible(true);
        jFrame.setSize(new Dimension(800, 800));
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

//Create the menu bar.
        menuBar = new JMenuBar();

//Build the first menu.
        menu = new JMenu("File");
        menu.setMnemonic(KeyEvent.VK_A);
        menu.getAccessibleContext().setAccessibleDescription(
                "The only menu in this program that has menu items");
        menuBar.add(menu);
//a group of JMenuItems
        menuItem = new JMenuItem("Open",
                KeyEvent.VK_T);
        menuItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_O, ActionEvent.CTRL_MASK));
        menuItem.getAccessibleContext().setAccessibleDescription(
                "This doesn't really do anything");
        this.actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                openFileChooser();
            }
        };
        menuItem.addActionListener(actionListener);
        menu.add(menuItem);
        jFrame.setJMenuBar(menuBar);
        jFrame.setLayout(new MigLayout("fill"));
    }

    void openFileChooser() {
        this.mainController.openFileChooser();
    }

    public void setController(MainController mainController) {
        this.mainController = mainController;

    }

    public JFrame getjFrame() {
        return jFrame;
    }

    public void setjFrame(JFrame jFrame) {
        this.jFrame = jFrame;
    }

    public void addjPanel(JPanel jPanel) {
        this.jFrame.add(jPanel);
        this.jFrame.revalidate();

    }

    public void setLayoutManager(LayoutManager layoutManager) {
        this.jFrame.setLayout(layoutManager);
    }
}
