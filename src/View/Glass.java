package View;

import Algorithms.Constants;
import javafx.scene.shape.Circle;
import sample6.Map;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Line2D;

/**
 * Created with IntelliJ IDEA.
 * User: Shrey
 * Date: 11/19/13
 * Time: 10:30 AM
 * To change this template use File | Settings | File Templates.
 */
public class Glass extends JComponent {

    double startX =-1;
    double startY =-1;
    double endX =-1;
    double endY =-1;

    @Override
    public void paintComponent(Graphics graphics) {
//        if (this.startX > 0) {                                                     o
//
//            graphics.setColor(Color.BLACK);
//            graphics.drawLine((int) this.startX, (int) this.startY, (int) this.endX, (int) this.endY);
//        }
        Graphics2D graphics2D = (Graphics2D)graphics;
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        graphics2D.setStroke(new BasicStroke(4.0F, BasicStroke.CAP_ROUND,
                BasicStroke.JOIN_BEVEL));

        double dx = startX - endX;
        double dy = startY - endY;
        double length = Math.sqrt(dx * dx + dy * dy);
        dx /= length;
        dy /= length;
        dx *= Constants.MAP_LOCATION_RADIUS+1;
        dy *= Constants.MAP_LOCATION_RADIUS+1;
        int finalX = (int)(startX - dx);
        int finalY = (int)(startY - dy);


        graphics2D.drawLine(finalX, finalY, (int) this.endX, (int) this.endY);

    }

    public void setGlass(double startX, double startY , double endX, double endY) {
        this.startX =startX;
        this.endX = endX;
        this.startY= startY;
        this.endY = endY;
    }



}
