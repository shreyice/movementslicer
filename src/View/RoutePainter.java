
package View;

import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;
import java.awt.geom.Point2D;
import java.util.*;
import java.util.List;

import Algorithms.*;
import org.jdesktop.swingx.JXMapViewer;
import org.jdesktop.swingx.mapviewer.GeoPosition;
import org.jdesktop.swingx.painter.Painter;

/**
 * Paints a route
 * @author Martin Steiger
 * @Modified Shrey Gupta
 */
public class RoutePainter implements Painter<JXMapViewer>
{
//	private Color color = new Color(201, 30, 54,90);
	private boolean antiAlias = true;
	
	private HashMap<Integer,ArrayList<GeoPosition>> meetTrack;
	private HashMap<Integer,ArrayList<LatitudeAndLongitude>> meetTrackLNL;

	/**
	 * @param meetTrack the track
	 */
	public RoutePainter(HashMap<Integer,ArrayList<GeoPosition>> meetTrack,HashMap<Integer,ArrayList<LatitudeAndLongitude>> meetTrackLNL)
	{
		// copy the list so that changes in the 
		// original list do not have an effect here
		this.meetTrack = meetTrack;
        this.meetTrackLNL = meetTrackLNL;
    }

	@Override
	public void paint(Graphics2D graphics2D, JXMapViewer map, int w, int h)
	{

		// convert from viewport to world bitmap
		Rectangle rect = map.getViewportBounds();
		graphics2D.translate(-rect.x, -rect.y);
        graphics2D.setFont( new Font("Tamoha", Font.BOLD, Constants.MAP_TEXT));

		if (antiAlias)
			graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		// do the drawing
		drawRoute(graphics2D, map);
		graphics2D.dispose();
	}

    void drawAtVisualCenter(String stringNum, int centerX, int centerY, Graphics2D graphics2D, Color color) {
        Color old = graphics2D.getColor();
        graphics2D.setColor(color);

// get the bounds of the string to draw.
        FontMetrics fontMetrics = graphics2D.getFontMetrics();

        Rectangle stringBounds = fontMetrics.getStringBounds(stringNum, graphics2D).getBounds();

// get the visual bounds of the text using a GlyphVector.
        Font font = graphics2D.getFont();
        FontRenderContext renderContext = graphics2D.getFontRenderContext();
        GlyphVector glyphVector = font.createGlyphVector(renderContext, stringNum);
        Rectangle visualBounds = glyphVector.getVisualBounds().getBounds();

// calculate the lower left point at which to draw the string. note that this we
// give the graphics context the y corridinate at which we want the baseline to
// be placed. use the visual bounds height to center on in conjuction with the
// position returned in the visual bounds. the vertical position given back in the
// visualBounds is a negative offset from the basline of the text.
        int textX = centerX - stringBounds.width / 2;
        int textY = centerY - visualBounds.height / 2 - visualBounds.y;

        graphics2D.drawString(stringNum, textX, textY);
        graphics2D.setColor(old);
    }

	/**
	 * @param graphics2D the graphics object
	 * @param map the map
	 */
//	private void drawRoute(Graphics2D graphics2D, JXMapViewer map) {
//
//        ArrayList<Polygon> polygonList = new ArrayList<>();
//        ArrayList<Point> list;
//        double max =0;
//        for (int i  : meetTrack.keySet()) {
//            ArrayList<GeoPosition> gpl = this.meetTrack.get(i);
//            if (max < gpl.size()) {
//                max = gpl.size();
//            }
//            if (gpl.size() >= 2) {
//                Polygon polygon = new Polygon();
//                list = new ArrayList<>();
//                for (GeoPosition gp : gpl) {
//                    // convert geo-coordinate to world bitmap pixel
//                    Point2D pt = map.getTileFactory().geoToPixel(gp, map.getZoom());
//                    list.add(new Point((int) pt.getX(), (int) pt.getY()));
//                    graphics2D.setPaint(Constants.MAP_POINTS_COLOR);
//                    graphics2D.fillOval((int) pt.getX(), (int) pt.getY(), 4, 4);
//
////                polygon.addPoint((int) pt.getX(), (int) pt.getY());
//                }
//
//                ConvexHullAlgorithm convexHullAlgorithm = new FastConvexHull();
//                // find the convex hull
//                List<Point> convexHull = convexHullAlgorithm.execute(list);
//
//                for (Point p : convexHull) {
//                    polygon.addPoint((int) p.getX(), (int) p.getY());
//
//                }
//                polygonList.add(polygon);
//                graphics2D.setPaint(new Color(202, 41, 66, 187));
//                Font font = new Font("Tamoha", Font.BOLD, (int) Constants.TEXT_SIZE );
//                graphics2D.setFont(font);
//                graphics2D.fill(polygon);
//                graphics2D.setPaint(Constants.MAP_POINTS_COLOR);
////                graphics2D.fillRect((int)(polygon.getBounds().getCenterX() - graphics2D.getFontMetrics().stringWidth( "L"+Integer.toString(i))/2) - 2
////                        ,(int)(polygon.getBounds().getCenterY() - graphics2D.getFontMetrics().stringWidth("H")/2) -2 ,graphics2D.getFontMetrics().stringWidth("L"+Integer.toString(i)) +4
////                        ,graphics2D.getFontMetrics().stringWidth("H")+4);
//
//                graphics2D.fillOval((int)(polygon.getBounds().getCenterX() - Constants.MAP_LOCATION_RADIUS)
//                        , (int)(polygon.getBounds().getCenterY() - Constants.MAP_LOCATION_RADIUS)
//                        , 2 * Constants.MAP_LOCATION_RADIUS
//                        , 2 * Constants.MAP_LOCATION_RADIUS);
//                graphics2D.setPaint(Color.black);
//                drawAtVisualCenter("L"+Integer.toString(i),(int)polygon.getBounds().getCenterX(),(int)polygon.getBounds().getCenterY(),graphics2D,Color.BLACK);
//
//            }
//            else {
//                polygonList.add(null);
//
//                for (GeoPosition gp : gpl) {
//                    // convert geo-coordinate to world bitmap pixel
//                    Point2D pt = map.getTileFactory().geoToPixel(gp, map.getZoom());
////                polygon.addPoint((int) pt.getX(), (int) pt.getY());
//
//                    graphics2D.setPaint(Constants.MAP_POINTS_COLOR);
//                    graphics2D.fillOval((int) pt.getX() - Constants.MAP_LOCATION_RADIUS, (int) pt.getY() - Constants.MAP_LOCATION_RADIUS, 2 * Constants.MAP_LOCATION_RADIUS, 2 * Constants.MAP_LOCATION_RADIUS);
////                    graphics2D.fillRect((int)pt.getX() - Constants.MAP_LOCATION_RADIUS ,(int) pt.getY() - Constants.MAP_LOCATION_RADIUS, Constants.MAP_LOCATION_RADIUS, Constants.MAP_LOCATION_RADIUS);
//                    graphics2D.setPaint(Color.BLACK);
//                    FontMetrics metrics = graphics2D.getFontMetrics();
//                    drawAtVisualCenter("L"+Integer.toString(i), (int)pt.getX() , (int)pt.getY(),graphics2D,Color.BLACK);
//
//                }
//            }
//
//        }
//
//    }




     //latitude and longitude
    private void drawRoute(Graphics2D graphics2D, JXMapViewer map) {

        ArrayList<LatitudeAndLongitude> list;
        double max =0;
        for (int i  : meetTrackLNL.keySet()) {
            ArrayList<LatitudeAndLongitude> gpl = this.meetTrackLNL.get(i);
            if (max < gpl.size()) {
                max = gpl.size();
            }
            if (gpl.size() >= 2) {

                LatitudeAndLongitude mean = LatitudeAndLongitudeUtils.meanFromLatitudeAndLongitude(gpl);

                GeoPosition gp = new GeoPosition(mean.getLatitude(), mean.getLongitude());
                Point2D pt = map.getTileFactory().geoToPixel(gp, map.getZoom());

                graphics2D.setPaint(new Color(202, 41, 66, 187));
                Font font = new Font("Tamoha", Font.BOLD, (int) Constants.TEXT_SIZE );
                graphics2D.setFont(font);
                graphics2D.setPaint(Constants.MAP_POINTS_COLOR);


                graphics2D.fillOval((int)(pt.getX() - Constants.MAP_LOCATION_RADIUS)
                        , (int)(pt.getY() - Constants.MAP_LOCATION_RADIUS)
                        , 2 * Constants.MAP_LOCATION_RADIUS
                        , 2 * Constants.MAP_LOCATION_RADIUS);
                graphics2D.setPaint(Color.black);
                drawAtVisualCenter("L"+Integer.toString(i),(int)pt.getX(),(int)pt.getY(),graphics2D,Color.BLACK);

            }
            else {

                for (LatitudeAndLongitude gp : gpl) {

                    GeoPosition geo = new GeoPosition(gp.getLatitude(), gp.getLongitude());
                    // convert geo-coordinate to world bitmap pixel
                    Point2D pt = map.getTileFactory().geoToPixel(geo, map.getZoom());
//                polygon.addPoint((int) pt.getX(), (int) pt.getY());

                    graphics2D.setPaint(Constants.MAP_POINTS_COLOR);
                    graphics2D.fillOval((int) pt.getX() - Constants.MAP_LOCATION_RADIUS, (int) pt.getY() - Constants.MAP_LOCATION_RADIUS, 2 * Constants.MAP_LOCATION_RADIUS, 2 * Constants.MAP_LOCATION_RADIUS);
//                    graphics2D.fillRect((int)pt.getX() - Constants.MAP_LOCATION_RADIUS ,(int) pt.getY() - Constants.MAP_LOCATION_RADIUS, Constants.MAP_LOCATION_RADIUS, Constants.MAP_LOCATION_RADIUS);
                    graphics2D.setPaint(Color.BLACK);
                    FontMetrics metrics = graphics2D.getFontMetrics();
                    drawAtVisualCenter("L" + Integer.toString(i), (int) pt.getX(), (int) pt.getY(), graphics2D, Color.BLACK);

                }
            }

        }

    }
}

