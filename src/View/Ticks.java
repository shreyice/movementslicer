package View;
import Algorithms.Constants;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.MutableDateTime;
import java.awt.*;
import java.awt.geom.AffineTransform;

/**
 * Created by Shrey on 2/25/14.
 */
public class Ticks {


    String interval[] ={
            "15 min"
            ,"30 min"
            ,"1 hour"
            ,"6 hour"
            ,"12 hour"
            ,"1 day"
            ,"2 day"
            ,"1 week"
            ,"2 week"
            ,"1 month"
            ,"2 month"
            ,"3 month"
            ,"6 month"
            ,"1 year"
            ,"2 year"
            ,"5 year"
    };

    String pattern[] = {
            "H:mm"
            ,"H:mm"
            ,"H:00"
            ,"EEE"
            ,"EEE"
            ,"EEE dd"
            ,"MMM dd"
            ,"MMM dd"
            ,"MMM dd"
            ,"MMM"
            ,"MMM"
            ,"MMM yyyy"
            ,"MMM yyyy"
            ,"yyyy"
            ,"yyyy"
            ,"yyyy"
    };

    int counter[] = {0,0};


    boolean check = true;

    GroupGanttView ganttView;

    public Ticks(GroupGanttView ganttView) {
        this.ganttView = ganttView;
    }

    MutableDateTime roundDate(MutableDateTime dt, String format, String interval) {
        if (format.equals("yyyy")) {
            dt.setMillisOfSecond(0);
            dt.setSecondOfMinute(0);
            dt.setMinuteOfHour(0);
            dt.setHourOfDay(0);
            dt.setDayOfMonth(1);
            dt.setMonthOfYear(1);
            return dt;
        }
        else if (format.equals("MMM yyyy")) {
            dt.setMillisOfSecond(0);
            dt.setSecondOfMinute(0);
            dt.setMinuteOfHour(0);
            dt.setHourOfDay(0);
            dt.setDayOfMonth(1);
            dt.setMonthOfYear(1);
            return dt;
        }
        else if (format.equals("MMM")) {
            dt.setMillisOfSecond(0);
            dt.setSecondOfMinute(0);
            dt.setMinuteOfHour(0);
            dt.setHourOfDay(0);
            dt.setDayOfMonth(1);
            return dt;
        }
        else if (format.equals("MMM dd")) {
            dt.setDayOfWeek(1);
            dt.setHourOfDay(0);
            dt.setMinuteOfHour(0);
            dt.setSecondOfMinute(0);
            dt.setMillisOfSecond(0);
            return dt;
        }
        else if (format.equals("HH:mm EEE")) {
            dt.setMinuteOfHour(0);
            dt.setSecondOfMinute(0);
            dt.setMillisOfSecond(0);
            return dt;
        }
        else if(format.equals("HH:mm")){

            dt.setSecondOfMinute(0);
            dt.setMillisOfSecond(0);
            return dt;
        }
        else if(format.equals("HH:mm:ss")){
            dt.setMillisOfSecond(0);
            return dt;
        }
        else if(format.equals("H:mm") && interval.equals("30 min")) {

            if(counter[0] == 0){
                dt.setMillisOfSecond(0);
                dt.setSecondOfMinute(0);
                dt.setMinuteOfHour(0);
                counter[0]++;
            }
            else {
                dt.setMillisOfSecond(0);
                dt.setSecondOfMinute(0);
                dt.setMinuteOfHour(30);
                counter[0]--;
            }

            return dt;
        }
        else if(format.equals("H:mm") && interval.equals("15 min") ){

            if(counter[1] == 0){
                dt.setMillisOfSecond(0);
                dt.setSecondOfMinute(0);
                dt.setMinuteOfHour(0);
                counter[1]++;
            }
            else if(counter[1] == 1) {
                dt.setMillisOfSecond(0);
                dt.setSecondOfMinute(0);
                dt.setMinuteOfHour(15);
                counter[1]++;
            }
            else if(counter[1] == 2) {
                dt.setMillisOfSecond(0);
                dt.setSecondOfMinute(0);
                dt.setMinuteOfHour(30);
                counter[1]++;
            }
            else if(counter[1] == 3) {
                dt.setMillisOfSecond(0);
                dt.setSecondOfMinute(0);
                dt.setMinuteOfHour(45);
                counter[1]++;
            }
            if(counter[1] == 4) counter[1] = 0;
            return dt;
        }
        else if(format.equals("H:mm")){
            dt.setMillisOfSecond(0);
            dt.setSecondOfMinute(0);
            return dt;
        }
        else if(format.equals("EEE")){
            dt.setHourOfDay(0);
            dt.setMinuteOfHour(0);
            dt.setMillisOfSecond(0);
            return dt;
        }
        else if(format.equals("H:00")){
            dt.setMinuteOfHour(0);
            dt.setSecondOfMinute(0);
            dt.setMillisOfSecond(0);
            return dt;
        }
        else if(format.equals("EEE dd")){
            dt.setHourOfDay(0);
            dt.setMinuteOfHour(0);
            dt.setSecondOfMinute(0);
            dt.setMillisOfSecond(0);
            return dt;
        }
        else if(format.equals("EEE")) {
            dt.setHourOfDay(0);
            dt.setMinuteOfHour(0);
            dt.setSecondOfMinute(0);
            dt.setMillisOfSecond(0);
            return dt;
        }
        return dt;
    }

    double stringToDays(String format) {
        if (format.equals("15 min")) {
            return 1.0/(4.0*24.0);
        }
        else if (format.equals("30 min")) {
            return 1.0/(2.0*24.0);
        }
        else if (format.equals("1 hour")) {
            return 1.0/24.0;
        }
        else if (format.equals("6 hour")) {
            return 6.0/24.0;
        }
        else if (format.equals("12 hour")) {
            return 12.0/24.0;
        }
        else if (format.equals("1 day")) {
            return 1.0;
        }
        else if(format.equals("2 day")){
            return 2.0;
        }
        else if(format.equals("1 week")){
            return 7.0;
        }
        else if(format.equals("2 week")){
            return 2.0*7.0;
        }
        else if(format.equals("1 month")){
            return 30.0;
        }
        else if(format.equals("2 month")){
            return 60.0;
        }
        else if(format.equals("3 month")){
            return 90.0;
        }
        else if(format.equals("6 month")){
            return 180.0;
        }
        else if(format.equals("1 year")){
            return 365.0;
        }
        else if(format.equals("2 year")){
            return 365.0*2.0;
        }
        else {
            return 365.0*5.0;
        }
    }


    /*
    *Algorithm for visually centering the text is taken from the http://explodingpixels.wordpress.com/2009/01/29/drawing-text-about-its-visual-center/
    */
    void  drawTicks(double startMillis, double endMillis, int start, int end ,Graphics2D graphics2D) {

        Font font = new Font("Tamoha", Font.BOLD, (int) Constants.TEXT_SIZE - 6);
        graphics2D.setFont(font);
        int size = end - start;
//        DateTime startDate = new DateTime((long)startMillis);
//        DateTime endDate = new DateTime((long) endMillis);

        double millis = (endMillis - startMillis);
        String datePattern = "HH:mm:ss";
        int datePer = graphics2D.getFontMetrics().stringWidth(datePattern);
        int num = size / datePer;

        int i = 0;
        for (i = 0; i < this.interval.length; i++) {
            double value1 =  (millis) / (1000.0 * 60.0 * 60.0 * 24.0);
            double value2 =   stringToDays(this.interval[i]);
//            System.out.println("in the interval phase");
            if (((millis) / (1000.0 * 60.0 * 60.0 * 24.0) < stringToDays(this.interval[i]))) {
                if (Constants.VERBOSE)
                System.out.println("break 1");
                break;
            }
        }
//        System.out.println("in the interval out of it");
        i--;
        if (i < 0) {
            i =0;
        }

        double millisToAdd = 1.0;
        double numOfLabels = millis / millisToAdd;
        for (int j = i - 1; j >= 0; j--) {
            datePattern = this.pattern[i];
            datePer = graphics2D.getFontMetrics().stringWidth(datePattern);
            //number of labels in terms of pixels you can accomadate
            num = size / (datePer * 2);
//            System.out.println("in the pixel phase");
            millisToAdd = stringToDays(this.interval[i]) * 1000.0 * 24.0 * 60.0 * 60.0;
            // no of labels in terms of differnce in time
            numOfLabels = millis / millisToAdd;
            if (numOfLabels < num) {
                i = j;
            } else {
                if (Constants.VERBOSE)
                System.out.println("break 2");
                break;

            }
        }
//        System.out.println("in the pixel out of it");
//        System.out.println("Millis  "+millisToAdd);

        millisToAdd = stringToDays(this.interval[i]) * 1000.0 * 24.0 * 60.0 * 60.0;
        while (numOfLabels * datePer > size) {
            if (Constants.VERBOSE)
                System.out.println("break 2.1");
            millisToAdd += millisToAdd;
            numOfLabels = millis / millisToAdd;
            if(numOfLabels < 1)break;
        }

        double currentY = ganttView.getVisibleRect().getY() + ganttView.getVisibleRect().getHeight() - Constants.PADDING_VERTICAL;


        startMillis = startMillis - 100*stringToDays(this.interval[i]);
        endMillis = endMillis + 100*stringToDays(this.interval[i]);

        while (startMillis <= endMillis) {
            if (Constants.VERBOSE)
            System.out.println("break 2.2");
//            System.out.println("date start " + new DateTime((long)startMillis));
//            System.out.println("startmillis " + startMillis);
            MutableDateTime dt = new MutableDateTime((long) startMillis);
//            System.out.println("mutable before " + dt.getMillis());
            dt = roundDate(dt, datePattern, this.interval[i]);
//            System.out.println("mutable after " + dt.getMillis());
            DateTime date = new DateTime(dt.getMillis());
//            System.out.println("miilis  " + dt.getMillis());
//            System.out.println("date  " + dt);
            int pixel = (int) ganttView.pixelAtMillis(date.getMillis());
//            System.out.println("PIXEL"+ pixel);
            if( pixel < end + 100 && pixel > start - 100 ){
                graphics2D.drawLine(pixel
                        , (int) currentY
                        , pixel
                        , (int) currentY + 2);

                String stringDate = date.withZone(
                        DateTimeZone.forID(Constants.TIMEZONE_TO_DISPLAY)).toString(datePattern);

                // get the visual center of the component.
                int centerX = pixel;
                int centerY = (int)currentY + 15;

// get the bounds of the string to draw.
                FontMetrics fontMetrics = graphics2D.getFontMetrics();


//                graphics2D.drawString(stringDate, textX, textY);
                label_line(graphics2D,centerX - fontMetrics.stringWidth("H")/2,centerY,45 * java.lang.Math.PI / 180,stringDate);
//
//            graphics2D.drawString(date.withZone(
//                    DateTimeZone.forID(Constants.TIMEZONE_TO_DISPLAY)).toString(datePattern)
//                    , pixel - graphics2D.getFontMetrics().stringWidth(datePattern) / 2, (int) currentY + graphics2D.getFontMetrics().getMaxAscent());
            }
            startMillis = startMillis + millisToAdd;
            if (startMillis == endMillis) {
                if (Constants.VERBOSE)
                System.out.println("break 3");

                break;
            }
//            System.out.println("in progrss printing " + startMillis+"endmillis  " + endMillis + "   add" + millisToAdd);
        }
//        System.out.println("done");
    }


    private void label_line(Graphics g, double x, double y, double theta, String label) {

        Font font = new Font("Tamoha", Font.BOLD, (int) Constants.TEXT_SIZE - 6);
        g.setFont(font);
        Graphics2D g2D = (Graphics2D) g;
        // Create a rotation transformation for the font.
        AffineTransform fontAT = new AffineTransform();
        // get the current font
        Font theFont = g2D.getFont();
        // Derive a new font using a rotation transform
        fontAT.rotate(theta);
        Font theDerivedFont = theFont.deriveFont(fontAT);
        // set the derived font in the Graphics2D context
        g2D.setFont(theDerivedFont);
        // Render a string using the derived font
        g2D.drawString(label, (int) x, (int) y);
        // put the original font back
        g2D.setFont(theFont);
        fontAT.rotate(-theta);

    }
}
