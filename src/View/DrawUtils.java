package View;

import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;

/**
 * Created by Shrey on 3/12/2015.
 */
public class DrawUtils {






    public static void drawAtVisualCenter(String stringNum, int centerX, int centerY, Graphics2D graphics2D, Color textColor
    ,boolean border, Color borderColor, float thickness, boolean rectangluarBox, Color rectangleColor ) {
        Color old = graphics2D.getColor();

        Rectangle stringBounds = getStringBounds(stringNum, graphics2D);
        Rectangle visualBounds = getVisualBounds(stringNum, graphics2D);


// calculate the lower left point at which to draw the string. note that this we
// give the graphics context the y corridinate at which we want the baseline to
// be placed. use the visual bounds height to center on in conjuction with the
// position returned in the visual bounds. the vertical position given back in the
// visualBounds is a negative offset from the basline of the text.
        int textX = centerX - stringBounds.width / 2;
        int textY = centerY - visualBounds.height / 2 - visualBounds.y;

        Rectangle shape =  getPixelBounds(stringNum,graphics2D,textX,textY);

        if(rectangluarBox) {
            graphics2D.setColor(rectangleColor);
//            graphics2D.fillRect(textX, textY - visualBounds.height, stringBounds.width, stringBounds.height);
//            graphics2D.fill(shape);
            graphics2D.fillRect(shape.x - 2, shape.y -2,shape.width+2,shape.height +2 );
        }
        if(border){
            Stroke stroke = graphics2D.getStroke();
            graphics2D.setStroke(new BasicStroke(thickness));
            graphics2D.setColor(borderColor);
//            graphics2D.drawRect(textX, textY - visualBounds.height, stringBounds.width, stringBounds.height);
//            graphics2D.drawRect(textX, textY, stringBounds.width, stringBounds.height);
            graphics2D.drawRect(shape.x - 2, shape.y -2,shape.width+2,shape.height +2 );
            graphics2D.setStroke(stroke);
        }
        graphics2D.setColor(textColor);
        graphics2D.drawString(stringNum, textX, textY);
        graphics2D.setColor(old);
    }

    public static Rectangle getPixelBounds(String text, Graphics2D graphics2D, int textX, int textY) {
        return graphics2D.getFont().createGlyphVector(graphics2D.getFontRenderContext(), text).getPixelBounds(graphics2D.getFontRenderContext(), textX, textY);
    }

    public static Rectangle getStringBounds(String text, Graphics2D graphics2D) {

        // get the bounds of the string to draw.
        FontMetrics fontMetrics = graphics2D.getFontMetrics();
        Rectangle stringBounds = fontMetrics.getStringBounds(text, graphics2D).getBounds();

        return stringBounds.getBounds();
    }
    public static Rectangle getVisualBounds(String text, Graphics2D graphics2D) {

        // get the bounds of the string to draw.
        FontMetrics fontMetrics = graphics2D.getFontMetrics();

        // get the visual bounds of the text using a GlyphVector.
        Font font = graphics2D.getFont();
        FontRenderContext renderContext = graphics2D.getFontRenderContext();
        GlyphVector glyphVector = font.createGlyphVector(renderContext, text);
        Rectangle visualBounds = glyphVector.getVisualBounds().getBounds();

        return visualBounds.getBounds();
    }

}
