package View;

import Algorithms.Constants;
import Algorithms.LatitudeAndLongitude;
import Controller.MainController;
import org.jdesktop.swingx.mapviewer.GeoPosition;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Point2D;
import java.text.DecimalFormat;
import java.text.ParseException;

/**
 * Created by Shrey on 12/11/13.
 */
public class SmoothPan implements Runnable {

    public static final int NUM_FRAMES_PER_SECOND = 10;
    public static final float DURATION_OF_ANIMATION = 1.0f; // in seconds

    Thread animationThread = null;
    boolean isAnimationThreadSuspended = true;

    MapView mapView;
    Glass glass;
    MainController mainController;


    boolean isAnimationActive = false;
    float animationParameter = 0; // in [0.0f,1.0f], where 0.0f corresponds to the start of the animation, and 1.0f corresponds to the end
//    GanttFoldState oldFoldState = new GanttFoldState(); // corresponds to start of animation
//    GanttFoldState newFoldState = new GanttFoldState(); // corresponds to end of animation




    public void requestRedraw() {
        mapView.repaint();
        glass.repaint();
    }


    public void startBackgroundWork() {
        if ( animationThread == null ) {
            animationThread = new Thread( this );
            isAnimationThreadSuspended = false;
            animationThread.start();
        }
        else {
            // The the thread already exists; we simply need to "wake it up"
            if ( isAnimationThreadSuspended ) {
                isAnimationThreadSuspended = false;
                synchronized( this ) {
                    notify();
                }
            }
        }
    }
    public void stopBackgroundWork() {
        isAnimationThreadSuspended = true;
    }

    public void run() {
        try {
            while (true) {
                synchronized( this ) {

                    if ( isAnimationActive ) {
                        animationParameter += 1.0f/(NUM_FRAMES_PER_SECOND * DURATION_OF_ANIMATION );
                        if ( animationParameter > 1.0f )
                            animationParameter = 1.0f;
                        this.mainController.updateAnimationState(animationParameter);

                    }
                }


                requestRedraw();
//
                if (animationParameter == 1.0f) {
                    stopBackgroundWork();
                }


                // Now the thread checks to see if it should suspend itself
                if ( isAnimationThreadSuspended ) {
                    synchronized( this ) {
                        while ( isAnimationThreadSuspended ) {
                            wait();
                        }
                    }
                }

                Thread.sleep((long)
                        1000.0f / NUM_FRAMES_PER_SECOND   // sleepIntervalInMilliseconds
                );


            }
        }
        catch (InterruptedException e) { }
    }

    public void startAnimation() {

        this.mainController.setAnimationState();
        animationParameter = 0.0f;
        isAnimationActive = true;
        startBackgroundWork();
    }


    public void stopAnimation() {
        isAnimationActive = false;
        stopBackgroundWork();
    }

    public void setMapView(MapView mapView) {
        this.mapView = mapView;
    }

    public void setGlass(Glass glass) {
        this.glass = glass;
    }

    public void setMainController(MainController mainController) {
        this.mainController = mainController;
    }
}
