package View;

import Algorithms.Constants;


/**
 * Created with IntelliJ IDEA.
 * User: Shrey
 * Date: 10/20/13
 * Time: 4:01 AM
 * To change this template use File | Settings | File Templates.
 * Thanks to Michael McGuffin (http://profs.etsmtl.ca/mmcguffin/)
 * for the framework of animation code.
 *
 */

public class AnimationGantt implements Runnable {

    Thread animationThread = null;
    boolean isAnimationThreadSuspended = true;

    GroupGanttView ganttView;


    boolean isAnimationActive = false;
    double animationParameter = 10.0f; // in [0.0f,1.0f], where 0.0f corresponds to the start of the animation, and 1.0f corresponds to the end
//    GanttFoldState oldFoldState = new GanttFoldState(); // corresponds to start of animation
//    GanttFoldState newFoldState = new GanttFoldState(); // corresponds to end of animation


    public AnimationGantt(GroupGanttView ganttView) {
        this.ganttView = ganttView;
    }

    public void requestRedraw() {
        ganttView.repaint(0,0,this.ganttView.getWidth(),this.ganttView.getHeight());
    }


    public void startBackgroundWork() {
        if ( animationThread == null ) {
            animationThread = new Thread( this );
            isAnimationThreadSuspended = false;
            animationThread.start();
        }
        else {
            // The the thread already exists; we simply need to "wake it up"
            if ( isAnimationThreadSuspended ) {
                isAnimationThreadSuspended = false;
                synchronized( this ) {
                    notify();
                }
            }
        }
    }
    public void stopBackgroundWork() {
        isAnimationThreadSuspended = true;
    }

    public void run() {
        try {
            while (true) {
                synchronized( this ) {

                    if ( isAnimationActive ) {
                        animationParameter += 1.0f/(Constants.NUM_FRAMES_PER_SECOND * Constants.DURATION_OF_ANIMATION );
                        if ( animationParameter > 1.0f)
                            animationParameter = 1.0f;

                        if(this.ganttView.autoFrameWithFolds)
                        {
                            this.ganttView.updateAnimationWithAutoFoldState(animationParameter);
                        }
                        else if (this.ganttView.autoFrame) {

                            this.ganttView.updateAutoFrame(animationParameter);

                        }
                        else if (this.ganttView.frameVisibleWithFold) {
                            this.ganttView.updateAnimationWithAutoFoldStateVisibleInterval(animationParameter);

                        } else if (this.ganttView.animation) {

                            this.ganttView.updateAnimationState(animationParameter);
                        } else if (this.ganttView.autoFrameWithUnFold) {
                            this.ganttView.updateUnFoldWithFrame(animationParameter);

                        } else if (this.ganttView.frameVisible) {
                            this.ganttView.updateAutoFrameVisibleInterval(animationParameter);
                        }else if (this.ganttView.frameVisibleWithUnFold) {
                            this.ganttView.updateUnFoldWithFrameVisibleInterval(animationParameter);
                        } else {
                            this.ganttView.updateResetAnimationState(animationParameter);

                        }
                    }
                }

                requestRedraw();
//
                if (animationParameter == 1.0f) {

                    stopBackgroundWork();
                }
                // Now the thread checks to see if it should suspend itself
                if ( isAnimationThreadSuspended ) {
                    synchronized( this ) {
                        while ( isAnimationThreadSuspended ) {
                            wait();
                        }
                    }
                }

                Thread.sleep((long)
                        1000.0f / Constants.NUM_FRAMES_PER_SECOND   // sleepIntervalInMilliseconds
                );

            }
        }
        catch (InterruptedException e) { }
    }

    public void startGanttAnimation() {
        this.ganttView.setAnimationState();
        animationParameter = 0.0f;
        isAnimationActive = true;
        startBackgroundWork();
    }

    public void startAutoFrameAnimation() {
        this.ganttView.setAutoFrameState();
//        this.ganttView.setAnimationState();
        animationParameter =0.0f;
        isAnimationActive = true;
        startBackgroundWork();
    }

    public void resetGanttAnimation() {
        animationParameter =0.0f;
        this.ganttView.setResetAnimation();
//        this.ganttView.setAnimationState();
        isAnimationActive = true;
        startBackgroundWork();
    }

    public void startAutoFrameWithFolds() {
        animationParameter =0.0f;

        this.ganttView.setAnimationState();
        this.ganttView.setAutoFrameState();
        isAnimationActive = true;
        startBackgroundWork();
    }
    public void startAutoFrameWithFoldsVisibleInterval() {
        animationParameter =0.0f;

        this.ganttView.setAnimationState();
        this.ganttView.setAutoFrameVisibleInterval();
        isAnimationActive = true;
        startBackgroundWork();
    }

    public void startUnFoldWithFrame(){
        animationParameter =0.0f;
        this.ganttView.setResetAnimation();
        isAnimationActive = true;
        startBackgroundWork();
    }

    public void startUnFoldVisible(){
        animationParameter =0.0f;
        this.ganttView.setResetAnimation();
        isAnimationActive = true;
        startBackgroundWork();
    }

    public void startAutoFrameVisible(){
        animationParameter =0.0f;
        this.ganttView.setAutoFrameVisibleInterval();
        isAnimationActive = true;
        startBackgroundWork();
    }

    public void stopAnimation() {
        isAnimationActive = false;
        stopBackgroundWork();
    }
}


