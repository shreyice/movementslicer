
package View;

import org.jdesktop.swingx.mapviewer.DefaultWaypoint;
import org.jdesktop.swingx.mapviewer.GeoPosition;

import java.awt.*;

/**
 * A waypoint that also has a color and a label
 * @author Martin Steiger
 */
public class MapMeetPoint extends DefaultWaypoint
{
	private final String label;

	/**
	 * @param label the text
	 * @param coord the coordinate
	 */
	public MapMeetPoint(String label, GeoPosition coord)
	{
		super(coord);
		this.label = label;
	}

	/**
	 * @return the label text
	 */
	public String getLabel()
	{
		return label;
	}


	
}
