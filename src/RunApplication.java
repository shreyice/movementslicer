import Controller.MainController;
import Model.PersonListModel;
import View.MainView;
import org.joda.time.DateTimeZone;

import javax.swing.*;

/**
 * Created with IntelliJ IDEA.
 * User: Shrey
 * Date: 9/23/13
 * Time: 9:21 PM
 * To change this template use File | Settings | File Templates.
 */
public class RunApplication {

     public static void main(String[] args) {


        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {

                try {
                    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                } catch (Exception e) {
                }
//                DateTimeZone.setDefault(DateTimeZone.UTC);
                PersonListModel personListModel = new PersonListModel();
                MainView mainView = new MainView();
                MainController mainController = new MainController(personListModel ,mainView);
            }
        });
    }

}
