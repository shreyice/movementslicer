import Algorithms.StayPoint;
import com.google.common.annotations.GwtCompatible;
import com.google.common.collect.Sets;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * Created by Shrey on 1/30/14.
 */
public class Testing {




        public static void main(String[] args) {


            Set<Set<Integer>> list = Sets.powerSet(Sets.newHashSet(Arrays.asList(1,101, 2, 3, 4)));

            final List<List<String>> allSubsets = powerSet(Arrays.asList(1, 2, 3, 4), 0);
            for (List<String> subsets : allSubsets) {
                System.out.println(subsets);
            }
        }

        private static List<List<String>> powerSet(final List<Integer> values,
        int index) {
            if (index == values.size()) {
                return new ArrayList<>();
            }
            int val = values.get(index);
            List<List<String>> subset = powerSet(values, index + 1);
            List<List<String>> returnList = new ArrayList<>();
            returnList.add(Arrays.asList(String.valueOf(val)));
            returnList.addAll(subset);
            for (final List<String> subsetValues : subset) {
                for (final String subsetValue : subsetValues) {
                    returnList.add(Arrays.asList(val + "," + subsetValue));
                }
            }
            return returnList;
        }
}
