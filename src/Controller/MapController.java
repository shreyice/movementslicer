package Controller;

import Algorithms.Group;
import Algorithms.MeetingPoint;
import Model.GroupGanttModel;
import View.MapView;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Shrey
 * Date: 11/18/13
 * Time: 6:35 PM
 * To change this template use File | Settings | File Templates.
 */
public class MapController implements Controller, PropertyChangeListener{

    GroupGanttModel model;
    MapView mapView;
    Controller parentController;


    public MapController( GroupGanttModel model, MapView mapView) {

        this.model = model;
        this.mapView = mapView;
//        this.model.addPropertyChangeListener(this);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        //To change body of implemented methods use File | Settings | File Templates.

        if(evt.getPropertyName().equals("highlighted change")) {
            this.model.updateGroupModel((Group) evt.getNewValue());
            this.mapView.updateWayPointList();
            this.mapView.repaint();
        }
        if(evt.getPropertyName().equals("Person Centric")) {
            this.mapView.updateWayPointList();
            this.mapView.repaint();
        }

    }


    public List<MeetingPoint> listOfMeetingPointsToDisplay() {
        return this.model.getSelectPeopleMeetings();
    }







    @Override
    public void setParentController(Controller controller) {
        //To change body of implemented methods use File | Settings | File Templates.
        this.parentController = controller;

    }
}
