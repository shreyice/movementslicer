package Controller;

import Algorithms.MinMax;
import Algorithms.Node;
import Algorithms.Person;
import AppEvent.AppEvent;
import Model.MatrixModel;
import View.MatrixView;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Shrey
 * Date: 10/9/13
 * Time: 5:53 PM
 * To change this template use File | Settings | File Templates.
 */
public class MatrixController implements Controller,PropertyChangeListener{

    MatrixModel model;
    MatrixView view;

    Controller parentController;

    public MatrixController(MatrixModel model, MatrixView view) {
        this.model = model;
        this.view = view;
    }


    public void handleAppEvent(AppEvent appEvent) {

        if(appEvent.isNavEvent()) {

            AppEvent ae = new AppEvent(AppEvent.DATA_EVENT);
            ae.setObject(this.model.getGroup());
            MainController mainController = (MainController) parentController;
            mainController.handleAppEvent(ae);

        }
    }
    public int positionOfNode(int node) {
        return this.model.positionOfNode(node);
    }

    public int numberOfNode() {
        return this.model.numberOfNode();
    }

    public MinMax getMinMax() {
        return this.model.getMinMax();
    }

    public int nodeAtPosition(int node) {
        return this.model.nodeAtPosition(node);
    }

    public int getNoOfMeetingsBetween(int i, int j) {
        return this.model.getNoOfMeetingsBetween(i, j);
    }

    public Node[][] getMeet() {
        return this.model.getMeet();
    }

    public void reorderMatrix(int iterations) {
        this.model.reorderNodesBasedOnBarycentric(1);

        this.view.repaint();
    }

    public void updatePerson(ArrayList<Person> persons) {
        this.model.setPersons(persons);
        this.view.revalidate();
        this.view.repaint();
    }

    public void setParentController(Controller parentController) {
        this.parentController = parentController;
    }

    public void setAndRemoveHighlight(int i, int j) {
        this.model.setAndRemoveHighlightedNode(i,j);
        this.view.repaint();
    }

    public void setAndRemoveHighlightedPerson(int i) {
        this.model.setAndRemoveHighlightedPerson(i);
        this.view.repaint();
    }

    public boolean isHighlightedPerson(int i) {
        return this.model.isPersonHighlighted(i);

    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        String propertyName = evt.getPropertyName();
        if(!propertyName.equals("Person Centric"))
        this.view.personId = (int)evt.getNewValue();
        this.view.repaint();

    }

    public void deselectAll() {
        this.model.deselectAll();
    }

    public void setTimeInside(long time) {
         this.model.setTimeInside(this.model.getTimeInside() + time);
    }

    public void setClickRightCount(long count) {
         this.model.setClickRightCount(this.model.getClickRightCount() + count);
    }

    public void setClickLeftCount(long count) {
        this.model.setClickLeftCount(this.model.getClickLeftCount() + count);
    }
}
