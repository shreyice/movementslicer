package Controller;

import Algorithms.Group;
import Algorithms.MeetingPoint;
import Model.GroupGanttModel;
import Model.MatrixModel;
import View.GroupGanttView;
import org.joda.time.DateTime;

import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Shrey
 * Date: 10/21/13
 * Time: 8:11 PM
 * To change this template use File | Settings | File Templates.
 */
public class GroupGanttController implements PropertyChangeListener {

    GroupGanttModel ganttModel;
    GroupGanttView ganttView;
    MainController parentController;

    public GroupGanttController(GroupGanttModel ganttModel, GroupGanttView ganttView) {
        this.ganttModel = ganttModel;
        this.ganttView = ganttView;
        this.ganttView.setCurrentStartDate(startDate().getMillis());
        this.ganttView.setCurrentEndDate(endDate().getMillis());


    }

    public Group getGroup() {
            return this.ganttModel.getGroup();
    }

    public DateTime startDate() {
        return this.ganttModel.startDate();
    }

    public DateTime endDate() {
        return this.ganttModel.endDate();
    }

    public int numberOfPeopleSelected() {
        return this.ganttModel.numberOfPeopleSelected();
    }

    public int positionOfSelectedPersonInArrayList(int i) {
        return this.ganttModel.positionOfSelectedPersonInArrayList(i);
    }

    public int numberOfUniquePlacesSelectedPeopleMeet()
    {
        return this.ganttModel.numberOfUniquePlacesSelectedPeopleMeet();
    }

    public List<MeetingPoint> getMeetingPointsToDisplay(){
        return this.ganttModel.getSelectPeopleMeetings();
    }

    public ArrayList<Integer> listOfStayRegionsForMeetings() {
        return this.ganttModel.getListOfUniqueStayRegions();
    }

    public ArrayList<Integer> selectedPeople() {
        return this.ganttModel.getSelectedPeople();
    }

    public void setParentController(MainController controller) {
        this.parentController = controller;
    }

    public void drawCurves(MeetingPoint meetingPoint, double startX, double endX) {
        if(meetingPoint != null)
            this.parentController.drawCurves(meetingPoint.getCenter(), startX,endX);
                               else
            this.parentController.drawCurves(null, startX,endX);

    }

    public String getName(int i) {
        return this.ganttModel.getName(i);

    }

    public void setName(int i, String s) {
        this.ganttModel.setName(i , s);
    }

    public void sort(){
        this.ganttModel.sort();
    }

    public void removeStayRegion(int id) {
        this.ganttModel.removeStayRegion(id);
        this.ganttView.invalidate();
        this.ganttView.repaint();

    }

    public int numberOfUniquePlacesForAPerson(int i) {
        return this.ganttModel.numberOfUniquePlacesForAPerson(i);
    }

    public int totalNumberOfRowsWhenSeperatePerson(){
        return this.ganttModel.totalNumberOfRowsWhenSeperatePerson();
    }

    public int totalNumberOfRows() {
        if(this.ganttModel.isDiffRows())
            return totalNumberOfRowsWhenSeperatePerson();
        else return numberOfUniquePlacesSelectedPeopleMeet();
    }

    public ArrayList<Integer> listOfUniquePlacesVisitedByAPerson(int i) {
        return this.ganttModel.listOfUniquePlacesVisitedByAPerson(i);
    }

    public boolean isDiffRows() {
        return this.ganttModel.isDiffRows();
    }

    public void setDiffRows() {
        this.ganttModel.setDiffRows();
    }

    public int noOfTimes(int i) {
        return this.ganttModel.noOfTimes(i);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if(evt.getPropertyName().equals("OrderChanged")) {
            this.ganttModel.updateGroupModel(getGroup());
            this.ganttView.setCurrentStartDate(startDate().getMillis());
            this.ganttView.setCurrentEndDate(endDate().getMillis());
            this.ganttView.initialize();
            this.ganttView.revalidate();
            this.ganttView.repaint();
            return;
        }

        if(!evt.getPropertyName().equals("Person Centric")) {
            this.ganttModel.updateGroupModel((Group) evt.getNewValue());
            this.ganttView.setCurrentStartDate(startDate().getMillis());
            this.ganttView.setCurrentEndDate(endDate().getMillis());
            this.ganttView.recalculateSize();
            this.ganttView.initialize();
            this.parentController.update();
            this.ganttView.revalidate();
            this.ganttView.repaint();

        }


    }

    public List<MeetingPoint> getJustMeeting() {
        return this.ganttModel.getJustMeeting();
    }

    public void higihLightPerson(int id) {
        this.ganttModel.setHighlightPerson(id);
    }


    public void updateMainView() {
        this.parentController.update();
    }

    public void fireUpdate(){
        this.ganttModel.fireUpdate();
    }


}
