package Controller;

import Algorithms.*;
import AppEvent.AppEvent;
import Model.GroupGanttModel;
import Model.MatrixModel;
import Model.PersonListModel;
import View.*;
import org.jdesktop.swingx.mapviewer.GeoPosition;

import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLProfile;
import javax.swing.*;
import java.awt.*;
import java.awt.geom.Point2D;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Shrey
 * Date: 10/9/13
 * Time: 1:20 PM
 * To change this template use File | Settings | File Templates.
 */
public class MainController implements PropertyChangeListener, Controller {

    PersonListModel personListModel;
    MainView mainView;
    Controller childController;
    MatrixController matrixController;
    GroupGanttController groupGanttController;
    MapController mapController;
    MatrixView matrixView;
    MatrixModel matrixModel;
    GroupGanttModel ganttModel;
    GroupGanttView ganttView;
    MapView mapView;
    JSplitPane jSplitPanetop;
    JSplitPane jSplitPane;
    Glass glass;

    Point2D current;
    Point2D destination;
    LatitudeAndLongitude lnl = null;
    double startX = -1 ;
    double endX = -1;

    SmoothPan smoothPan = new SmoothPan();

    ArrayList<Long> timeInsideMatrix;
    ArrayList<Long> rightClickInsideMatrix;
    ArrayList<Long> leftClickInsideMatrix;

    ArrayList<Long> timeInsideGantt;
    ArrayList<Long> rightClickInsideGantt;
    ArrayList<Long> leftClickInsideGantt;

    public MainController(PersonListModel personListModel, MainView mainView) {
        this.personListModel = personListModel;
        this.mainView = mainView;
        this.personListModel.addPropertyChangeListener(this);
        this.childController = null;
        this.smoothPan.setMainController(this);
        mainView.setController(this);
    }

    public void openFileChooser() {
        FileChooserView fileChooserView = new FileChooserView(new JFrame());
        loadPersonData(fileChooserView.getDirs());
    }

    void createMatrixVC() {
        if (this.childController == null) {
            this.matrixModel = new MatrixModel();
            this.matrixView = new MatrixView();
            this.matrixController = new MatrixController(matrixModel, matrixView);
            matrixController.updatePerson(this.personListModel.getPersons());
            this.ganttModel = new GroupGanttModel();
            this.ganttModel.addPropertyChangeListener(this.groupGanttController);
            this.matrixModel.addPropertyChangeListener(this.groupGanttController);
            this.mapView = new MapView();
            this.mapController = new MapController(this.ganttModel, this.mapView);
            this.matrixModel.addPropertyChangeListener(this.mapController);
            this.mapView.setMapController(this.mapController);
            this.smoothPan.setMapView(this.mapView);
             this.ganttModel.addPropertyChangeListener(this.matrixController);
            JScrollPane matrix = new JScrollPane(this.matrixView);
            JScrollPane map = new JScrollPane(this.mapView);
            jSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
                    matrix, map);
            jSplitPane.setOneTouchExpandable(true);
            jSplitPane.setDividerLocation(this.mainView.getjFrame().getWidth() / 2);
            this.mainView.getjFrame().add(jSplitPane,"grow");
            matrixView.setVisible(true);
            matrixView.setController(matrixController);
            this.childController = matrixController;
            matrixController.setParentController(this);
            mapController.setParentController(this);
            this.mainView.getjFrame().revalidate();
            this.mainView.getjFrame().repaint();

        }
        else if(!(this.childController instanceof MatrixController)){
            this.mainView.getjFrame().remove(this.jSplitPane);
            this.matrixModel = new MatrixModel();
            this.matrixView = new MatrixView();
            this.matrixController = new MatrixController(matrixModel, matrixView);
            matrixController.updatePerson(this.personListModel.getPersons());
            this.ganttModel = new GroupGanttModel();
//            this.ganttModel.setGroupAndPersons(this.matrixModel.getGroup());
            this.mapView = new MapView();
            this.mapController = new MapController(this.ganttModel, this.mapView);
            this.matrixModel.addPropertyChangeListener(this.mapController);
            this.matrixModel.addPropertyChangeListener(this.groupGanttController);
            this.ganttModel.addPropertyChangeListener(this.matrixController);
            this.mapView.setMapController(this.mapController);
            JScrollPane matrix = new JScrollPane(this.matrixView);
            JScrollPane map = new JScrollPane(this.mapView);
            jSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,matrix, map);
            jSplitPane.setOneTouchExpandable(true);
            jSplitPane.setDividerLocation(this.mainView.getjFrame().getWidth()/2);
            this.mainView.getjFrame().add(jSplitPane);
            matrixView.setVisible(true);
            matrixView.setController(matrixController);
            this.childController = matrixController;
            matrixController.setParentController(this);
            mapController.setParentController(this);
            this.mainView.getjFrame().repaint();
        }
        else {
            this.matrixController.updatePerson(this.personListModel.getPersons());
        }

    }
    JScrollPane gantt;
    JScrollPane matrix;
    JScrollPane map;
    void createGroupGanttVC(Group group) {
        if(this.ganttView != null) this.mainView.getjFrame().remove(this.ganttView);
        this.mainView.getjFrame().remove(this.jSplitPane);
        this.ganttView = new GroupGanttView();

        this.glass = new Glass();
        this.smoothPan.setGlass(this.glass);
        this.groupGanttController = new GroupGanttController(this.ganttModel, this.ganttView);
        this.matrixModel.addPropertyChangeListener(this
        .groupGanttController);
        this.matrixModel.addPropertyChangeListener(this);
        this.ganttView.setGroupGanttController(this.groupGanttController);
        this.ganttModel.addPropertyChangeListener(this.matrixController);
        this.ganttModel.addPropertyChangeListener(this.mapController);
        matrix = new JScrollPane(this.matrixView);
        gantt = new JScrollPane(this.ganttView);
        map = new JScrollPane(this.mapView);
        jSplitPanetop = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, matrix, map);
        jSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, jSplitPanetop, gantt);
        jSplitPane.setOneTouchExpandable(true);
        jSplitPanetop.setDividerLocation(this.mainView.getjFrame().getWidth() / 2);
        jSplitPane.setDividerLocation(this.mainView.getjFrame().getHeight() / 2);
        this.mainView.getjFrame().add(jSplitPane, "grow");
        this.groupGanttController.setParentController(this);
        gantt.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

        gantt.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        this.ganttView.recalculateSize();
        this.ganttView.initialize();

        this.ganttView.setVisible(true);
        mainView.getjFrame().setGlassPane(this.glass);
        this.glass.setVisible(true);
        this.mainView.getjFrame().revalidate();
        this.mainView.getjFrame().repaint();
    }

    public void handleAppEvent(AppEvent appEvent) {
        if (appEvent.isDataEvent()) {
            createGroupGanttVC((Group)appEvent.getObject());
        }
    }


    public void updateCurves(LatitudeAndLongitude lnl, double startX, double endX) {

        if (lnl != null
                ) {

            DecimalFormat df = new DecimalFormat("0.00000");
            String formate = df.format(lnl.getLatitude());
            String formate1 = df.format(lnl.getLongitude());
            try {
                double finalValue = (Double) df.parse(formate);
                double finalValue1 = (Double) df.parse(formate1);
                //location of Java
                GeoPosition gp = new GeoPosition(finalValue, finalValue1);

//                this.mapView.setAddressLocation(gp);

                //convert to world bitmap
                Point2D gp_pt = this.mapView.getTileFactory().geoToPixel(gp, this.mapView.getZoom());

                panToLocation(gp_pt);

                //convert to screen
                Rectangle rect = this.mapView.getViewportBounds();
                Point2D converted_gp_pt = new Point((int) gp_pt.getX() - (int) rect.getX(),
                        (int) gp_pt.getY() - (int) rect.getY());

                Point point = SwingUtilities.convertPoint(this.mapView
                        , (int) converted_gp_pt.getX()
                        , (int) converted_gp_pt.getY()
                        , this.glass);

                if(!this.mapView.getVisibleRect().contains((int) converted_gp_pt.getX()
                        , (int) converted_gp_pt.getY())) {

                    this.lnl = lnl;
                    this.startX = startX;
                    this.endX = endX;
                    this.glass.setGlass(-1, -1, -1, -1);
                    return;
                }
                Point point1 = SwingUtilities.convertPoint(this.ganttView
                        , (int) startX
                        , (int) endX
                        , this.glass);

                this.glass.setGlass( point.getX(), point.getY(), point1.getX(), point1.getY());


            } catch (ParseException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }


        }
        else {
            this.lnl = lnl;
            this.startX = startX;
            this.endX = endX;
            this.glass.setGlass(-1, -1, -1, -1);
        }

    }


    public void drawCurves(LatitudeAndLongitude lnl, double startX, double endX) {

        if (lnl != null
                ) {
            this.lnl = lnl;
            this.startX = startX;
            this.endX = endX;


            try {
                DecimalFormat df = new DecimalFormat("0.00000");
                String formate = df.format(lnl.getLatitude());
                String formate1 = df.format(lnl.getLongitude());
                double finalValue = (double) df.parse(formate);
                double finalValue1 = (double) df.parse(formate1);
                //location of Java
                GeoPosition gp = new GeoPosition(finalValue, finalValue1);
//                this.mapView.setAddressLocation(gp);
                //convert to world bitmap
                Point2D gp_pt = this.mapView.getTileFactory().geoToPixel(gp, this.mapView.getZoom());

                panToLocation(gp_pt);
                smoothPan.startAnimation();

            } catch (ParseException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        else {
            this.lnl = lnl;
            this.startX = startX;
            this.endX = endX;
            this.glass.setGlass(-1, -1, -1, -1);
        }
        this.mapView.invalidate();
        this.mapView.repaint();
        this.mainView.getjFrame().invalidate();
        this.mainView.getjFrame().repaint();

    }


    public void panToLocation(Point2D geoPosition) {

        this.destination = geoPosition;
    }

    public void setAnimationState() {

        this.current = this.mapView.getCenter();

    }

    public void updateAnimationState(float animationParameter ) {

        this.mapView.setCenter(
                new Point2D.Double(this.current.getX() + (this.destination.getX() - this.current.getX()) * animationParameter,
                        this.current.getY() + (this.destination.getY() - this.current.getY()) * animationParameter

                ));

        this.current = this.mapView.getCenter();
        updateCurves(this.lnl, this.startX, this.endX);
    }

    void loadPersonData(File[] dirs) {
        personListModel.setPersons(dirs);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        System.out.println(evt.getPropertyName());
        if(evt.getPropertyName().equals("People List Loaded"))
        createMatrixVC();
        else if (evt.getPropertyName().equals("Evaluation Loaded")) {
        }
        else if (evt.getPropertyName().equals("highlighted change")) {

            this.mainView.getjFrame().revalidate();
            this.mainView.getjFrame().repaint();
            this.jSplitPane.revalidate();
            this.jSplitPane.repaint();

//            createGroupGanttVC((Group)evt.getNewValue());
        }
    }

    @Override
    public void setParentController(Controller controller) {
        //To change body of implemented methods use File | Settings | File Templates.

    }

    void addPanels() {
        this.matrixModel.setTimeInside(0);
        this.matrixModel.setClickRightCount(0);
        this.matrixModel.setClickLeftCount(0);
        if(ganttView!=null)
        this.ganttView.initialize();
        this.mainView.getjFrame().add(jSplitPane, "grow");
        this.mainView.getjFrame().invalidate();
        this.mainView.getjFrame().repaint();
    }
    void removePanels(){
        this.timeInsideMatrix.add(this.matrixModel.getTimeInside());
        this.rightClickInsideMatrix.add(this.matrixModel.getClickRightCount());
        this.leftClickInsideMatrix.add(this.matrixModel.getClickLeftCount());
        this.mainView.getjFrame().remove(jSplitPane);
        this.mainView.getjFrame().invalidate();
        this.mainView.getjFrame().repaint();
    }
    public void update() {
            this.mainView.getjFrame().revalidate();
            this.mainView.getjFrame().repaint();

    }

}