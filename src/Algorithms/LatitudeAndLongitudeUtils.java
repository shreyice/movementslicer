/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Algorithms;

import java.util.ArrayList;

/**
 *
 * @author Shrey
 */
public class LatitudeAndLongitudeUtils {
    
      
    public static double distance(LatitudeAndLongitude a, LatitudeAndLongitude b)
        {
            double distance=0;
            double R = 6378137/1000; // Radius of the earth in km
            double dLat = Math.toRadians(b.getLatitude()-a.getLatitude());  // deg2rad below
            double dLon = Math.toRadians(b.getLongitude()-a.getLongitude());
            double temp = Math.sin(dLat/2) * Math.sin(dLat/2) +    Math.cos(Math.toRadians(a.getLatitude())) * Math.cos(Math.toRadians(b.getLatitude())) *    Math.sin(dLon/2) * Math.sin(dLon/2);
            double c = 2 * Math.atan2(Math.sqrt(temp), Math.sqrt(1-temp));
            distance = R * c; // Distance in km
            
//            System.out.println(a.getLongitude()+"   "+a.getLatitude()+"   "+b.getLongitude()+"  "+b.getLatitude()+"   "+"   "+distance);
            
            return distance*1000.0;// distance in meteres
        }
    
    public static LatitudeAndLongitude meanFromLatitudeAndLongitude(ArrayList<LatitudeAndLongitude> temp)
    {
        LatitudeAndLongitude output = new LatitudeAndLongitude();
        double meanLatitude = 0.0f;
        double meanLongitude = 0.0f;
        double meanHyp = 0.0f;
        double sumX =0.0f;
        double sumY =0.0f;
        double sumZ =0.0f;
        double meanX =0.0f;
        double meanY =0.0f;
        double meanZ =0.0f;
        double count =1;
        for(int i=0; i < temp.size();++i)
        {
//           System.out.println(temp.get(i).getLatitude()+"  "+temp.get(i).getLatitude()); 
           double x = Math.cos(Math.toRadians(temp.get(i).getLatitude())) *Math.cos(Math.toRadians(temp.get(i).getLongitude()));
           double y = (double)Math.cos(Math.toRadians(temp.get(i).getLatitude()))*Math.sin(Math.toRadians(temp.get(i).getLongitude()));
           double z = Math.sin(Math.toRadians(temp.get(i).getLatitude()));
           sumX = sumX + x;
           sumY = sumY + y;
           sumZ = sumZ + z;
           count++;
        }   
            
        meanX = sumX/count;
        meanY = sumY/count;
        meanZ = sumZ/count;
        meanLongitude = Math.atan2(meanY,meanX);
        meanHyp = Math.sqrt(meanX*meanX+meanY*meanY);
        meanLatitude = Math.atan2(meanZ, meanHyp);
        output.setLatitude(Math.toDegrees(meanLatitude));
        output.setLongitude(Math.toDegrees(meanLongitude));
//        System.out.println(meanLatitude+"   "+meanLongitude);
        return output;
    }
    
    public static LatitudeAndLongitude mean(ArrayList<LatitudeAndLongitude> temp,int i, int j)
    {
        LatitudeAndLongitude output = new LatitudeAndLongitude();
        double meanLatitude = 0.0;
        double meanLongitude = 0.0;
        double meanHyp = 0.0;
        double sumX = 0.0;
        double sumY = 0.0;
        double sumZ = 0.0;
        double meanX = 0.0;
        double meanY = 0.0;
        double meanZ = 0.0;
        double count =1;
        for(int k=i; k <=j;++k)
        {
           double x = Math.cos(Math.toRadians(temp.get(k).getLatitude()))*Math.cos(Math.toRadians(temp.get(k).getLongitude()));
           double y = Math.cos(Math.toRadians(temp.get(k).getLatitude()))*Math.sin(Math.toRadians(temp.get(k).getLongitude()));
           double z = Math.sin(Math.toRadians(temp.get(k).getLatitude()));
           sumX = sumX + x;
           sumY = sumY + y;
           sumZ = sumZ + z;
           count++;
        }   
            
        meanX = sumX/count;
        meanY = sumY/count;
        meanZ = sumZ/count;
        meanLongitude = Math.atan2(meanY,meanX);
        meanHyp = Math.sqrt(meanX*meanX+meanY*meanY);
        meanLatitude = Math.atan2(meanZ, meanHyp);
        output.setLatitude(Math.toDegrees(meanLatitude));
        output.setLongitude(Math.toDegrees(meanLongitude));
        
        return output;
    }
    
    /**
     *
     * @param temp
     * @param i
     * @param j
     * @return
     */
    public static LatitudeAndLongitude meanfromLocations(ArrayList<GPSLocation> temp,int i, int j)
    {
        LatitudeAndLongitude output = new LatitudeAndLongitude();
        double meanLatitude = 0.0;
        double meanLongitude = 0.0;
        double meanHyp = 0.0;
        double sumX = 0.0;
        double sumY = 0.0;
        double sumZ = 0.0;
        double meanX = 0.0;
        double meanY = 0.0;
        double meanZ = 0.0;
        double count =1;
        for(int k=i; k <=j;++k)
        {
//           System.out.println(temp.get(k).getLatnlog().getLatitude()+","+temp.get(k).getLatnlog().getLongitude()); 
           double x = Math.cos(Math.toRadians(temp.get(k).getLatnlog().getLatitude()))*Math.cos(Math.toRadians(temp.get(k).getLatnlog().getLongitude()));
           double y = Math.cos(Math.toRadians(temp.get(k).getLatnlog().getLatitude()))*Math.sin(Math.toRadians(temp.get(k).getLatnlog().getLongitude()));
           double z = Math.sin(Math.toRadians(temp.get(k).getLatnlog().getLatitude()));
           sumX = sumX + x;
           sumY = sumY + y;
           sumZ = sumZ + z;
           count++;
        }   
            
        meanX = sumX/count;
        meanY = sumY/count;
        meanZ = sumZ/count;
        meanLongitude = Math.atan2(meanY,meanX);
        meanHyp = Math.sqrt(meanX*meanX+meanY*meanY);
        meanLatitude = Math.atan2(meanZ, meanHyp);
        output.setLatitude(Math.toDegrees(meanLatitude));
        output.setLongitude(Math.toDegrees(meanLongitude));
//        System.out.println("mean "+Math.toDegrees(meanLatitude)+","+Math.toDegrees(meanLongitude));
        return output;
    }
    
    public static LatitudeAndLongitude MaximumLatitudeAndLongiutde(ArrayList<LatitudeAndLongitude> input)
    {
        LatitudeAndLongitude output= new LatitudeAndLongitude(input.get(0));
        TileCoord2D out = MapPoint.LatLongToPixelXY(output.getLatitude(), output.getLongitude(), 20);
        for(int i=0; i<input.size();++i)
        {
            TileCoord2D in = MapPoint.LatLongToPixelXY(input.get(i).getLatitude(), input.get(i).getLongitude(), 20);
            if(out.getXcoordinate()> in.getXcoordinate() || out.getYcoordinate() > in.getYcoordinate())
            {
                output.setLatitude(input.get(i).getLatitude());
                output.setLongitude(input.get(i).getLongitude());
            }
        }
//        System.out.println("Max  "+output.getLatitude()+","+output.getLongitude());
        return output;
    }
    
     public static LatitudeAndLongitude MinimumLatitudeAndLongiutde(ArrayList<LatitudeAndLongitude> input)
    {
        LatitudeAndLongitude output= new LatitudeAndLongitude(input.get(0));
        TileCoord2D out = MapPoint.LatLongToPixelXY(output.getLatitude(), output.getLongitude(), 20);
        for(int i=0; i<input.size();++i)
        {
            TileCoord2D in = MapPoint.LatLongToPixelXY(input.get(i).getLatitude(), input.get(i).getLongitude(), 20);
            if(out.getXcoordinate() < in.getXcoordinate() || out.getYcoordinate()< in.getYcoordinate())
            {
                output.setLatitude(input.get(i).getLatitude());
                output.setLongitude(input.get(i).getLongitude());
            }
        }
//        System.out.println("Min  "+output.getLatitude()+","+output.getLongitude());
        return output;
    }
}
