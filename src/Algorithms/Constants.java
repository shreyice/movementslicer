/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Algorithms;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author Shrey
 */
public class Constants {

    public static int DISTANCE_THRESHOLD = 200; // in meteres
    public static long MIN_TIME_THRESHOLD = 60 * 40 * 1000; // in milliseconds
    public static int DISTANCE_BETWEEN_MEETING_POINTS = 200;
//    public static  int DISTANCE_THRESHOLD = 500; // in meteres
//    public static long MIN_TIME_THRESHOLD = 60*5*1000; // in milliseconds
//    public static int DISTANCE_BETWEEN_MEETING_POINTS = 500;
    public static int LEVEL_OF_DETAIL = 17;
    public static int BAR_WIDTH = 37;
    public static double TEXT_SIZE = 20;
    public static int MAX_ROWS_ALLOWED = 1;
    public static int ANGLE_OF_INCLINE_TICKS = 45;


    public static Color[] PEOPLE_COLOR = {
            new Color(0, 0, 255)
            , new Color(0, 255, 0)
            , new Color(255, 255, 64)
            , new Color(255, 0, 0)
            , new Color(255, 0, 255)
            , new Color(0, 255, 255)

    };

    public static String[] PEOPLE_NAMES = {
            "Alice"
            , "Bob"
            , "Carol"
            , "Dan"
            , "Eve"
            , "Fred"

            , "Grace"
            , "Harry"
            , "Ivy"
            , "John"
            , "Katy"


            , "Larry"
            , "Marry"
            , "Noah"
            , "Olga"
            , "Peter"
    };


    // matrix colors
    public static Color ROLLOVER_HIGHLIGHT_MEETING = new Color(204, 204, 204);
    public static Color ROLLOVER_HIGHLIGHT_PEOPLE = new Color(204, 204, 204);
    public static Color SELECTED_HIGHLIGHT_PEOPLE = new Color(255, 255, 255);
    public static Color SELECTED_HIGHLIGHT_MEETING = new Color(255, 255, 255);
    public static Color BACKGROUND_COLOR = new Color(153, 153, 153);


    //Meeting Colors on Gantt Chart
    public static Color MEETING_COLOR = new Color(121, 121, 121);

    // map saturation
    public static Color MAP_SATURATION_VALUE = new Color(255,255,255, 135);

    // map points size
    public static int MAP_LOCATION_RADIUS = 20;
    public static int MAP_TEXT = 15;

    //map Point Color
    public static Color MAP_POINTS_COLOR = new Color(234, 130, 8);

    public static String DATE_PATTERN = "yyyy MMM dd EEE HH:mm";
    //    public static String TIMEZONE_TO_DISPLAY = "UTC";
    public static String TIMEZONE_TO_DISPLAY = "Canada/Eastern";
    public static int PADDING_HORIZONTAL_LEFT = 150;
    public static int PADDING_HORIZONTAL_RIGHT = 150;
    public static int PADDING_VERTICAL = 70;
    public static int PADDING_MATRIX = 70;
    public static int PADDING = 50;
    public static boolean VERBOSE = false;

    public static boolean includeWeightsInBaryCentric = false;
    public static long FOLD_SIZE = 30; //number of Pixels
    public static Color X_FRAME_COLOR = new Color(0, 0, 0);

    public static final int NUM_FRAMES_PER_SECOND = 10;
    public static final double DURATION_OF_ANIMATION = 2.0; // in seconds

    public static boolean PIXEL_TYPE_RULER = false;


    public static int offsetForStringBoxFromCenter = 40;
    public static int offsetForStringBoxforLine = 20;

    public static int heightOfHoverNames = 40;
    public static int widthOfHoverNames = 40;
    public static int spacingHover = 10;

    public  static boolean leftHover = true;

    public  static boolean matrixNumbers = false;


    public static ArrayList<Integer> orderOfNode = new ArrayList<>();
}
