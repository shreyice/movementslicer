/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Algorithms;

import com.google.common.collect.Sets;
import org.joda.time.DateTime;
import org.joda.time.Interval;

import java.util.*;

/**
 * @author Shrey
 */
public class Group {

    List<Person> personList;
    List<MeetingPoint>[][] meetingPoints;
    Node[][] meet;
    MinMax minMax;
    Map<Integer, ArrayList<Integer>> network;
    ArrayList<StayRegion> CommonStayRegionList = new ArrayList<>();

    List<MeetingPoint> multiplepeople;

    ArrayList<String> combinations;


    public Group(ArrayList<Person> personList) {
        this.personList = personList;
        meet = new Node[this.personList.size()][this.personList.size()];
        for (int i = 0; i < this.personList.size(); i++) {
            for (int j = 0; j < this.personList.size(); j++) {
                this.meet[i][j] = new Node(false, 0);
            }
        }
        this.CommonStayRegionList = stayRegionClusteringBasedOnCommonStayRegions();
//        this.meetingPoints = groupingAlgorithm();
        this.meetingPoints = groupingAlgorithmCommonStayRegions();
        this.multiplepeople = groupingMultiplePeopleMeeting();

        this.minMax = new MinMax();
        this.minMax.calculate(meet);
        network = new HashMap<>();
        buildNetwork();
        System.out.println("Minimum " + this.minMax.min);
        System.out.println("Maximum " + this.minMax.max);
    }

    public void buildNetwork() {
        for (int i = 0; i < this.personList.size(); i++) {
            network.put(i, new ArrayList<Integer>());
            for (int j = 0; j < this.personList.size(); j++) {
                if (meet[i][j].getNumberOfMeetings() != 0) network.get(i).add(j);
            }
        }
    }

    public List<MeetingPoint>[][] groupingAlgorithm() {
//      System.out.println();
        this.meetingPoints = (ArrayList<MeetingPoint>[][]) new ArrayList[this.personList.size()][this.personList.size()];
        for (int i = 0; i < personList.size(); ++i) {
            ArrayList<StayPoint> tempi = new ArrayList<>(personList.get(i).getStayPoint().values());
            for (int j = i + 1; j < personList.size(); j++) {
                ArrayList<StayPoint> tempj = new ArrayList<>(personList.get(j).getStayPoint().values());
                for (int k = 0; k < tempi.size(); ++k) {
                    Interval first = new Interval(tempi.get(k).getArrivalDateTime(), tempi.get(k).getDepartureDateTime());
                    for (int m = 0; m < tempj.size(); ++m) {
                        double dist = LatitudeAndLongitudeUtils.distance(tempi.get(k).getStayPointCenter(), tempj.get(m).getStayPointCenter());
//                            System.out.println(dist + "  " + personList.get(i).getStayPoint().get(temp.get(k)).getStayPointCenter().getLatitude() + "," + personList.get(i).getStayPoint().get(temp.get(k)).getStayPointCenter().getLongitude() + "    " + tempmap.get(m).getStayPointCenter().getLatitude() + "," + tempmap.get(m).getStayPointCenter().getLongitude());
                        if (dist < Constants.DISTANCE_BETWEEN_MEETING_POINTS) {
                            Interval second = new Interval(tempj.get(m).getArrivalDateTime(), tempj.get(m).getDepartureDateTime());
                            if (first.overlap(second) != null) {
                                if (this.meetingPoints[tempi.get(k).getPersonId()][tempj.get(m).getPersonId()] == null
                                        )
                                    this.meetingPoints[tempi.get(k).getPersonId()][tempj.get(m).getPersonId()] =
                                            new ArrayList<>();
                                if (this.meetingPoints[tempj.get(m).getPersonId()][tempi.get(k).getPersonId()] == null
                                        )
                                    this.meetingPoints[tempj.get(m).getPersonId()][tempi.get(k).getPersonId()] =
                                            new ArrayList<>();
                                MeetingPoint meetingPoint1 = new MeetingPoint();
                                meetingPoint1.addPerson(tempi.get(k).getPersonId());
                                meetingPoint1.addPerson(tempj.get(m).getPersonId());
                                meetingPoint1.addStayPoint(tempi.get(k));
                                meetingPoint1.addStayPoint(tempj.get(m));
                                meetingPoint1.setInterval(first.overlap(second));
                                this.meetingPoints[tempi.get(k).getPersonId()][tempj.get(m).getPersonId()]
                                        .add(meetingPoint1);
                                MeetingPoint meetingPoint2 = new MeetingPoint();
                                meetingPoint2.addPerson(tempj.get(m).getPersonId());
                                meetingPoint2.addPerson(tempi.get(k).getPersonId());
                                meetingPoint2.addStayPoint(tempj.get(m));
                                meetingPoint2.addStayPoint(tempi.get(k));
                                meetingPoint2.setInterval(first.overlap(second));


                                this.meetingPoints[tempj.get(m).getPersonId()][tempi.get(k).getPersonId()]
                                        .add(meetingPoint2);
//                                this.meetingPoints[tempj.get(m).getPersonId()][tempi.get(k).getPersonId()]
//                                        .add(new MeetingPoint(tempj.get(m).getPersonId()
//                                                , tempi.get(k).getPersonId()
//                                                , tempj.get(m)
//                                                , tempi.get(k)
//                                                , first.overlap(second)));
//                                System.out.println(tempi.get(k).getPersonId()+" "+tempj.get(m).getPersonId());
                                this.meet[tempj.get(m).getPersonId()][tempi.get(k).getPersonId()].incrementMeetings();
                                this.meet[tempi.get(k).getPersonId()][tempj.get(m).getPersonId()].incrementMeetings();
                            }
                        }
                    }
                }
            }
        }
//      System.out.println();
        return this.meetingPoints;
    }

    public ArrayList<StayRegion> stayRegionClusteringBasedOnCommonStayRegions() {
        int id = 0;
        int count = 0;
        ArrayList<StayPoint> stayPointsList = new ArrayList<StayPoint>();
        for (int i = 0; i < this.personList.size(); i++) {
            stayPointsList.addAll(this.personList.get(i).getStayPointinArrayList(i));
        }
        ArrayList<StayRegion> out = new ArrayList<StayRegion>();
        ArrayList<Grid> g = new ArrayList<Grid>();
        for (int i = 0; i < stayPointsList.size(); i++) {
            TileCoord2D pnt = MapPoint.LatLongToPixelXY(stayPointsList.get(i).getStayPointCenter().getLatitude(),
                    stayPointsList.get(i).getStayPointCenter().getLongitude(), Constants.LEVEL_OF_DETAIL);
            pnt = MapPoint.PixelXYToTileXY(pnt.getXcoordinate(), pnt.getYcoordinate());
//                System.out.println(stayPointsList.get(i)..getStayPointCenter().getLatitude()+","+stayPointsList.get(i)..getStayPointCenter().getLongitude());
            if (g.size() == 0) {
                Grid tempGrid = new Grid();
                tempGrid.x = pnt.getXcoordinate();
                tempGrid.y = pnt.getYcoordinate();
                if (tempGrid.personWiseStayPointList.containsKey(stayPointsList.get(i).getPersonId()))
                    tempGrid.personWiseStayPointList.get(stayPointsList.get(i).getPersonId()).put(stayPointsList.get(i).getArrivalDateTime(), stayPointsList.get(i));
                else {
                    tempGrid.personWiseStayPointList.put(stayPointsList.get(i).getPersonId(), new TreeMap<DateTime, StayPoint>());
                    tempGrid.personWiseStayPointList.get(stayPointsList.get(i).getPersonId()).put(stayPointsList.get(i).getArrivalDateTime(), stayPointsList.get(i));
                }
                tempGrid.regionid = -1;
                g.add(tempGrid);
            } else {
                boolean flag = true;
                for (int k = 0; k < g.size(); ++k) {
                    if (g.get(k).x == pnt.getXcoordinate() && g.get(k).y == pnt.getYcoordinate()) {
                        if (g.get(k).personWiseStayPointList.containsKey(stayPointsList.get(i).getPersonId())) {
                            g.get(k).personWiseStayPointList.get(stayPointsList.get(i).getPersonId()).put(stayPointsList.get(i).getArrivalDateTime(), stayPointsList.get(i));
                        } else {
                            g.get(k).personWiseStayPointList.put(stayPointsList.get(i).getPersonId(), new TreeMap<DateTime, StayPoint>());
                            g.get(k).personWiseStayPointList.get(stayPointsList.get(i).getPersonId()).put(stayPointsList.get(i).getArrivalDateTime(), stayPointsList.get(i));
                        }
                        k++;
                        flag = false;
                    }
                }
                if (flag) {
                    Grid tempGrid = new Grid();
                    tempGrid.x = pnt.getXcoordinate();
                    tempGrid.y = pnt.getYcoordinate();
                    if (tempGrid.personWiseStayPointList.containsKey(stayPointsList.get(i).getPersonId())) {
                        tempGrid.personWiseStayPointList.get(stayPointsList.get(i).getPersonId()).put(
                                stayPointsList.get(i).getArrivalDateTime(), stayPointsList.get(i));
                    } else {
                        tempGrid.personWiseStayPointList.put(stayPointsList.get(i).getPersonId(), new TreeMap<DateTime, StayPoint>());
                        tempGrid.personWiseStayPointList.get(stayPointsList.get(i).getPersonId()).put(stayPointsList.get(i).getArrivalDateTime()
                                , stayPointsList.get(i));
                    }
                    tempGrid.regionid = -1;
                    g.add(tempGrid);
                }
            }
        }
        while (Grid.checkIfAnyGridUnsed(g)) {
            Grid temp = Grid.gridWithMaxStayPointsAndNoRegionAssignedPersonWiseList(g);
            Grid enlargedGrid = Grid.getNeighbourGridPersonWise(temp, g, count);
//            grid enlargedGrid = temp;
            StayRegion s = new StayRegion();
            s.setStayRegionCenter(StayPoint.meanFromStayPoints(new ArrayList<StayPoint>(enlargedGrid.listOfStayPointsFromHashMaps())));
            s.setId(id);
            ArrayList<StayPoint> tempList = new ArrayList<>(enlargedGrid.listOfStayPointsFromHashMaps());
            for (int i = 0; i < enlargedGrid.listOfStayPointsFromHashMaps().size(); i++) {
                tempList.get(i).setCommonStayRegionId(id);
            }
            s.setPersonWiseListOfStayPoints(enlargedGrid.getPersonWiseStayPointList());
            id = id + 1;
            temp.regionid = count;
            out.add(s);
            count++;
        }
        out.trimToSize();
        return out;
    }

    public List<MeetingPoint>[][] groupingAlgorithmCommonStayRegions() {
        this.meetingPoints = (ArrayList<MeetingPoint>[][]) new ArrayList[this.personList.size()][this.personList.size()];

        for (StayRegion temp : this.CommonStayRegionList) {
            int ar[] = new int[temp.getPersonWiseListOfStayPoints().size()];
            int count = 0;
            for (int i : temp.getPersonWiseListOfStayPoints().keySet()) {
                ar[count++] = i;
            }
            for (int i = 0; i < ar.length - 1; ++i) {
                ArrayList<StayPoint> person1 = new ArrayList<>(temp.getStayPointTreeMapOfPerson(ar[i]).values());
                for (int j = i + 1; j < ar.length; j++) {
                    ArrayList<StayPoint> person2 = new ArrayList<>(temp.getStayPointTreeMapOfPerson(ar[j]).values());
                    for (int k = 0; k < person1.size(); ++k) {
                        Interval first = new Interval(person1.get(k).getArrivalDateTime()
                                , person1.get(k).getDepartureDateTime());
                        for (int l = 0; l < person2.size(); l++) {
                            Interval second = new Interval(person2.get(l).getArrivalDateTime()
                                    , person2.get(l).getDepartureDateTime());

                            if (first.overlaps(second)) {
                                if (this.meetingPoints[person1.get(k).getPersonId()][person2.get(l).getPersonId()] == null
                                        )
                                    this.meetingPoints[person1.get(k).getPersonId()][person2.get(l).getPersonId()] =
                                            new ArrayList<>();
                                if (this.meetingPoints[person2.get(l).getPersonId()][person1.get(k).getPersonId()] == null
                                        )
                                    this.meetingPoints[person2.get(l).getPersonId()][person1.get(k).getPersonId()] =
                                            new ArrayList<>();
                                MeetingPoint meetingPoint1 = new MeetingPoint();
                                meetingPoint1.addPerson(person1.get(k).getPersonId());
                                meetingPoint1.addPerson(person2.get(l).getPersonId());
                                meetingPoint1.addStayPoint(person1.get(k));
                                meetingPoint1.addStayPoint(person2.get(l));
                                meetingPoint1.setInterval(first.overlap(second));
                                meetingPoint1.calculateCenter();
                                this.meetingPoints[person1.get(k).getPersonId()][person2.get(l).getPersonId()]
                                        .add(meetingPoint1);
                                meetingPoint1.setStayRegionId(person1.get(k).getCommonStayRegionId());
                                MeetingPoint meetingPoint2 = new MeetingPoint();
                                meetingPoint2.addPerson(person2.get(l).getPersonId());
                                meetingPoint2.addPerson(person1.get(k).getPersonId());
                                meetingPoint2.addStayPoint(person2.get(l));
                                meetingPoint2.addStayPoint(person1.get(k));
                                meetingPoint2.setInterval(first.overlap(second));
                                meetingPoint2.setStayRegionId(person2.get(l).getCommonStayRegionId());
                                meetingPoint2.calculateCenter();


                                this.meetingPoints[person2.get(l).getPersonId()][person1.get(k).getPersonId()]
                                        .add(meetingPoint2);
//                                this.meetingPoints[tempj.get(m).getPersonId()][tempi.get(k).getPersonId()]
//                                        .add(new MeetingPoint(tempj.get(m).getPersonId()
//                                                , tempi.get(k).getPersonId()
//                                                , tempj.get(m)
//                                                , tempi.get(k)
//                                                , first.overlap(second)));
//                                System.out.print(person1.get(k).getStayPointCenter().getLatitude()
//                                        + "," + person1.get(k).getStayPointCenter().getLongitude());
//                                System.out.println("  "+person2.get(l).getStayPointCenter().getLatitude()
//                                        + "," + person2.get(l).getStayPointCenter().getLongitude()+"   "
//                                + LatitudeAndLongitudeUtils.distance(person1.get(k).getStayPointCenter(), person2.get(l).getStayPointCenter()));
                                this.meet[person2.get(l).getPersonId()][person1.get(k).getPersonId()].incrementMeetings();
                                this.meet[person1.get(k).getPersonId()][person2.get(l).getPersonId()].incrementMeetings();

                            }
                        }
                    }
                }
            }
        }
        return this.meetingPoints;
    }


    public List<MeetingPoint> groupingMultiplePeopleMeeting() {
        this.multiplepeople = new ArrayList<>();

        for (StayRegion temp : this.CommonStayRegionList) {

            ArrayList<StayPoint> currentlyUsed = new ArrayList<>();

            int ar[] = new int[temp.getPersonWiseListOfStayPoints().size()];
            int count = 0;

            for (int i : temp.getPersonWiseListOfStayPoints().keySet()) {
                ar[count++] = i;
            }
            if (ar.length == 1) continue;
            ArrayList<DateTime> arrival = arrivalTime(temp);
            ArrayList<DateTime> departure = departureTime(temp);
            ArrayList<StayPoint> arrivalSP = arrivalStayPoint(temp);
            ArrayList<StayPoint> departureSP = departureStayPoint(temp);
            int asp = 0;
            int dsp = 0;

            DateTime start = arrival.get(0);
            DateTime end = departure.get(departure.size() - 1);

            currentlyUsed.add(arrivalSP.get(0));
            asp++;
            while (start.getMillis() < end.getMillis()) {
                try {
                    if (arrival.get(asp).getMillis() < departure.get(dsp).getMillis()) {
                        start = arrival.get(asp);
                        currentlyUsed.add(arrivalSP.get(asp));
                        asp++;
                    } else if (arrival.get(asp).getMillis() > departure.get(dsp).getMillis()) {
                        String com = "";
                        ArrayList<Integer> sort = new ArrayList<>();
                        for (int i = 0; i < currentlyUsed.size(); i++) {
                            sort.add(currentlyUsed.get(i).getPersonId());
                        }
                        Collections.sort(sort);

                        Set<Set<Integer>> list = Sets.powerSet(Sets.newHashSet(sort));

                        StayPoint remove;

                        for (Set<Integer> current:list) {
                            if(current.size() == 0)continue;
                            sort = new ArrayList<>();
                            for (int i : current) {
                                sort.add(i);
                            }
                            Collections.sort(sort);
                            MeetingPoint meetingPoint = createMeetingPoint(currentlyUsed, temp.getId(), sort);

                            if (meetingPoint.getStayPoints().size() > 1) {
                                if (!this.multiplepeople.contains(meetingPoint))
                                    this.multiplepeople.add(meetingPoint);
                            }
                        }

                        ListIterator<StayPoint> listIterator = currentlyUsed.listIterator();

                        while (listIterator.hasNext()) {
                            StayPoint stayPoint =  listIterator.next();
                            if (stayPoint.equals(departureSP.get(dsp))) {
                                remove = stayPoint;
                                listIterator.remove();

                            }



                        }

//                        for (int i = 0; i < currentlyUsed.size(); i++) {
//                            if (currentlyUsed.get(i).equals(departureSP.get(dsp))) {
//                                remove = currentlyUsed.get(i);
//                                currentlyUsed.remove(i);
//                            }
//                        }
                        start = departure.get(dsp);
                        dsp++;
                    } else if (arrival.get(asp).getMillis() == departure.get(dsp).getMillis()) {
                        ArrayList<Integer> sort = new ArrayList<>();
                        for (int i = 0; i < currentlyUsed.size(); i++) {
                            sort.add(currentlyUsed.get(i).getPersonId());
                        }
                        Collections.sort(sort);
                        Set<Set<Integer>> list = Sets.powerSet(Sets.newHashSet(sort));
                        StayPoint remove;

                        for (Set<Integer> current:list) {
                            if(current.size() == 0)continue;


                            sort = new ArrayList<>();
                            for (int i : current) {
                                sort.add(i);
                            }
                            Collections.sort(sort);
                            MeetingPoint meetingPoint = createMeetingPoint(currentlyUsed, temp.getId(), sort);

                            if (meetingPoint.getStayPoints().size() > 1) {

                                if(!this.multiplepeople.contains(meetingPoint))
                                this.multiplepeople.add(meetingPoint);

                            }
                        }


                        ListIterator<StayPoint> listIterator = currentlyUsed.listIterator();

                        while (listIterator.hasNext()) {
                            StayPoint stayPoint =  listIterator.next();
                            if (stayPoint.equals(departureSP.get(dsp))) {
                                remove = stayPoint;
                                listIterator.remove();

                            }
                        }

//                        for (int i = 0; i < currentlyUsed.size(); i++) {
//                            if (currentlyUsed.get(i).equals(departureSP.get(dsp
//                            ))) {
//                                currentlyUsed.remove(i);
//                            }
//                        }
                        start = departure.get(dsp);
                        dsp++;
                        start = arrival.get(asp);
                        currentlyUsed.add(arrivalSP.get(asp));
                        asp++;
                    }
                } catch (Exception e
                        ) {
                    ArrayList<Integer> sort = new ArrayList<>();
                    for (int i = 0; i < currentlyUsed.size(); i++) {
                        sort.add(currentlyUsed.get(i).getPersonId());
                    }
                    Collections.sort(sort);
                    StayPoint remove;

                    Set<Set<Integer>> list = Sets.powerSet(Sets.newHashSet(sort));

                    for (Set<Integer> current:list) {

                        if(current.size() == 0)continue;
                        sort = new ArrayList<>();
                        for (int i : current) {
                            sort.add(i);
                        }

                        Collections.sort(sort);
                        MeetingPoint meetingPoint = createMeetingPoint(currentlyUsed, temp.getId(), sort);

                        if (meetingPoint.getStayPoints().size() > 1) {
                            if(!this.multiplepeople.contains(meetingPoint))
                                this.multiplepeople.add(meetingPoint);

                        }
                    }


                    ListIterator<StayPoint> listIterator = currentlyUsed.listIterator();

                    while (listIterator.hasNext()) {
                        StayPoint stayPoint =  listIterator.next();
                        if (stayPoint.equals(departureSP.get(dsp))) {
                            remove = stayPoint;
                            listIterator.remove();

                        }
                    }

//                    for (int i = 0; i < currentlyUsed.size(); i++) {
//                        if (currentlyUsed.get(i).equals(departureSP.get(dsp
//                        ))) {
//                            currentlyUsed.remove(i);
//                        }
//                    }
                    start = departure.get(dsp);
                    dsp++;
                }
            }
        }
        return this.multiplepeople;
    }

    void combine(String instr, StringBuffer outstr, int index) {
        for (int i = index; i < instr.length(); i++) {
            outstr.append(instr.charAt(i));
            this.combinations.add(outstr.toString());
            combine(instr, outstr, i + 1);
            outstr.deleteCharAt(outstr.length() - 1);
        }
    }



    ArrayList<Integer> stringToList(String s) {
        ArrayList<Integer> list = new ArrayList<>();

        for (int i = 0; i < s.length(); i++) {
            list.add(Integer.parseInt(s.substring(i, i + 1)));
        }
        return list;
    }

    String listToString(ArrayList<Integer> list ) {

        Collections.sort(list);
        String s ="";
        for (int i = 0; i < list.size(); i++) {
            s += Integer.toString(list.get(i));
        }
        return s;
    }

    MeetingPoint createMeetingPoint(ArrayList<StayPoint> current, int stayRegion, ArrayList<Integer> list) {

        Collections.sort(list);

        ArrayList<StayPoint> currentlyUsed = new ArrayList<>();

        for (int i = 0; i < current.size(); i++) {
            if (list.contains(current.get(i).getPersonId()))
                currentlyUsed.add(current.get(i));
        }

        MeetingPoint meetingPoint = new MeetingPoint();
        ArrayList<Integer> person = new ArrayList();
        ArrayList<StayPoint> sp = new ArrayList<>();
        Interval interval = new Interval(currentlyUsed.get(0).getArrivalDateTime(), currentlyUsed.get(0).getDepartureDateTime());

        for (int i = 0; i < currentlyUsed.size(); i++) {
            Interval inter = new Interval(currentlyUsed.get(i).getArrivalDateTime(), currentlyUsed.get(i).getDepartureDateTime());
            if (interval.overlap(inter) == null) {
                System.out.println("hold");
            }  else
            interval = interval.overlap(inter);
            sp.add(currentlyUsed.get(i));
            person.add(currentlyUsed.get(i).getPersonId());
        }
        Collections.sort(person);
//        System.out.println(person);

        sp = sortStayPointPerson(sp);
        meetingPoint.setPersonId(person);
        meetingPoint.setStayPoints(sp);
        meetingPoint.setStayRegionId(stayRegion);
        meetingPoint.setInterval(interval);
        meetingPoint.setCenter(CommonStayRegionList.get(stayRegion).getStayRegionCenter());
        meetingPoint.setName("L " + Integer.toString(stayRegion));
        return meetingPoint;

    }

    ArrayList<StayPoint> sortStayPointPerson(ArrayList<StayPoint> stayPoints) {

        for (int i = 0; i < stayPoints.size(); ++i) {
            for (int j = 0; j < stayPoints.size(); ++j) {
                if (stayPoints.get(i).getPersonId() < stayPoints.get(j).getPersonId()) {
                    StayPoint tempo = stayPoints.get(j);
                    stayPoints.set(j, stayPoints.get(i));
                    stayPoints.set(i, tempo);
                }
            }
        }
        return stayPoints;
    }


    ArrayList<StayPoint> arrivalStayPoint(StayRegion stayRegion) {

        int ar[] = new int[stayRegion.getPersonWiseListOfStayPoints().size()];

        int count = 0;
        TreeMap<DateTime, ArrayList<StayPoint>> listMilli = new TreeMap<>();
        for (int i : stayRegion.getPersonWiseListOfStayPoints().keySet()) {
            TreeMap<DateTime, StayPoint> list = stayRegion.getStayPointTreeMapOfPerson(i);
            for (DateTime date : list.keySet()) {
                if (listMilli.containsKey(date)) {
                    listMilli.get(date).add(list.get(date));
                }
                else {
                    ArrayList<StayPoint> tempStay = new ArrayList<>();
                    tempStay.add(list.get(date));
                    listMilli.put(date, tempStay);
                }
            }
//            listMilli.putAll(temp.getStayPointTreeMapOfPerson(i));
        }

        ArrayList<StayPoint> temp = new ArrayList<>();
        for (DateTime dateTime : listMilli.keySet()) {
            temp.addAll(listMilli.get(dateTime));
        }

        return temp;

    }

    ArrayList<DateTime> arrivalTime(StayRegion temp) {

        int ar[] = new int[temp.getPersonWiseListOfStayPoints().size()];
        int count = 0;
        ArrayList<DateTime> listMilli = new ArrayList<>();
        for (int i : temp.getPersonWiseListOfStayPoints().keySet()) {
            listMilli.addAll(new ArrayList<>(temp.getStayPointTreeMapOfPerson(i).keySet()));
        }
        Collections.sort(listMilli);
        return listMilli;

    }

    ArrayList<StayPoint> departureStayPoint(StayRegion stayRegion) {

        int ar[] = new int[stayRegion.getPersonWiseListOfStayPoints().size()];
        int count = 0;
        TreeMap<DateTime, ArrayList<StayPoint>> listMilli = new TreeMap<>();

        for (int i : stayRegion.getPersonWiseListOfStayPoints().keySet()) {
            TreeMap<DateTime, StayPoint> list = stayRegion.getStayPointTreeMapOfPerson(i);
            for (DateTime date : list.keySet()) {
                if (listMilli.containsKey(list.get(date).getDepartureDateTime())) {
                    listMilli.get(list.get(date).getDepartureDateTime()).add(list.get(date));
                }
                else {
                    ArrayList<StayPoint> tempStay = new ArrayList<>();
                    tempStay.add(list.get(date));
                    listMilli.put(stayRegion.getStayPointTreeMapOfPerson(i).get(date).getDepartureDateTime(), tempStay);
                }
            }
        }


        ArrayList<StayPoint> temp = new ArrayList<>();
        for (DateTime dateTime : listMilli.keySet()) {
            temp.addAll(listMilli.get(dateTime));
        }


        return temp;

    }

    ArrayList<DateTime> departureTime(StayRegion temp) {

        int ar[] = new int[temp.getPersonWiseListOfStayPoints().size()];
        int count = 0;
        ArrayList<DateTime> listMilli = new ArrayList<>();
        for (int i : temp.getPersonWiseListOfStayPoints().keySet()) {

            for (DateTime dt : temp.getStayPointTreeMapOfPerson(i).keySet())
                listMilli.add(temp.getStayPointTreeMapOfPerson(i).get(dt).getDepartureDateTime());
        }
        Collections.sort(listMilli);
        return listMilli;

    }


    public void displayMatrix() {
        for (int i = 0; i < this.personList.size(); i++) {
            for (int j = 0; j < this.personList.size(); j++) {
                System.out.print(meet[i][j] + "  ");
            }
            System.out.println();
        }
    }

    public int noOfPeopleInGroup() {
        return this.personList.size();
    }

    public ArrayList<Integer> getNeighboursOfNode(int node) {
        return this.network.get(node);
    }

    public Node[][] getMeet() {
        return meet;
    }

    public int getNoOfMeetingsBetweenPeople(int i, int j) {
        return meet[i][j].getNumberOfMeetings();
    }

    public MinMax getMinMax() {
        return minMax;
    }

    public Person getPerson(int no) {
        return this.personList.get(no);
    }

    public List<MeetingPoint> getMeetings(int person1, int person2) {
        return this.meetingPoints[person1][person2];
    }

    /*
    *   list of uniqueStayRegions Visited by poeple
     */

    public ArrayList<Integer> listOfUniqueStayRegions(ArrayList<Integer> person) {
        ArrayList<Integer> uniqueStayRegion = new ArrayList<>();
        for (int i = 0; i < person.size() - 1; i++) {
            for (int j = i + 1; j < person.size(); j++) {
                for (MeetingPoint meet : getMeetings(person.get(i), person.get(j))) {
                    if (!uniqueStayRegion.contains(meet.getStayPoints().get(0).getCommonStayRegionId()))
                        uniqueStayRegion.add(meet.getStayPoints().get(1).getCommonStayRegionId());
                }
            }

        }
        return uniqueStayRegion;
    }


    public DateTime getMeetingStartBetweenTwoPeople(int person1, int person2) {
        DateTime temp = this.meetingPoints[person1][person2].get(0).startDateTime();
        for (int i = 0; i < this.meetingPoints[person1][person2].size(); i++) {
            if (temp.isAfter(this.meetingPoints[person1][person2].get(i).startDateTime())) {
                temp = this.meetingPoints[person1][person2].get(i).startDateTime();
            }
        }
        return temp;

    }

    public DateTime getMeetingEndBetweenTwoPeople(int person1, int person2) {
        DateTime temp = this.meetingPoints[person1][person2].get(0).endDateTime();
        for (int i = 0; i < this.meetingPoints[person1][person2].size(); i++) {
            if (temp.isBefore(this.meetingPoints[person1][person2].get(i).endDateTime())) {
                temp = this.meetingPoints[person1][person2].get(i).endDateTime();
            }
        }
        return temp;
    }


    public List<MeetingPoint> getMeetingsPeople(ArrayList<Integer> list) {

        Collections.sort(list);
        List<MeetingPoint> meet = new ArrayList<>();
        for (MeetingPoint meetingPoint : multiplepeople) {
            ArrayList<Integer> list2 = meetingPoint.getPersonId();
            if (list.equals(list2)) {
                meet.add(meetingPoint);
            }
        }
        return meet;
    }

    public List<Person> getPersonList() {
        return personList;
    }

    public List<MeetingPoint> getMultiplepeople() {
        return multiplepeople;
    }

    public void setMultiplePeople(List<MeetingPoint> multiplepeople) {
        this.multiplepeople = multiplepeople;
    }

    public ArrayList<StayRegion> getCommonStayRegionList() {
        return CommonStayRegionList;
    }

    public LatitudeAndLongitude getCommonStayRegionCentreFromId(int id) {
        return CommonStayRegionList.get(id).getStayRegionCenter();
    }

}


