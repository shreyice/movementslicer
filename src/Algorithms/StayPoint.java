/**
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package Algorithms;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Shrey
 */
public class StayPoint {

    private LatitudeAndLongitude stayPointCenter;
    private List<GPSLocation> gpsLocations;
    private DateTime arrivalDateTime;
    private DateTime departureDateTime;
    private int personId;
    private int stayPointId;
    private int stayRegionId;
    private int commonStayRegionId;
    
    public StayPoint() {
        this.stayPointCenter = null;
        this.gpsLocations = null;
        this.arrivalDateTime = null;
        this.departureDateTime = null;

    }
    public static LatitudeAndLongitude meanFromStayPoints(ArrayList<StayPoint> stayPointList) {
        
        ArrayList<LatitudeAndLongitude> temp = new ArrayList<LatitudeAndLongitude>();
        for(int i =0; i<stayPointList.size();i++)
        {
            temp.add(stayPointList.get(i).stayPointCenter);
        }
        
        return (LatitudeAndLongitudeUtils.meanFromLatitudeAndLongitude(temp));
    }


    // TODO check
    public static LatitudeAndLongitude polygonCenterFromStayPoints(ArrayList<StayPoint> stayPointList) {

        ArrayList<LatitudeAndLongitude> temp = new ArrayList<LatitudeAndLongitude>();
        for(int i =0; i<stayPointList.size();i++)
        {
            temp.add(stayPointList.get(i).stayPointCenter);
        }

        return (LatitudeAndLongitudeUtils.meanFromLatitudeAndLongitude(temp));
    }

    public LatitudeAndLongitude getStayPointCenter() {
        return this.stayPointCenter;
    }

    public List<GPSLocation> getGpsLocations() {
        return gpsLocations;
    }

    public DateTime getArrivalDateTime() {
        return arrivalDateTime;
    }

    public DateTime getDepartureDateTime() {
        return departureDateTime;
    }

    public void setStayPointCenter(LatitudeAndLongitude stayPointCenter) {
        this.stayPointCenter = stayPointCenter;
    }

    public void setGpsLocations(List<GPSLocation> gpsLocations) {
        this.gpsLocations = gpsLocations;
    }

    public void setArrivalDateTime(DateTime arrivalDateTime) {
        this.arrivalDateTime = arrivalDateTime;
    }

    public void setDepartureDateTime(DateTime departureDateTime) {
        this.departureDateTime = departureDateTime;
    }

    public int getPersonId() {
        return personId;
    }

    public void setPersonId(int personId) {
        this.personId = personId;
    }

    public int getStayPointId() {
        return stayPointId;
    }

    public void setStayPointId(int stayPointId) {
        this.stayPointId = stayPointId;
    }

    public int getStayRegionId() {
        return stayRegionId;
    }

    public void setStayRegionId(int stayRegionId) {
        this.stayRegionId = stayRegionId;
    }

    public int getCommonStayRegionId() {
        return commonStayRegionId;
    }

    public void setCommonStayRegionId(int commonStayRegionId) {
        this.commonStayRegionId = commonStayRegionId;
    }
}
