/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Algorithms;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.TreeMap;

/**
 *
 * @author Shrey
 */
public class DataRead{
    public static ArrayList<Person> loadData(File []dirs) {
        ArrayList<Person> persons= new ArrayList<>();
        int id = 0;
        for (File f : dirs) {
            persons.add(new Person(id++, f.getAbsolutePath() + "\\Trajectory"));
        }
        return persons;
    }

    public static TreeMap<DateTime,GPSLocation> loadTreeMapFromDir(File dir) {
        TreeMap<DateTime,GPSLocation> temp = new TreeMap<>();

        String outfile ;
        try {
            File[] files = dir.listFiles();
            for (File file : files) {
                if (file.isDirectory()) {
                    loadTreeMapFromDir(file);
                } else {
                    outfile = file.getCanonicalPath();
                    System.out.println(outfile);
//                    System.out.println(outfile.substring(outfile.lastIndexOf(".")));
                    if(outfile.substring(outfile.lastIndexOf(".")).equals(".kml"))
                        temp.putAll(loadTreeMapFromFileKML(outfile));
                    else if(outfile.substring(outfile.lastIndexOf(".")).equals(".plt"))
                        temp.putAll(loadTreeMapFromFilePLT(outfile));
                    else if (outfile.substring(outfile.lastIndexOf(".")).equals(".csv"))
                        temp.putAll(loadTreeMapFromFileCSV(outfile));
                    else if (outfile.substring(outfile.lastIndexOf(".")).equals(".txt"))
                        temp.putAll(loadTreeMapFromFileTXT(outfile));
                    else
//                        temp.putAll(loadTreeMapFromFile(outfile));
                        System.out.println(outfile + " Not recognized format");
                }
            }
        } catch (IOException e) {
            System.out.println("Dir not found");
//            e.printStackTrace();
        }
        return temp;
    }

    public static TreeMap<DateTime,GPSLocation> loadTreeMapFromFilePLT(String file){
            
           TreeMap<DateTime,GPSLocation> temp = new TreeMap<DateTime,GPSLocation>();
           try

           {
            BufferedReader k = new BufferedReader(new FileReader(file));
            int i = 0;
            while (i < 6) {
                k.readLine();
                i++;
            }
            String s = k.readLine();
            while (s != null) {
                String ar[];
                
                ar = s.split(",");
                GPSLocation templ = new GPSLocation();
                templ.setLatnlog(new LatitudeAndLongitude(Double.parseDouble(ar[0]), Double.parseDouble(ar[1])));
//                templ.setAltitude(Double.parseDouble(ar[3]));
                DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
                DateTime locationTime = formatter.withZone(DateTimeZone.UTC).parseDateTime(ar[5]+ " " +ar[6]);
//                System.out.println(locationTime);
                templ.setDateandtime(locationTime);
//                templ.setDate(ar[5]);
//                templ.setTime(ar[6]);
//                if(!temp.containsKey(locationTime))
                temp.put(locationTime, templ);
                s = k.readLine();
            }
            k.close();
        } catch (IOException e) {
            System.out.println("File not found");
        }

        return temp;
        
    }

    public static TreeMap<DateTime,GPSLocation> loadTreeMapFromFileCSV(String file){

        TreeMap<DateTime,GPSLocation> temp = new TreeMap<DateTime,GPSLocation>();
        int i = 1;
        try

        {
            BufferedReader k = new BufferedReader(new FileReader(file));
            String s = k.readLine();
            while (s != null) {
                String ar[];

                ar = s.split(",");
                GPSLocation templ = new GPSLocation();
                templ.setLatnlog(new LatitudeAndLongitude(Double.parseDouble(ar[5]), Double.parseDouble(ar[6])));
                //                templ.setAltitude(Double.parseDouble(ar[3]));
//                int day = Integer.parseInt(ar[7].substring(0, 2));
//                int month = Integer.parseInt(ar[7].substring(3, 5));
//                int yyyy = Integer.parseInt(ar[7].substring(6, 10));
//                int hour = Integer.parseInt(ar[7].substring(11, 13));
//                int min = Integer.parseInt(ar[7].substring(14, 16));
//                MutableDateTime dateTime = new MutableDateTime();
//                dateTime.setDayOfMonth(day);
//                dateTime.setMonthOfYear(month);
//                dateTime.setYear(yyyy);
//                dateTime.setHourOfDay(hour);
//                dateTime.setMinuteOfHour(min);
//                dateTime.setSecondOfDay(00);
//                dateTime.setMillisOfSecond(00);

                DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm");
                DateTime locationTime = formatter.withZone(DateTimeZone.UTC).parseDateTime(ar[7]);



//                System.out.println(locationTime);
                templ.setDateandtime(locationTime);
//                templ.setDate(ar[5]);
//                templ.setTime(ar[6]);
//                if(!temp.containsKey(locationTime))
                if(temp.containsKey(locationTime)){
//                    System.out.print(day + "    "+month+ "    "+yyyy+"    "+hour+"   "+min);
//                    System.out.print(ar[7] + "    ");
//
//                    System.out.print(s+"       "+i+"     ");
//                System.out.println(temp.get(locationTime).getDateandtime()+"        ");
//                System.out.println(temp.size());


                }

                temp.put(locationTime, templ);
    //                System.out.print("add       "+s+"       "+i+"     ");
    //
    //                System.out.println(temp.get(locationTime).getDateandtime()+"        ");

                s = k.readLine();
                i++;
            }
            k.close();
        } catch (IOException e) {
            System.out.println("File not found");
        }

        return temp;

    }



    public static TreeMap<DateTime,GPSLocation> loadTreeMapFromFileTXT(String file){

        TreeMap<DateTime,GPSLocation> temp = new TreeMap<DateTime,GPSLocation>();
        int i = 1;
        try

        {
            BufferedReader k = new BufferedReader(new FileReader(file));
            String s = k.readLine();
            while (s != null) {
                String ar[];

                ar = s.split(",");
                GPSLocation templ = new GPSLocation();
                templ.setLatnlog(new LatitudeAndLongitude(Double.parseDouble(ar[3]), Double.parseDouble(ar[2])));


                DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
                DateTime locationTime = formatter.withZone(DateTimeZone.UTC).parseDateTime(ar[1]);

//                System.out.println(locationTime);
                templ.setDateandtime(locationTime);
//                templ.setDate(ar[5]);
//                templ.setTime(ar[6]);
//                if(!temp.containsKey(locationTime))
                if(temp.containsKey(locationTime)){
//                    System.out.print(day + "    "+month+ "    "+yyyy+"    "+hour+"   "+min);
//                    System.out.print(ar[7] + "    ");

//                    System.out.print(s+"       "+i+"     ");
//                    System.out.println(temp.get(locationTime).getDateandtime()+"        ");
//                System.out.println(temp.size());


                }

                temp.put(locationTime, templ);
//                System.out.print("add       "+s+"       "+i+"     ");

//                System.out.println(temp.get(locationTime).getDateandtime()+"        ");

                s = k.readLine();
                i++;
            }
            k.close();
        } catch (IOException e) {
            System.out.println("File not found");
        }

        return temp;

    }

    public static TreeMap<DateTime,GPSLocation> loadTreeMapFromFileKML(String file){
        TreeMap<DateTime,GPSLocation> temp = new TreeMap<DateTime,GPSLocation>();
        try {

            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document doc = docBuilder.parse(new File(file));


            // normalize text representation
            doc.getDocumentElement().normalize();


            NodeList when= doc.getElementsByTagName("when");
            NodeList coord = doc.getElementsByTagName("gx:coord");
            int totalwhen = when.getLength();

            for(int s=0; s<when.getLength() ; s++){
                Node tempwhen = when.item(s);
                Node tempcoord = coord.item(s);
                String strwhen = tempwhen.getTextContent();
                String strcoord = tempcoord.getTextContent();
//                System.out.println(str);

                String arwhen = tempwhen.getTextContent();
                String arcoord[] = tempcoord.getTextContent().split(" ");
//                DateTimeFormatter formatter = DateTimeFormat.forPattern("YYYY-MM-DDThh:mm:ss");
                GPSLocation templ = new GPSLocation();
                templ.setLatnlog(new LatitudeAndLongitude(Double.parseDouble(arcoord[1]), Double.parseDouble(arcoord[0])));
//                templ.setAltitude(Double.parseDouble(ar[3]));
                DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
                DateTime locationTime = new DateTime(arwhen);
//                System.out.println(locationTime);
                templ.setDateandtime(locationTime);
//                templ.setDate(ar[5]);
//                templ.setTime(ar[6]);
//                if(!temp.containsKey(locationTime))
                temp.put(locationTime, templ);
//                        System.out.print("gjhghj");
//                System.out.println(arcoord[1]+";"+arcoord[    0]+";"+locationTime);
            }//end of for loop with s var


        }catch (SAXParseException err) {
            System.out.println ("** Parsing error" + ", line "
                    + err.getLineNumber () + ", uri " + err.getSystemId ());
            System.out.println(" " + err.getMessage ());

        }catch (SAXException e) {
            Exception x = e.getException ();
            ((x == null) ? e : x).printStackTrace ();

        }catch (Throwable t) {
            t.printStackTrace ();
        }
        return temp;
    }


    public static TreeMap<DateTime,GPSLocation> loadTreeMapFromFile(String file){

        TreeMap<DateTime,GPSLocation> temp = new TreeMap<DateTime,GPSLocation>();
        try {
//            System.out.println(file);
            BufferedReader k = new BufferedReader(new FileReader(file));
            int i = 0;
            int row_no = 0;
            String s = k.readLine();
            while (s != null) {
                String ar[];
                ar = s.split(" ");
//                System.out.println(ar[0]);
//                System.out.println(ar[1]);
//                System.out.println(ar[2]);
//                System.out.println(ar[3]);
                GPSLocation templ = new GPSLocation();
                templ.setLatnlog(new LatitudeAndLongitude(Double.parseDouble(ar[0]), Double.parseDouble(ar[1])));
//                System.out.println(ar[3]);
                //                templ.setAltitude(Double.parseDouble(ar[3]));


                DateTime locationTime = new DateTime(Integer.parseInt(ar[3])*1000L)  ;
//                System.out.println(locationTime);
                templ.setDateandtime(locationTime);
//                templ.setDate(ar[5]);
//                templ.setTime(ar[6]);
//                if(!temp.containsKey(locationTime))
                temp.put(locationTime, templ);
                s = k.readLine();
//                System.out.println(templ.getLatnlog().getLatitude()+"  "+templ.getLatnlog().getLongitude()+"  "+templ.getDateandtime().toString("yyyy-MM-dd HH:mm:ss"));
            }
            k.close();
        } catch (IOException e) {
            System.out.println("File not found");
        }
        return temp;
    }


    
    public static ArrayList<GPSLocation> loadArrayListFromFile(String file) {
        ArrayList<GPSLocation> temp = new ArrayList<GPSLocation>();
        
        try {
            BufferedReader k = new BufferedReader(new FileReader(file));
            int i = 0;
            int row_no = 0;
            while (i < 6) {
                k.readLine();
                i++;
            }
            String s = k.readLine();

            while (s != null) {
                String ar[];
                ar = s.split(",");
                GPSLocation templ = new GPSLocation();
                templ.setLatnlog(new LatitudeAndLongitude(Double.parseDouble(ar[0]), Double.parseDouble(ar[1])));
//                templ.setAltitude(Double.parseDouble(ar[3]));
                DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd hh:mm:ss");
                
                DateTime locationTime = formatter.parseDateTime(ar[5]+ " " +ar[6]);

                templ.setDateandtime(locationTime);

//                templ.setDate(ar[5]);
//                templ.setTime(ar[6]);
                temp.add(templ);
                s = k.readLine();
            }
            k.close();
        } catch (IOException e) {
            System.out.println("File not found");
        }

        return temp;
    }

    public static ArrayList<GPSLocation> loadArrayListFromDir(File dir) {
        ArrayList<GPSLocation> temp = new ArrayList<GPSLocation>();

        String outfile = "";
        try {
            File[] files = dir.listFiles();
            for (File file : files) {
                if (file.isDirectory()) {
                    loadArrayListFromDir(file);
                } else {
                    outfile = file.getCanonicalPath();
                    System.out.println("list = DataRead.loadFromDir(new File(" + file.getCanonicalPath() + "));");
                    temp.addAll(loadArrayListFromFile(outfile));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return temp;
    }


}