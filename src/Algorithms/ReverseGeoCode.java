/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Algorithms;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

/**
 *
 * @author Shrey
 */
public class ReverseGeoCode {
//    public static void main(String []args)
//    {
//        System.out.println(new ReverseGeoCode().getAddress(45.49693f,-73.801674f));
//    }



    public String getAddress(double lat, double log) {
        String latlong = Double.toString(lat)+","+Double.toString(log);
        String address = null;
        String gURL = "http://maps.google.com/maps/api/geocode/xml?latlng=" + latlong + "&sensor=true";
        try {
            DocumentBuilderFactory df = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = df.newDocumentBuilder();
            Document dom = db.parse(gURL);
            Element docEl = dom.getDocumentElement();
            NodeList nl = docEl.getElementsByTagName("result");
            
            if (nl != null && nl.getLength() > 0) {
                address = ((Element) nl.item(0)).getElementsByTagName("formatted_address").item(0).getTextContent();
                for (int i = 0; i < nl.getLength(); i++) {
                    String temp = ((Element) nl.item(i)).getElementsByTagName("formatted_address").item(0).getTextContent();
                }
            }
        } catch (Exception ex) {
            address = "Err";
        }
        return address;
    }

    
   
}
