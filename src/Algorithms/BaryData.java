package Algorithms;

/**
 * Created with IntelliJ IDEA.
 * User: Shrey
 * Date: 10/9/13
 * Time: 4:49 PM
 * To change this template use File | Settings | File Templates.
 */
public class BaryData {

        public int index;
        public double average;

        public BaryData(int index, double average) {
            this.index = index;
            this.average = average;
        }

        public BaryData(BaryData baryData) {
            this.index = baryData.index;
            this.average = baryData.average;
        }

    public static BaryData[] sort(BaryData[] reorderArray) {
        for (int i = 0; i < reorderArray.length; i++) {
            for (int j = i + 1; j < reorderArray.length; j++) {
                if (reorderArray[i].average > reorderArray[j].average) {
                    BaryData temp = new BaryData(reorderArray[i]);
                    reorderArray[i] = reorderArray[j];
                    reorderArray[j] = temp;
                }
            }
        }
        return reorderArray;
    }
}
