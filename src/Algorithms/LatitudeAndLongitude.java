/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Algorithms;

/**
 *
 * @author Shrey
 */
public class LatitudeAndLongitude implements java.io.Serializable{

     private double latitude;
     private double longitude;
    
    public LatitudeAndLongitude()
    {
        this.latitude=0;
        this.longitude=0;
        
    }

   public LatitudeAndLongitude(double latitude, double longitude) {
        this.latitude  = latitude;
        this.longitude =longitude;
    }
    
    public LatitudeAndLongitude(LatitudeAndLongitude temp) {
        this.latitude  = temp.latitude;
        this.longitude =temp.longitude;
    }
    
    
    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }
    
    

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    
}
