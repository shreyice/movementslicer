/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Algorithms;

/**
 *
 * @author Shrey
 */
public class TileCoord2D {

    private int xcoordinate;
    private int ycoordinate;

    public TileCoord2D() {
        this.xcoordinate = 0;
        this.ycoordinate = 0;

    }

    public int getXcoordinate() {
        return xcoordinate;
    }

    public void setXcoordinate(int xcoordinate) {
        this.xcoordinate = xcoordinate;
    }

    public int getYcoordinate() {
        return ycoordinate;
    }

    public void setYcoordinate(int ycoordinate) {
        this.ycoordinate = ycoordinate;
    }

    TileCoord2D(int pixelX, int pixelY) {
        this.xcoordinate = pixelX;
        this.ycoordinate = pixelY;
    }
}
