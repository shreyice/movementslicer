/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Algorithms;

import java.awt.geom.Rectangle2D.Double;

/**
 *
 * @author Shrey
 */
public class RectangleInformation {
    
    Double rect;
    StayPoint stayPoint;
    int stayRegionId;
    int personId;

    public RectangleInformation(Double rect, StayPoint stayPoint, int stayRegionId, int personId) {
        this.rect = rect;
        this.stayPoint = stayPoint;
        this.stayRegionId = stayRegionId;
        this.personId = personId;
    }

    public Double getRect() {
        return rect;
    }

    public StayPoint getStayPoint() {
        return stayPoint;
    }

    public int getStayRegionId() {
        return stayRegionId;
    }

    public int getPersonId() {
        return personId;
    }

 
    
    
    
    
}
