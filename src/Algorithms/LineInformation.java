/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Algorithms;

import java.awt.geom.Line2D.Double;

/**
 *
 * @author Shrey
 */
public class LineInformation {

    Double line;
    MeetingPoint meet;

    public LineInformation(Double line, MeetingPoint meet) {
        this.line = line;
        this.meet = meet;
    }

    public Double getLine() {
        return line;
    }

    public void setLine(Double line) {
        this.line = line;
    }

    public MeetingPoint getMeet() {
        return meet;
    }

    public void setMeet(MeetingPoint meet) {
        this.meet = meet;
    }
    
    
}
