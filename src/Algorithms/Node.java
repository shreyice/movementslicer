package Algorithms;

/**
 * Created with IntelliJ IDEA.
 * User: Shrey
 * Date: 10/18/13
 * Time: 7:02 PM
 * To change this template use File | Settings | File Templates.
 */
public class Node {

    boolean highlighted;
    int numberOfMeetings;

    public Node(boolean highlighted, int numberOfMeetings) {
        this.highlighted = highlighted;
        this.numberOfMeetings = numberOfMeetings;
    }

    public boolean isHighlighted() {
        return highlighted;
    }

    public void setHighlighted(boolean highlighted) {
        this.highlighted = highlighted;
    }

    public int getNumberOfMeetings() {
        return numberOfMeetings;
    }

    public void setNumberOfMeetings(int numberOfMeetings) {
        this.numberOfMeetings = numberOfMeetings;
    }

    public void incrementMeetings() {
        this.numberOfMeetings++;
    }
}
