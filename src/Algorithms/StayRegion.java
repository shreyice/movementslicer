 /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Algorithms;

 import org.joda.time.DateTime;

 import java.util.ArrayList;
 import java.util.HashMap;
 import java.util.TreeMap;


 /**
  *
  * @author Shrey
  */
 public class StayRegion {

     private LatitudeAndLongitude stayRegionCenter;
     private TreeMap<DateTime, StayPoint> regionStayPoints;
     private HashMap<Integer, TreeMap<DateTime,StayPoint>> personWiseListOfStayPoints;
     private int id;


     public StayRegion()
     {
         stayRegionCenter = null;
         regionStayPoints = null;
     }

     public StayRegion(LatitudeAndLongitude stayRegionCenter, TreeMap<DateTime, StayPoint> regionStayPoints, HashMap<Integer, TreeMap<DateTime, StayPoint>> personWiseListOfStayPoints, int id) {
         this.stayRegionCenter = stayRegionCenter;
         this.regionStayPoints = regionStayPoints;
         this.personWiseListOfStayPoints = personWiseListOfStayPoints;
         this.id = id;
     }
     //
 //       private ArrayList<StayRegion> globalStayRegionsAlgorithm(ArrayList<Person> personList)
 //        {
 //            ArrayList<StayRegion> stayRegionList = new ArrayList<StayRegion>();
 //            for(Person tempPerson : personList)
 //            {
 //                stayRegionList.addAll(tempPerson.getStayRegion());
 //            }
 //        int id = 0;
 //        int count =0;
 //        ArrayList<StayRegion> out = new ArrayList<StayRegion>();
 //        ArrayList<grid> g = new ArrayList<grid>();
 //        for (int i = 0; i < stayRegionList.size(); i++) {
 //                TileCoord2D pnt = new TileCoord2D();
 //                pnt = MapPoint.LatLongToPixelXY(stayRegionList.get(i).getStayRegionCenter().getLatitude(), stayRegionList.get(i).getStayRegionCenter().getLongitude(), 16);
 //                pnt = MapPoint.PixelXYToTileXY(pnt.getXcoordinate(), pnt.getYcoordinate());
 ////                System.out.println(stayPointsList.get(i)..getStayPointCenter().getLatitude()+","+stayPointsList.get(i)..getStayPointCenter().getLongitude());
 //                if (g.size() == 0) {
 //                    grid tempgrid = new grid();
 //                    tempgrid.x = pnt.getXcoordinate();
 //                    tempgrid.y = pnt.getYcoordinate();
 //                    tempgrid.stayRegionList.put(stayPointsList.get(i).getArrivalDateTime(),stayPointsList.get(i));
 //                    tempgrid.regionid = -1;
 //                    g.add(tempgrid);
 //                } else {
 //                    boolean flag = true;
 //                    for (int k = 0; k < g.size(); ++k) {
 //                        if (g.get(k).x == pnt.getXcoordinate() && g.get(k).y == pnt.getYcoordinate()) {
 //                            g.get(k).stayPointList.put(stayPointsList.get(i).getArrivalDateTime(),stayPointsList.get(i));
 //                            k++;
 //                            flag = false;
 //                        }
 //                    }
 //                    if (flag) {
 //                        grid tempgrid = new grid();
 //                        tempgrid.x = pnt.getXcoordinate();
 //                        tempgrid.y = pnt.getYcoordinate();
 //                        tempgrid.stayPointList.put(stayPointsList.get(i).getArrivalDateTime(),stayPointsList.get(i));
 //                        tempgrid.regionid = -1;
 //                        g.add(tempgrid);
 //                    }
 //                }
 //        }
 //        while (grid.checkIfAnyGridUnsed(g)) {
 //            grid temp = grid.gridWithMaxStayPointsAndNoRegionAssigned(g);
 //            grid enlargedGrid = grid.getNeighbourGrid(temp, g, count)   ;
 //            StayRegion s = new StayRegion();
 //            s.setStayRegionCenter(StayPoint.meanFromStayPoints(new ArrayList<StayPoint>(enlargedGrid.stayPointList.values())));
 //            s.setRegionStayPoints( enlargedGrid.stayPointList);
 //            s.setStayPointId(id);
 //            id = id+1;
 //            temp.regionid = count;
 ////            for (int i = 0; i < enlargedGrid.stayPointList.size(); ++i) {
 ////                System.out.println("stayregions center");
 ////                System.out.println(count + "  " + enlargedGrid.stayPointList.get(i).getStayPointCenter().getLatitude() + "," + enlargedGrid.stayPointList.get(i).getStayPointCenter().getLongitude());
 ////            }
 //            out.add(s);
 //            count++;
 //        }
 //
 //                out.trimToSize();
 //        return out;
 //    }


     public LatitudeAndLongitude getStayRegionCenter() {
         return stayRegionCenter;
     }

     public TreeMap<DateTime, StayPoint> getRegionStayPoints() {
         return regionStayPoints;
     }

     public void setRegionStayPoints(TreeMap<DateTime, StayPoint> regionStayPoints) {
         this.regionStayPoints = regionStayPoints;
     }

     public void setStayRegionCenter(LatitudeAndLongitude StayRegionCenter) {
         this.stayRegionCenter = StayRegionCenter;
     }

     public int getId() {
         return id;
     }

     public void setId(int id) {
         this.id = id;
     }

     public HashMap<Integer, TreeMap<DateTime,StayPoint>> getPersonWiseListOfStayPoints() {
         return personWiseListOfStayPoints;
     }

     public void setPersonWiseListOfStayPoints(HashMap<Integer,TreeMap<DateTime,StayPoint>> personWiseListOfStayPoints) {
         this.personWiseListOfStayPoints = personWiseListOfStayPoints;
     }

     public TreeMap<DateTime, StayPoint> getStayPointTreeMapOfPerson(int i) {
         return this.personWiseListOfStayPoints.get(i);
     }
 }

