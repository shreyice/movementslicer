package Algorithms;
import java.awt.Point;
import java.util.ArrayList;



/*
   * http://code.google.com/p/convex-hull/
   * special thanks to adam.larkeryd@gmail.com
 */

public interface ConvexHullAlgorithm
{
    ArrayList<Point> execute(ArrayList<Point> points);
}

