/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Algorithms;

import org.joda.time.DateTime;

/**
 *
 * @author shrey
 */
public class GPSLocation{


    private LatitudeAndLongitude latnlog;
    private DateTime dateandtime;


    public LatitudeAndLongitude getLatnlog() {
        return latnlog;
    }

    public void setLatnlog(LatitudeAndLongitude latnlog) {
        this.latnlog = latnlog;
    }

    public DateTime getDateandtime() {
        return dateandtime;
    }

    public void setDateandtime(DateTime dateandtime) {
        this.dateandtime = dateandtime;
    }
    
    
    
    
}
