/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Algorithms;

import org.joda.time.DateTime;
import org.joda.time.Interval;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.TreeMap;

/**
 *
 * @author Shrey
 */
public class MeetingPoint implements Comparable<MeetingPoint>{
    
    ArrayList<Integer> personId = new ArrayList<>();
    ArrayList<StayPoint> stayPoints = new ArrayList<>();
    int stayRegionId;
    LatitudeAndLongitude regionCentre;
    Interval interval;
    LatitudeAndLongitude center;
    String name;

    public void setRegionCentre(LatitudeAndLongitude regionCentre) {
        this.regionCentre = regionCentre;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void calculateCenter() {
        ArrayList<LatitudeAndLongitude> gps = new ArrayList<>();
        for (StayPoint s : this.stayPoints) {
            gps.add(s.getStayPointCenter());
        }

        this.center = LatitudeAndLongitudeUtils.meanFromLatitudeAndLongitude(gps);
    }

    public void addPerson(int n) {
        this.personId.add(n);

    }

    public void addStayPoint(StayPoint stayPoint) {
        this.stayPoints.add(stayPoint);


    }

    public ArrayList<Integer> getPersonId() {
        return personId;
    }

    public void setPersonId(ArrayList<Integer> personId) {
        this.personId = personId;
    }

    public ArrayList<StayPoint> getStayPoints() {
        return stayPoints;
    }

    public void setStayPoints(ArrayList<StayPoint> stayPoints) {
        this.stayPoints = stayPoints;
    }

    public int getStayRegionId() {
        return stayRegionId;
    }

    public void setStayRegionId(int stayRegionId) {
        this.stayRegionId = stayRegionId;
    }

    public Interval getInterval() {
        return interval;
    }

    public void setInterval(Interval interval) {
        this.interval = interval;
    }

    public LatitudeAndLongitude getCenter() {
        return center;
    }

    public void setCenter(LatitudeAndLongitude center) {
        this.center = center;
    }

    public DateTime startDateTime() {
        return this.interval.getStart();
    }
    public DateTime endDateTime() {
        return this.interval.getEnd();
    }

    @Override
    public int compareTo(MeetingPoint o) {

        if(this.getInterval().getStart().isBefore(o.getInterval().getStart()))
            return -1;  //To change body of implemented methods use File | Settings | File Templates.
        else if(this.getInterval().getStart().isAfter(o.getInterval().getStart()))
            return 1;
        else
            return 0;
    }

    @Override
    public boolean equals(Object obj) {
        final MeetingPoint other = (MeetingPoint) obj;

        if (this.getPersonId().size() != other.getPersonId().size()
                ) {
            return false;
        }

        for (int i = 0; i < other.getStayPoints().size(); i++) {

            if (this.getStayPoints().get(i).getStayRegionId() != other.getStayPoints().get(i).getStayRegionId())
                return false;

                   if( this.getStayPoints().get(i).getStayPointId() != other.getStayPoints().get(i).getStayPointId())
                       return false;

                    if( this.getPersonId().get(i) != other.getPersonId().get(i))
                        return false;

        }
        return true;
    }



}
