package Algorithms;

/**
 * Created with IntelliJ IDEA.
 * User: Shrey
 * Date: 10/9/13
 * Time: 5:40 PM
 * To change this template use File | Settings | File Templates.
 */
public class MinMax {


    public int min;
    public int max;

    MinMax() {
        min =0;
        max =0;
    }

    public void calculate(Node[][] matrix) {

        min = matrix[0][0].getNumberOfMeetings();
        max = matrix[0][0].getNumberOfMeetings();

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                if (matrix[i][j].getNumberOfMeetings() < min) {
                    min = matrix[i][j].getNumberOfMeetings();
                }
                if (matrix[i][j].getNumberOfMeetings() > max) {
                    max = matrix[i][j].getNumberOfMeetings();
                }
            }
        }
    }
}
