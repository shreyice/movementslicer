/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Algorithms;

import org.joda.time.DateTime;
import org.joda.time.Seconds;

import java.io.File;
import java.util.ArrayList;
import java.util.TreeMap;

/**
 *
 * @author Shrey
 */
public class Person {
    
    private int id;
    private boolean isHighlighted = false;
    private TreeMap<DateTime, GPSLocation> track;
    private TreeMap<DateTime, StayPoint> stayPoint;
    private ArrayList<StayRegion> stayRegion;

    public Person() {
        this.id = -99;
        this.track = null;
        this.stayPoint = null;
        this.stayRegion = null;
    }

    public Person(int id, String path) {
        this.id = id;
        this.track = DataRead.loadTreeMapFromDir(new File(path));
        System.out.println(id+"Track size" + this.track.values().size());
        this.stayPoint = stayPointAlgorithm(new ArrayList<>(track.values()));
//        this.stayRegion = stayRegionAlgorithm(new ArrayList<>(stayPoint.values()));
        this.stayRegion = stayRegionAlgorithm(new ArrayList<>(stayPoint.values()));
    }

    public  void reCalculate()
    {
        this.stayPoint = stayPointAlgorithm(new ArrayList<>(track.values()));
        this.stayRegion = stayRegionAlgorithm(new ArrayList<>(stayPoint.values()));
    }

    public static ArrayList<Person> loadData(File []dirs) {
        ArrayList<Person> persons= new ArrayList<>();
        int id = 0;
        for (File f : dirs) {
            persons.add(new Person(id++, f.getAbsolutePath() + "\\Trajectory"));
        }
        return persons;
    }
  

    /**
     * Calculates StayPoints for the given Person using thresholds of Time and Distance defined in the Constants Class
     * Algorithm used is described in Paper :  
     * Mining User Similarity Based on Loacation History
     * by Quannan LI, Yu Zheng, Yukun Chen, Wenyu Liu, Wei-Ying Ma
     * 
     * @param input List of GPSLocations (LatitudeAndLongitude and TimeStamp)
     * @return      TreeMap of StayPoints
     */
    private TreeMap<DateTime, StayPoint> stayPointAlgorithm(ArrayList<GPSLocation> input) {
        int id =0;
        TreeMap<DateTime, StayPoint> output = new TreeMap<>();
        int i = 0;
        int j = 0;
        int size = input.size();
        double dist = 0.0;
        long timeDiff = 0;
        int token = 0;
        while (i < size) {
            j = i + 1;
            token = 0;
            while (j < size) {
//                timeDiff = timeDifference(input.get(j).getTime(), input.get(j - 1).getTime());
                dist = LatitudeAndLongitudeUtils.distance(input.get(i).getLatnlog(), input.get(j).getLatnlog());

                if (dist > Constants.DISTANCE_THRESHOLD) {
                      timeDiff = input.get(j-1).getDateandtime().getMillis() - input.get(i).getDateandtime().getMillis();
                    if (timeDiff > Constants.MIN_TIME_THRESHOLD ) {
                        StayPoint temp = new StayPoint();
                        temp.setStayPointCenter(LatitudeAndLongitudeUtils.meanfromLocations(input, i, j - 1));
                        ArrayList<GPSLocation> subList = new ArrayList<>();
                        for(int count = i ; count < j;++count) {
                         subList.add(input.get(count));    
                        }
                        subList.trimToSize();
                        temp.setStayPointId(id++);
                        temp.setGpsLocations(subList);
                        temp.setArrivalDateTime(input.get(i).getDateandtime());
                        temp.setDepartureDateTime(input.get(j-1).getDateandtime());
                        temp.setPersonId(this.id);
                        output.put(input.get(i).getDateandtime(), temp);
                        i = j;
                        token = 1;
                    }
                    break;
                }
                j = j + 1;
            }
            if (token != 1) {
                i = i + 1;
            }
        }
            return output;
    }
    
    /**
     * Calculates StayRegions using a Grid-Based clustering Algorithm.
     * The map is divided into grids based on the Tile-Map System (Zoom Levels) followed by BingMaps System.
     *                                                                                                1 1 1
     * It firsts takes all stayPoints in one grid  1  and then expands itself to form a grid of 3x3   1 1 1
     *                                                                                                1 1 1
     * and considers all the stayPoints in the 3x3 grid to be a part of the same stayRegion
     * for levelOfDetail :: corresponds to zoom level : http://msdn.microsoft.com/en-us/library/aa940990.aspx & 
     *                                                  http://msdn.microsoft.com/en-us/library/bb259689.aspx
     * 
     * 
     * So for eg 
     * 
     * Level of Detail   Map Width and Height (pixels)   Ground Resolution (meters / pixel) 
     *      21                 0.0746 m                 
     * So each grid is of 256x256 pixels which corresponds to (256 *.0746)^2 meters  = 364.71 meters^2
     * Algorithm from :
     * Mining User Similarity Based on Loacation History
     * by Quannan LI, Yu Zheng, Yukun Chen, Wenyu Liu, Wei-Ying Ma
     * 
     * @param stayPointsList List of StayPoint of the user calculated from the stayPointAlgorithm
     * @return List of StayRegions for the user
     */

        //with individual staypoints for everyone
       private ArrayList<StayRegion> stayRegionAlgorithm(ArrayList<StayPoint> stayPointsList)
        {
        int id = 0;
        int count =0;
        ArrayList<StayRegion> out = new ArrayList<StayRegion>();
        ArrayList<Grid> g = new ArrayList<Grid>();
        for (int i = 0; i < stayPointsList.size(); i++) {
                TileCoord2D pnt = new TileCoord2D();
                pnt = MapPoint.LatLongToPixelXY(stayPointsList.get(i).getStayPointCenter().getLatitude(),
                        stayPointsList.get(i).getStayPointCenter().getLongitude(), Constants.LEVEL_OF_DETAIL);
                pnt = MapPoint.PixelXYToTileXY(pnt.getXcoordinate(), pnt.getYcoordinate());
//                System.out.println(stayPointsList.get(i)..getStayPointCenter().getLatitude()+","+stayPointsList.get(i)..getStayPointCenter().getLongitude());
                if (g.size() == 0) {
                    Grid tempGrid = new Grid();
                    tempGrid.x = pnt.getXcoordinate();
                    tempGrid.y = pnt.getYcoordinate();
                    tempGrid.stayPointList.put(stayPointsList.get(i).getArrivalDateTime(),stayPointsList.get(i));
                    tempGrid.regionid = -1;
                    g.add(tempGrid);
                } else {
                    boolean flag = true;
                    for (int k = 0; k < g.size(); ++k) {
                        if (g.get(k).x == pnt.getXcoordinate() && g.get(k).y == pnt.getYcoordinate()) {
                            g.get(k).stayPointList.put(stayPointsList.get(i).getArrivalDateTime(),stayPointsList.get(i));
                            k++;
                            flag = false;
                        }
                    }
                    if (flag) {
                        Grid tempgrid = new Grid();
                        tempgrid.x = pnt.getXcoordinate();
                        tempgrid.y = pnt.getYcoordinate();
                        tempgrid.stayPointList.put(stayPointsList.get(i).getArrivalDateTime(),stayPointsList.get(i));
                        tempgrid.regionid = -1;
                        g.add(tempgrid);
                    }
                }
        }
        while (Grid.checkIfAnyGridUnsed(g)) {
            Grid temp = Grid.gridWithMaxStayPointsAndNoRegionAssigned(g);
            Grid enlargedGrid = Grid.getNeighbourGrid(temp, g, count);
//            grid enlargedGrid = temp;
            StayRegion s = new StayRegion();
            s.setStayRegionCenter(StayPoint.meanFromStayPoints(new ArrayList<StayPoint>(enlargedGrid.stayPointList.values())));
            s.setId(id);
            ArrayList<StayPoint> tempList = new ArrayList<>(enlargedGrid.stayPointList.values());
            for(int i =0; i < enlargedGrid.stayPointList.size(); i++)
            {
               tempList.get(i).setStayRegionId(id);
            }
            s.setRegionStayPoints(enlargedGrid.stayPointList);

            id = id+1;
            temp.regionid = count;
            out.add(s);
            count++;
        }
                out.trimToSize();
        return out;
    }

       /**
        * Displays stayPoints of the Person
        */
       public void displayStayPoints()
       {
           
           ArrayList<GPSLocation> temp = new ArrayList(track.values());
            ArrayList<StayPoint> stemp = new ArrayList(stayPoint.values());
//                  for displaying stay Points
            for(int i=0; i< stemp.size(); ++i)
            {
                System.out.println("StayPoint   "+stemp.get(i).getStayPointId());
                double timeDiff = Seconds.secondsBetween(stemp.get(i).getArrivalDateTime(), stemp.get(i).getDepartureDateTime()).getSeconds();
                double dist = LatitudeAndLongitudeUtils.distance(stemp.get(i).getGpsLocations().get(stemp.get(i).getGpsLocations().size() - 1).getLatnlog(), stemp.get(i).getGpsLocations().get(0).getLatnlog());
                System.out.println(stemp.get(i).getStayPointCenter().getLatitude()+","+stemp.get(i).getStayPointCenter().getLongitude()+"  "+stemp.get(i).getArrivalDateTime().toString("yyyy-MM-dd HH:mm:ss")+"  "+stemp.get(i).getDepartureDateTime().toString("yyyy-MM-dd HH:mm:ss")+"  "+timeDiff+"  "+dist);
                for(int k=0; k< stemp.get(i).getGpsLocations().size();++k)
                {
                    System.out.println(stemp.get(i).getGpsLocations().get(k).getLatnlog().getLatitude()+","+stemp.get(i).getGpsLocations().get(k).getLatnlog().getLongitude()+"  "+stemp.get(i).getGpsLocations().get(k).getDateandtime().toString("yyyy-MM-dd HH:mm:ss"));

                }
                
            }
       }

       /**
        * displays StayRegions of the Person
        */
       public void displayStayRegions()
       {
            ArrayList<StayRegion> srtemp = stayRegion;
            ArrayList<StayPoint> stemp = new ArrayList(stayPoint.values());

           double mili=0;
            int sum=0;
            for(int i=0; i< srtemp.size();++i)
            {
                
                ArrayList<StayPoint> s = new ArrayList<StayPoint>(srtemp.get(i).getRegionStayPoints().values());
                System.out.println("StayRegion"+"  "+srtemp.get(i).getId() +"  "+"StayPoint  "+ s.size());
                sum = sum+s.size();
                System.out.println(srtemp.get(i).getStayRegionCenter().getLatitude()+","+srtemp.get(i).getStayRegionCenter().getLongitude());
                for(int j=0; j< s.size();++j)
                {
                    double timeDiff = Seconds.secondsBetween(stemp.get(i).getArrivalDateTime(), stemp.get(i).getDepartureDateTime()).getSeconds();
                timeDiff = timeDiff/(1000*60);
                    mili = mili+ (s.get(j).getDepartureDateTime().getMillis() -s.get(j).getArrivalDateTime().getMillis());
                    System.out.println(s.get(j).getStayPointCenter().getLatitude()+","+s.get(j).getStayPointCenter().getLongitude()+" "+s.get(j).getArrivalDateTime().toString("yyyy-MM-dd HH:mm:ss")+" "+s.get(j).getDepartureDateTime().toString("yyyy-MM-dd HH:mm:ss")+" "+timeDiff);
                }
                System.out.println((mili/(1000*60)));
                mili =0;
            }
       }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public TreeMap<DateTime, GPSLocation> getTrack() {
        return track;
    }

    public void setTrack(TreeMap<DateTime, GPSLocation> track) {
        this.track = track;
    }

    public ArrayList<StayRegion> getStayRegion() {
        return stayRegion;
    }

    public void setStayRegion(ArrayList<StayRegion> stayRegion) {
        this.stayRegion = stayRegion;
    }

    public TreeMap<DateTime, StayPoint> getStayPoint() {
        return stayPoint;
    }

    public void setStayPoint(TreeMap<DateTime, StayPoint> stayPoint) {
        this.stayPoint = stayPoint;
    }

    public ArrayList<StayPoint> getStayPointinArrayList(int person) {

        return new ArrayList<StayPoint>(this.stayPoint.values());

    }
    public ArrayList<StayPoint> getStayPointinArrayList() {

        return new ArrayList<StayPoint>(this.stayPoint.values());

    }

    public boolean getHighlighted() {
        return isHighlighted;
    }

    public void setHighlighted(boolean isHighlighted) {
        this.isHighlighted = isHighlighted;
    }
}
