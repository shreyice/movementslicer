/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Algorithms;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeMap;

/**
 * This is a temporary internal data structure used by stayRegionAlgorithm
 * 
 * @author Shrey
 */
public class Grid
{

     TreeMap<DateTime, StayPoint> stayPointList = new TreeMap<DateTime, StayPoint>();
     HashMap<Integer,TreeMap<DateTime,StayPoint>> personWiseStayPointList = new HashMap<>();
//        ArrayList<LatitudeAndLongitude> arlnl = new ArrayList<LatitudeAndLongitude>();
        long x;
        long y;
        int regionid = -1;

        /**
         * Checks if all the grids in which the points have been initailly distributed are used
         * 
         * @param grid
         * @return boolean false if any unused
         */
        static boolean checkIfAnyGridUnsed(ArrayList<Grid> grid) {
        boolean flag = false;
        Iterator<Grid> it = grid.iterator();
        while (it.hasNext()) {
            if (it.next().regionid == -1) {
                flag = true;
                break;
            }
        }
        return flag;
    }

    
        /**
         * 
         * @param center the cente
         * @param g
         * @param count
         * @return anenlarged grid of 9
         */
        static Grid getNeighbourGrid(Grid center, ArrayList<Grid> g, int count) {
        Iterator<Grid> it = g.iterator();
        Grid temp = new Grid();
        temp.stayPointList.putAll(center.stayPointList);
        while (it.hasNext()) {
            Grid temp1 = it.next();
            if (temp1.regionid == -1) {
                if ((temp1.x == center.x - 1 && temp1.y == center.y - 1)
                        || (temp1.x == center.x + 1 && temp1.y == center.y + 1)
                        || (temp1.x == center.x && temp1.y == center.y - 1)
                        || (temp1.x == center.x && temp1.y == center.y + 1)
                        || (temp1.x == center.x - 1 && temp1.y == center.y)
                        || (temp1.x == center.x + 1 && temp1.y == center.y)
                        || (temp1.x == center.x && temp1.y == center.y + 1)
                        || (temp1.x == center.x + 1 && temp1.y == center.y - 1)
                        || (temp1.x == center.x - 1 && temp1.y == center.y + 1)) {
                    temp.stayPointList.putAll(temp1.stayPointList);
                    temp1.regionid = count;
                }
            }
        }
//      temp.add(g.get(index));
        return temp;
    }

    static Grid getNeighbourGridPersonWise(Grid center, ArrayList<Grid> g, int count) {
        Iterator<Grid> it = g.iterator();
        Grid temp = new Grid();
        temp.personWiseStayPointList = new HashMap<>();
        temp.personWiseStayPointList = addTwoPersonWiseList(temp.personWiseStayPointList, center.personWiseStayPointList);
        while (it.hasNext()) {
            Grid temp1 = it.next();
            if (temp1.regionid == -1) {
                if ((temp1.x == center.x - 1 && temp1.y == center.y - 1)
                        || (temp1.x == center.x + 1 && temp1.y == center.y + 1)
                        || (temp1.x == center.x && temp1.y == center.y - 1)
                        || (temp1.x == center.x && temp1.y == center.y + 1)
                        || (temp1.x == center.x - 1 && temp1.y == center.y)
                        || (temp1.x == center.x + 1 && temp1.y == center.y)
                        || (temp1.x == center.x && temp1.y == center.y + 1)
                        || (temp1.x == center.x + 1 && temp1.y == center.y - 1)
                        || (temp1.x == center.x - 1 && temp1.y == center.y + 1)) {
                    temp.personWiseStayPointList = addTwoPersonWiseList(temp.personWiseStayPointList
                            , temp1.personWiseStayPointList);
                    temp1.regionid = count;
                }
            }
        }
//      temp.add(g.get(index));
        return temp;
    }

    static  HashMap<Integer,TreeMap<DateTime,StayPoint>> addTwoPersonWiseList(
            HashMap<Integer,TreeMap<DateTime,StayPoint>> temp1, HashMap<Integer,TreeMap<DateTime,StayPoint>> temp2
    ) {

        for (int i : temp2.keySet()) {
            if (temp1.containsKey(i)) {
                temp1.get(i).putAll(temp2.get(i));
            }
            else {
                temp1.put(i, temp2.get(i));
            }
        }
        return temp1;
    }

     
        /**
         * Returns grid containing maximum number of stayPoints
         * @param g
         * @return 
         */
        static Grid gridWithMaxStayPointsAndNoRegionAssigned(ArrayList<Grid> g) {
        int max = 0;
        Iterator<Grid> it = g.iterator();
        Grid temp = null;
        for (int i = 0; i < g.size(); ++i) {
            if (g.get(i).stayPointList.size() > max && g.get(i).regionid == -1) {
                temp = g.get(i);
                max = g.get(i).stayPointList.size();
            }
        }
        return temp;
    }

    /**
         * Returns grid containing maximum number of stayPoints
         * @param g
         * @return
         */
        static Grid gridWithMaxStayPointsAndNoRegionAssignedPersonWiseList(ArrayList<Grid> g) {
        int max = 0;
        Iterator<Grid> it = g.iterator();
        Grid temp = null;

        for (int i = 0; i < g.size(); ++i) {
            int sum =0;
            for (int j : g.get(i).personWiseStayPointList.keySet()) {
                sum = sum +  g.get(i).personWiseStayPointList.get(j).size();

            }
            if (sum > max && g.get(i).regionid == -1) {
                temp = g.get(i);
                max = sum;
            }
        }
        return temp;
    }

    public ArrayList<StayPoint> listOfStayPointsFromHashMaps() {
        ArrayList<StayPoint> temp = new ArrayList<>();
        for (int i  :this.personWiseStayPointList.keySet()) {
            temp.addAll(this.personWiseStayPointList.get(i).values());
        }
                return temp;

    }

    public HashMap<Integer, TreeMap<DateTime,StayPoint>> getPersonWiseStayPointList() {
        return personWiseStayPointList;
    }
}
