/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Algorithms;

/**
 *
 * @author Shrey
 *
 * Thanks to Bing Maps for tiling and conversion functions
 * http://msdn.microsoft.com/en-us/library/bb259689.aspx
 *
 */
public class MapPoint {

    private static final double EarthRadius = 6378137;
    private static final double MinLatitude = -85.05112878f;
    private static final double MaxLatitude = 85.05112878f;
    private static final double MinLongitude = -180;
    private static final double MaxLongitude = 180;

    /**Clips a number to the specified minimum and maximum values.
    
     * @param n          The number to clip.
     * @param minValue  Minimum allowable value.
     * @param maxValue  Maximum allowable value.
     * @return 
     */
    private static double Clip(double n, double minValue, double maxValue) {
        return Math.min(Math.max(n, minValue), maxValue);
    }

    
    /**
     * determines map height and width at a zoom level
     * @param zoom  levelOfDetail
     * @return 
     */
    public static int MapSize(int zoom) {
        return 256 << zoom;
    }

    //Determines the ground resolution
    public static double GroundResolution(double latitude, int zoom) {
        latitude = Clip(latitude, MinLatitude, MaxLatitude);
        return Math.cos(latitude * Math.PI / 180) * 2 * Math.PI * EarthRadius / MapSize(zoom);
    }

    //Determines the map scale value
    public static double MapScale(double latitude, int zoom, int screenDpi) {
        return GroundResolution(latitude, zoom) * screenDpi / 0.0254;
    }

    //coordinates x and y in pixels from latitude and longitude
    public static TileCoord2D LatLongToPixelXY(double latitude, double longitude, int zoom) {
        latitude = Clip(latitude, MinLatitude, MaxLatitude);
        longitude = Clip(longitude, MinLongitude, MaxLongitude);

        double x = (longitude + 180) / 360;
        double sinLatitude = Math.sin(latitude * Math.PI / 180);
        double y = 0.5 - Math.log((1 + sinLatitude) / (1 - sinLatitude)) / (4 * Math.PI);
//        System.out.println(x+","+y);
        int mapSize = MapSize(zoom);
        
        double pixelX =  Clip(x * mapSize + 0.5f, 0, mapSize - 1);
        double pixelY =  Clip(y * mapSize + 0.5f, 0, mapSize - 1);
//            System.out.println(pixelX);
//            System.out.println(pixelY);
        TileCoord2D pnt = new TileCoord2D((int)pixelX,(int) pixelY);

        return pnt;

    }

    //Converts from pixel to latitude and longitude
    public static LatitudeAndLongitude PixelXYToLatLong(int pixelX, int pixelY, int zoom) {
        double mapSize = MapSize(zoom);
        double x = (double)(Clip(pixelX, 0, mapSize - 1) / mapSize) - 0.5f;
        double y = 0.5f - Clip(pixelY, 0, mapSize - 1) / mapSize;

        double latitude = 90 - 360 * Math.atan(Math.exp(-y * 2 * Math.PI)) / Math.PI;
        double longitude = 360 * x;

        LatitudeAndLongitude lnl = new LatitudeAndLongitude(latitude, longitude);

        return lnl;
    }

    //coordinates x and y in pixels from latitude and longitude
    public static void PixelXYToWorldXY(int pixelX, int pixelY, int zoom) {

        double worldX = pixelX / Math.pow(2, zoom);
        double worldY = pixelY / Math.pow(2, zoom);
//        System.out.println(worldX);
//        System.out.println(worldY);

    }

    //Converts from pixel to latitude and longitude
    public static void WorldXYToPixelXY(int worldX, int worldY, int zoom) {

        int pixelX = (int) (worldX * Math.pow(2, zoom));
        int pixelY = (int) (worldY * Math.pow(2, zoom));

    }

    //converts pixel coordinates to tile coordinates
    public static TileCoord2D PixelXYToTileXY(int pixelX, int pixelY) {
        int tileX = pixelX / 256;
        int tileY = pixelY / 256;

        TileCoord2D Tpnt = new TileCoord2D(tileX, tileY);

        return Tpnt;

    }

    //converts tile coordinates to pixel coordinates
    public static TileCoord2D TileXYToPixelXY(int tileX, int tileY) {
        int pixelX = tileX * 256;
        int pixelY = tileY * 256;

        TileCoord2D Ppnt = new TileCoord2D(pixelX, pixelY);

        return Ppnt;


    }
}
