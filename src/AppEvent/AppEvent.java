package AppEvent;

/**
 * Created with IntelliJ IDEA.
 * User: Shrey
 * Date: 10/12/13
 * Time: 5:59 PM
 * To change this template use File | Settings | File Templates.
 */
public class AppEvent extends java.util.EventObject
{
    public final static String NAV_EVENT = "NAV_EVENT";
    public final static String STATUS_EVENT = "STATUS_EVENT";
    public final static String DATA_EVENT = "DATA_EVENT";

    Object object;

    private String eventType;

    public AppEvent(String strEventType)
    {
        super(strEventType);
        this.eventType = strEventType;
    }

    public boolean isNavEvent()
    {
        if(this.eventType.equals(NAV_EVENT))
            return true;
        else
            return false;
    }

    public boolean isStatusEvent()
    {
        if(this.eventType.equals(STATUS_EVENT))
            return true;
        else
            return false;
    }

    public boolean isDataEvent()
    {
        if(this.eventType.equals(DATA_EVENT))
            return true;
        else
            return false;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }
}